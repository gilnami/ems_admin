<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ftt" tagdir="/WEB-INF/tags/"%>
<ftt:page>
	<!-- Content Header (Page header) -->
     <section class="content-header">
         <h1>
             Error Page
         </h1>
         <ol class="breadcrumb">
             <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
             <li class="active">error</li>
         </ol>
     </section>

 <!-- Main content -->
     <section class="content">
      
         <div class="error-page">
             <div class="error-content">
                 <h3><i class="fa fa-warning text-yellow"></i> 
      				<span class="msg">${message }</span>
                 </h3>
             </div><!-- /.error-content -->
         </div><!-- /.error-page -->

     </section><!-- /.content -->
    
</ftt:page>