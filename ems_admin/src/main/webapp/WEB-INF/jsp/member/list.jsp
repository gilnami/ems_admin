<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ftt" tagdir="/WEB-INF/tags/"%>
<ftt:page>
    <!-- 페이지명, breadcrumb -->
    <section class="content-header">
		<h1>
			사용자 조회 - <strong>${gameName} </strong>
		</h1>
		<ol class="breadcrumb">
        	<li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        	<li class="active">사용자 조회</li>
      	</ol>
	</section>

	<section class="content">
    	<!-- 페이지 안내 -->
      	<!-- 목록 -->
      	
	    
      	<div>
        	<div class="box">
        		
        		<div class="box-header text-right"></div>
        		<div class="box-body">
        		
		         	<input type="hidden" name="ssn" id="ssn" value="${ssn }"/>
		         	<div class="row">
		         		
		         		<div class="col-sm-4">
		         			<div class="dataTables_filter">
				            	<div class="form-group">
				            		<label>조회 구분 </label>
					                <input type="radio" id="searchType" name="searchType" value="accountIdx" checked>AccountID
					                <input type="radio" id="searchType" name="searchType" value="pimIdx">PIM
					                
					                <input type="text" class="form-control input" id="searchValue" name="searchValue">
					            	
					            	<button class="btn btn-default btn-sm" id="btnSearch">조회</button>
					            		
					            </div>
					         </div>
		         		</div>
		         	</div>
		         	
		         	<!-- List-->
        			<div class="row">
		          		<div class="col-sm-12 table-responsive">          	
		          			<table class="table table-bordered table-striped table-hover" id="list_area">
		          				<thead>
		          				<tr>
        							<th class="text-center"><label class="control-label">게임</label></th>
        							<th class="text-center"><label class="control-label">AccountID</label></th>
        							<th class="text-center"><label class="control-label">PIM</label></th>
        							<th class="text-center"><label class="control-label">유저상태</label></th>
        							<th class="text-center"><label class="control-label">이메일 수신상태</label></th>
        							<th class="text-center"><label class="control-label">국가</label></th>
        							<th class="text-center"><label class="control-label">언어</label></th>
        							<th class="text-center"><label class="control-label">등록일</label></th>
        							<th class="text-center"><label class="control-label">수정일(수신상태변경)</label></th>
		          				</tr>
		          				</thead>
		          				<tbody>
		          				
		          				</tbody>
					        </table>			    
		         		</div>
		         	</div>
		         	
		         	
         		</div>
          		<!-- /.box-body -->
       		</div>
        	<!-- /.box -->
      	</div>
    </section>
    <!-- /.content -->

</ftt:page>
<script type="text/javascript">
var _member = (function(){
	
	var drawList = function(data){
		$("#list_area > tbody").empty();
		var html = "";
		if(data.list == null || data.list.length == 0){
			html += '<tr>';
			html += '<td colspan="9">검색된 내용이 없습니다.</td>';
			html += '</tr>';
		}else{
			var statusList = data.accountStatusList;
			var recvList = data.recvStatusList;
			
			for(var i=0;i<data.list.length;i++){
				var r = data.list[i];
				html += '<tr>';
				html += '<td>'+r.gameName+'</td>';
				html += '<td>'+r.accountIdx+'</td>';
				html += '<td>'+r.pimIdx+'</td>';
				
				//유저 상태
				html += '<td><select id="selAccountStatus" ssn="'+r.ssn+'" accountIdx="'+r.accountIdx+'" onchange="updateAccountStatus(this)">';
				for(var key in statusList){
					var selected = "";
					if(r.accountStatus == key) selected = "selected";
					html += '<option value="'+key+' " '+selected+'>'+statusList[key]+'</option>';
				}
				html += '</select></td>';
				
				//수신 상태
				html += '<td><select id="selRecvStatus" pimIdx="'+r.pimIdx+'" onchange="updateRecvStatus(this)">';
				for(var key in recvList){
					var selected = "";
					if(r.recvStatus == key) selected = "selected";
					html += '<option value="'+key+' " '+selected+'>'+recvList[key]+'</option>';
				}
				html += '</select></td>';
				
				html += '<td>'+r.countryName+'</td>';
				html += '<td>'+r.langName+'</td>';
				html += '<td>'+r.regDt+'</td>';
				html += '<td>'+r.updDt+'</td>';
				html += '</tr>';
			}
			
		}
		
		$("#list_area > tbody").html(html);
	};
	
	return {
		
		init : function(){
			
			_member.list();
		},
		
		list : function(){
			var url = "/ajax/member/list";
			var accountIdx = 0, pimIdx = 0;
			if($("#searchType").val() == "accountIdx") accountIdx = $("#searchValue").val();
			else pimIdx = $("#searchValue").val();
			
			$.ajax({
				url : url,
				type: 'POST',
			    data : {ssn:$("#ssn").val(), accountIdx : accountIdx, pimIdx: pimIdx},
				dataType: 'json',
				success: function(resp) {
					drawList(resp.data);
				},
				error: function() {
					alert('시스템 에러가 발생하였습니다.');
				}
			});
		}
	}
})();


$("#document").ready(function(){
	
	$("#btnSearch").click(function(){
		if($("#searchValue").val() == ""){
			alert("검색할 AccountID 또는 PIM을 입력해주세요");
			$("#searchValue").focus();
			return false;
		}
		_member.list();
	});
});

function updateAccountStatus(obj){
	if(!confirm("상태를 변경하시겠습니까?")) return false;
	var ssn = $(obj).attr("ssn");
	var accountIdx = $(obj).attr("accountIdx");
	var status = $(obj).val();
	var url = "/ajax/member/change/account-status";			
	$.ajax({
		url : url,
		type: 'POST',
	    data : {ssn:ssn, accountIdx:accountIdx, status:status},
		dataType: 'json',
		success: function(resp) {
		},
		error: function() {
			alert('시스템 에러가 발생하였습니다.');
		}
	});
}

function updateRecvStatus(obj){
	if(!confirm("상태를 변경하시겠습니까?")) return false;
	var pimIdx = $(obj).attr("pimIdx");
	var status = $(obj).val();
	var url = "/ajax/member/change/recv-status";			
	$.ajax({
		url : url,
		type: 'POST',
	    data : {pimIdx:pimIdx, status:status},
		dataType: 'json',
		success: function(resp) {
		},
		error: function() {
			alert('시스템 에러가 발생하였습니다.');
		}
	});
}
</script>
