<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ftt" tagdir="/WEB-INF/tags/"%>
<ftt:page>
    <!-- 페이지명, breadcrumb -->
    <section class="content-header">
		<h1>
			소식통 관리 - <strong>${gameName} </strong>
		</h1>
		<ol class="breadcrumb">
        	<li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        	<li class="active">소식통 관리</li>
      	</ol>
	</section>
    <!-- 메인컨텐츠 영역 -->
		<section class="content">
			<div class="callout callout-danger">
				<p>
					<span class="glyphicon glyphicon-check"></span>&nbsp;&nbsp;제목(64자)은 꼭 입력해주세요(필수)
				</p>
			</div>
			<div class="clearfix">
				<div class="box box-default color-palette-box">
					<div class="box-body clearfix">
						<div class="col-xs-12 table-responsive">
							<form id="form">
								<table class="table">
									<tbody>
										<tr class="border-none-top" style="display:none;">
											<th>
												<div class="col-xs-12">설명</div>
											</th>
											<td>
												<div class="col-xs-12">
													<input type="text" class="form-control input-sm" name="desc" maxlength="100" placeholder="설명을 입력하세요." value="">
												</div>
											</td>
										</tr>
										
										<tr>
											<th>
												<div class="col-xs-12">서비스지역(서버)</div>
											</th>
											<td class="col-xs-10">
												<div class="col-xs-3">
													<select id="zoneSelector" name="zone" multiple>
														<c:forEach var="zone" items="${zoneList}">
															<option value="${zone.code}" ${fn:contains(emsZone,zone.code) ? 'selected' : ''}>${zone.name}</option>
														</c:forEach>
													</select>
												</div>
												<div class="col-md-9">
													<div>
														<!-- 선택된 서비스 존이 출력됩니다 -->
														<c:forEach var="zone" items="${zoneList}">
															<span class="label label-success text-sm zone-label" zoneCode="${zone.code}" style="display:${fn:contains(emsZone, zone.code) ? '' : 'none'}">${zone.name}</span>
														</c:forEach>
													</div>
												</div>
											</td>
										</tr>
										
										<tr>
											<th>
												<div class="col-xs-12">노출 국가</div>
											</th>
											<td class="col-xs-10">
												<div class="col-xs-3">
													<select id="countrySelector" name="country" multiple>
													    <option title="기타국가(00)" value="00" ${fn:contains(emsCountry,'00') ? 'selected' : ''}>기타국가(00)</option>
														<c:forEach var="country" items="${countryList}">
															<option title="${fn:escapeXml(country.name)}(${country.code})" value="${country.code}" ${fn:contains(emsCountry,country.code) ? 'selected' : ''}>
																${country.name}(${country.code})
																- 
																<c:forEach var="zone" items="${zoneList}">
																	<c:if test="${zoneCountryCodeListMap[zone.code].contains(country.code)}">
																		&lt;i style="font-size:9pt;"&gt;${zoneCodeTable[zone.code].name}&lt;/i&gt;
																	</c:if>
																</c:forEach>
															</option>
														</c:forEach>
														    
													</select>
												</div>
												<div class="col-md-9" style="max-height:200px;overflow-y:scroll">
													<div>
														<!-- 선택된 국가가 출력됩니다 -->
														<span class="label label-success country-label" countryCode="00" style="display:${fn:contains(emsCountry,'00') ? '' : 'none'}">기타국가</span>
														<c:forEach var="country" items="${countryList}">
															<span class="label label-success country-label" countryCode="${country.code}" style="display:${fn:contains(emsCountry,country.code) ? '' : 'none'}">${country.name}</span>
														</c:forEach>
													</div>
												</div>
											</td>
										</tr>
										
										<tr>
											<th class="col-xs-2">
												<div class="col-xs-12">마켓</div>
											</th>
											<td class="col-xs-10">
												<div class="col-xs-3">
													<select id="marketSelector" name="market" multiple>
														<c:forEach var="market"	items="${marketList}">
																<option title="${fn:escapeXml(market.name)}(${market.code})" value="${market.code}" ${fn:contains(emsMarket,market.code) ? 'selected' : ''}>
																	${market.name}(${market.code})
																	- 
																	<c:forEach var="zone" items="${zoneList}">
																		<c:if test="${zoneMarketCodeListMap[zone.code].contains(market.code)}">
																			&lt;i style="font-size:9pt;"&gt;${zoneCodeTable[zone.code].name}&lt;/i&gt;
																		</c:if>
																	</c:forEach>
																</option>
														</c:forEach>
													</select>
												</div>
												<div class="col-md-9">
													<div >
														<!-- 선택된 마켓이 출력됩니다 -->
														<c:forEach items="${marketList}" var="market">
															<span class="label label-success text-sm market-label" marketCode="${market.code}" <c:if test="${!fn:contains(emsMarket,market.code)}">style="display:none"</c:if> >${market.name}</span>
														</c:forEach>
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<th>
												<div class="col-xs-12">노출상태</div>
											</th>
											<td>
												 <div class="row margin form-group">
									                <label>
									                  <input type="radio" name="dispStatus" class="flat-red" value="0" <c:if test="${emsCondition.dispStatus == 0 }"> checked </c:if>> 노출 안함 &nbsp;&nbsp;&nbsp;&nbsp;
									                </label>
									                <label>
									                  <input type="radio" name="dispStatus" class="flat-red" value="1" <c:if test="${emsCondition.dispStatus == 1 }"> checked </c:if>> 내부 노출  &nbsp;&nbsp;&nbsp;&nbsp;
									                </label>
									                <label>
									                  <input type="radio" name="dispStatus" class="flat-red" value="3" <c:if test="${emsCondition.dispStatus == 3 }"> checked </c:if>> 공개 노출 &nbsp;&nbsp;&nbsp;&nbsp;
									                </label>
												</div>
											</td>
										</tr>
										<tr>
											<th>
												<div class="col-xs-12">보상입력</div>
											</th>
											<td>
												<jsp:include page="reward.jsp" flush="true" />	
											</td>
										</tr>
										<tr>
											<th>
												<div class="col-xs-12">언어별<br>내용입력</div>
											</th>
											<td>
											
											<div class="row">
												<div class="col-md-12">
													<div class="box box-solid">
														<div class="box-body">
																
															<c:forEach var="lang" items="${langList}" varStatus="status">
																<div class="box-group" id="accordion">
																	<c:forEach var="zone" items="${zoneList}">
																		<c:if test="${zoneLangCodeListMap[zone.code].contains(lang.code)}">
																			<c:set var="langZoneInfo" value="${langZoneInfo} ${zoneCodeTable[zone.code].name}" scope="request" />
																		</c:if>
																	</c:forEach>
																	
																	<c:forEach var="emsLang" items="${emsLang}">
																		<c:if test="${emsLang.langCode eq lang.shortName}">
																			<c:set var="langMessage" value="${emsLang.message} " scope="request" />
																		</c:if>
																	</c:forEach>
																	
																	<div class="panel box box-primary">
																		<div class="box-header with-border">
																			<h4 class="box-title">
																				<a data-toggle="collapse" data-parent="#accordion" href="#content_${lang.shortName}" class="" aria-expanded="true">
																					${lang.name} - <i style="font-size:10pt;">${langZoneInfo}</i>
																					<span class="content-exists" style="display:${empty langMessage ? 'none' : ''};">*</span>
																				</a>
																			</h4>
																		</div>
																		<div id="content_${lang.shortName}" class="panel-collapse collapse ${status.index == 0 ? "in" : "" }" aria-expanded="true">
																			<div class="box-body">
																				<button class="btn btn-primary btn-xs btn-entire-lang" langShortName="${lang.shortName }" langName="${lang.name}">모든 언어로 적용</button>
																				<button class="btn btn-danger btn-xs btn-content-remove" langShortName="${lang.shortName }" langName="${lang.name}">내용 삭제</button>
																				<div class="form-group margin">
																					<label for="title" class="col-sm-2 control-label">제목</label>
																					<div class="col-sm-10">
																							<input type="text" class="form-control input" name="message" langShortName="${lang.shortName }" maxlength="64" placeholder="제목을 입력해 주세요. (보상 내용 필수) (64자) (필수)" value="${langMessage}" >
																					</div>
																				</div>
																		</div>
																	</div>
																	<c:remove var="langZoneInfo" scope="request" />
																	<c:remove var="langMessage" scope="request" />
																</div>
															</c:forEach>	
															
														</div>
													</div>
												</div>
											</div>
											
											</td>
										</tr>
									</tbody>
								</table>
							</form>
						</div>
						<div class="col-xs-12 text-center">
							<div class="btn_area">
								<button class="btn btn-primary" id="btnRegist">&nbsp;&nbsp;&nbsp;&nbsp;저장&nbsp;&nbsp;&nbsp;&nbsp;</button>
								<button type="button" class="btn btn-default" id="btnList">&nbsp;&nbsp;목록 이동&nbsp;&nbsp;</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- // #noticeWrite -->
		</section>
    <!-- /.content -->
	
</ftt:page>

<script type="text/javascript">
var pageVars = {
		"rewardIdx" : ${not empty emsReward.rewardIdx? emsReward.rewardIdx : 0 },
		"ssn"         : ${ssn},
		"mode"        : "${mode}"
};

var contentVars = {
		"readonly": ${readonly ? 'true' : 'false'},
		"messageIdx": ${not empty emsReward.messageIdx? emsReward.messageIdx : 0 },
		"defaultMessage": "${not empty emsReward.defaultMessage? emsReward.defaultMessage : '' }",
		"rewardSvr": "${rewardSvr}",
		"pubInfoSvr": "${pubInfoSvr}"
};
</script>
<script src="/static/js/mng/form.js?v=1"></script>
