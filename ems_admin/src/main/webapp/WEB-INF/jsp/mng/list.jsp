<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ftt" tagdir="/WEB-INF/tags/"%>
<ftt:page>
    <!-- 페이지명, breadcrumb -->
    <section class="content-header">
		<h1>
			소식통 관리 - <strong>${gameName} </strong>
		</h1>
		<ol class="breadcrumb">
        	<li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        	<li class="active">소식통 관리</li>
      	</ol>
	</section>

	<section class="content">
    	<!-- 페이지 안내 -->
    	<div class="callout callout-danger">
			<p>
				<span class="glyphicon glyphicon-check"></span>&nbsp;&nbsp;상세내용을 확인하시려면 제목을 클릭해주세요
			</p>
		</div>
      	<!-- 목록 -->
      	
      	<c:if test="${empty emsCountry }">
      	<div class="box-header text-right" >
	       	<button class="btn btn-primary" onclick="location.href='form?ssn=${ssn}'">소식통 등록</button>
	    </div>
	    </c:if>
	    
      	<div>
        	<div class="box">
        		
        		<div class="box-header text-right"></div>
        		<div class="box-body">
        		
		         	
		         	<!-- List-->
        			<div class="row">
		          		<div class="col-sm-12 table-responsive">          	
		          			<table class="table table-bordered table-striped table-hover" id="list_area">
		          				<thead>
		          				<tr>
        							<th class="text-center"><label class="control-label">제목</label></th>
        							<th class="text-center"><label class="control-label">언어</label></th>
        							<th class="text-center"><label class="control-label">노출 국가</label></th>
        							<th class="text-center"><label class="control-label">노출 마켓</label></th>
        							<th class="text-center"><label class="control-label">노출 상태</label></th>
		          				</tr>
		          				</thead>
		          				<tbody>
		          				<c:if test="${empty emsCountry }">
		          				<tr>	
		          					<td colspan="5"><div style="padding:2px;text-align:center">등록된 소식통이 없습니다.</div></td>
		          				</tr>
		          				</c:if>
			          			<tr>
			          				
			          				<td>
				          				<c:forEach var="emsLang" items="${emsLang}" begin="0" end="0" step="1" varStatus="status">
											<a href="form?ssn=${ssn }"><div style="padding:2px"><span class="label label-info text-sm">${emsLang.message}</span></div></a>
										</c:forEach>
										
		          					
		          				
				          			</td>
			          				
			          				<td>
				          				<c:forEach var="emsLang" items="${emsLang}">
											<div style="padding:2px"><span class="label label-success text-sm">${langCodeTable[emsLang.langCode].name}</span>
											 </div>
										</c:forEach>
				          			</td>
				          			<td>
				          				<c:forEach var="emsCountry" items="${emsCountry}" begin="0" end="4" step="1" varStatus="status">
											<c:choose>
												<c:when test="${country == '00'}">
													<div style="padding:2px"><span class="label label-success text-sm">기타국가</span></div>
												</c:when>
												<c:otherwise>
													<div style="padding:2px"><span class="label label-success text-sm">${countryCodeTable[emsCountry].name}</span></div>
												</c:otherwise>
											</c:choose>
										</c:forEach>	
										<c:if test="${emsCountry.size() - 5 > 0}">
											외 ${emsCountry.size() - 5}개국
										</c:if>
				          			</td>
				          			
				          			<td>
				          				<c:forEach var="emsMarket" items="${emsMarket}">
											<div style="padding:2px"><span class="label label-success text-sm">${marketCodeTable[emsMarket].name}</span></div>
										</c:forEach>
				          			</td>
				          			<td>
				          				<c:choose>
											<c:when test="${emsCondition.dispStatus == 0}">
												<span class="badge bg-light-blue">노출 안함</span>
											</c:when>
											<c:when test="${emsCondition.dispStatus == 1}">
												<span class="badge bg-orange">내부 노출</span>
											</c:when>
											<c:when test="${emsCondition.dispStatus == 3}">
												<span class="badge bg-red">공개 노출</span>
											</c:when>
										</c:choose>
				          			</td>
			          			</tr>
		          				</tbody>
					        </table>			    
		         		</div>
		         	</div>
		         	
		         	
         		</div>
          		<!-- /.box-body -->
       		</div>
        	<!-- /.box -->
      	</div>
    </section>
    <!-- /.content -->

</ftt:page>
<script type="text/javascript">
	
</script>
