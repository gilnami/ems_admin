<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="ftt" tagdir="/WEB-INF/tags/"%>

<c:set var="readonly" value="false" />

<style>
	#t_msg > thead > tr > th { padding-right:0px; }
	#t_msg > thead th { padding: 0px 0px; }
	#t_msg > thead td { padding: 0px 0px; }
	#d_modal .dataTables_wrapper .dataTables_paginate .paginate_button { padding: 0px 0px; margin: 0px 0px }
</style>

	
<div class="row" style="margin-top:10px;padding-left:10px;">
	<div class="col-xs-12">
		<form id="form" class="form-horizontal">
			<div class="row margin">
				<div class="col-xs-2">
					<label>아이템 문자열</label>
				</div>	
				<div class="col-xs-10">
					<input name="itemStr" id="itemStr" type="text" class="form-control" value="${fn:escapeXml(emsReward.rewardStr)}" placeholder="아이템 문자열"  ${readonly ? 'disabled' : ''}>
				</div>	
			</div>
			<div class="row margin" style="display:none;" id="itemStrInputUi">
				<div class="box box-defalut">
					<div class="box-body">
						<div>
							<table class="table table-bordered itemstr-multiple" style="font-size:9pt;display:none;">
								<thead>
									<tr>
										<th>아이템</th>
										<th width="30">
											<button class="btn btn-default btn-block btn-xs btn-reset-itemstr">전체 삭제</button>
										</th>
									</tr>
								</thead>
								<tbody id="itemStrList" style="display:none;"></tbody>
								<tbody id="itemStrListEmpty">
									<tr>
										<td colspan="2" align="center">아이템 문자열 생성을 위해 등록한 아이템이 없습니다.</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div id="itemStrInputForm"></div>
						<div class="pull-right" style="margin-top:8px;">
							<button class="btn btn-default btn-xs btn-add-itemstr itemstr-multiple" style="display:none;">추가</button>
							<button class="btn btn-default btn-xs btn-create-itemstr">문자열 생성</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row margin" style="display:none;" id="rewardMsgWidgetUi">
				<div class="col-xs-2">
					<label>우편함 메시지</label>
				</div>	
				<div class="col-xs-10" id="rewardMsgWidgetContent">
				</div>
			</div>
			
			<div class="row margin" style="display:none;" id="rewardMsgUi">
				<div class="col-xs-2">
					<label>우편함 메시지</label>
				</div>	
				<div class="col-xs-10">
					<input name="rewardMsg" id="rewardMsg" type="text" class="form-control" value="${fn:escapeXml(emsReward.defaultMessage)}" placeholder="우편함 메시지">
				</div>	
			</div>
		</form>
	</div>
</div>
<script id="tplItemStrInput" type="text/template">
	<div class="row" style="margin-top:10px;">
		<label class="col-xs-2 control-label item-input-title"></label>
		<div class="col-xs-10">
			<input type="text" class="form-control item-input-value" value="">
		</div>
	</div>	
</script>
