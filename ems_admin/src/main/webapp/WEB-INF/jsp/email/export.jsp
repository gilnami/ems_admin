<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ftt" tagdir="/WEB-INF/tags/"%>
<ftt:page>
    <!-- 페이지명, breadcrumb -->
    <section class="content-header">
		<h1>
			E-mail 추출 - ${gameName} 
		</h1>
		<ol class="breadcrumb">
        	<li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        	<li class="active">E-mail 추출</li>
      	</ol>
	</section>

	<section class="content">
    	<!-- 페이지 안내 -->
    	<div class="callout callout-danger">
			<p>
				<span class="glyphicon glyphicon-check"></span>&nbsp;&nbsp;433 소식통은 433 게임을 이용하는 유저들의 E-mail 수집을 위한 시스템 입니다.
			</p>
		</div>
      	<!-- 목록 -->
	    
      	<div>
        	<div class="box box-info">
        		<div class="box-header with-border text-left"><h3 class="box-title">전체 이메일 추출</h3></div>
        		<div class="box-body">
        			<div class="row">
        				<form id="form">
						<table class="table">
							<tbody>
								<tr>
								<th>
									<div class="col-xs-12">국가</div>
								</th>
								<td class="col-xs-10">
									<div class="col-xs-3">
									
										<input type="hidden" id="countryAll" name="countryAll">									
										<input type="hidden" id="langAll" name="langAll">									
										<select id="countrySelector" name="country" multiple>
											<c:if test="${ssn != 0 }">
										    <option title="기타국가(00)" value="00">기타국가(00)</option>
										    </c:if>
											<c:forEach var="country" items="${countryList}">
												<option title="${fn:escapeXml(country.name)}(${country.code})" value="${country.code}" >
													${country.name}(${country.code})
												</option>
											</c:forEach>
											    
										</select>
									</div>
									<div class="col-md-9" style="max-height:200px;overflow-y:scroll">
										<div>
											<!-- 선택된 국가가 출력됩니다 -->
											<c:if test="${ssn != 0 }">	
											<span class="label label-success country-label" countryCode="00" style="display:none">기타국가</span>
											</c:if>
											<c:forEach var="country" items="${countryList}">
												<span class="label label-success country-label" countryCode="${country.code}" style="display:none">${country.name}</span>
											</c:forEach>
										</div>
									</div>
									</td>
								</tr>
								<tr>
								<th>
									<div class="col-xs-12">언어</div>
								</th>
								<td class="col-xs-10">
									<div class="col-xs-3">
										<select id="langSelector" name="language" multiple>
											<c:forEach var="language" items="${langList}">
												<option title="${fn:escapeXml(language.name)}(${language.shortName})" value="${language.shortName}" >
													${language.name}(${language.shortName})
												</option>
											</c:forEach>
											    
										</select>
									</div>
									<div class="col-md-9" style="max-height:200px;overflow-y:scroll">
										<div>
											<!-- 선택된 언어가 출력됩니다 -->
											<c:forEach var="language" items="${langList}">
												<span class="label label-success lang-label" langCode="${language.shortName}" style="display:none">${language.name}</span>
											</c:forEach>
										</div>
									</div>
									</td>
								</tr>
							</tbody>
						</table>
						</form>
        			</div>
		         	<div class="col-xs-12 text-center">
						<div class="btn_area">
							<button class="btn btn-primary" id="btnExport">&nbsp;&nbsp;추출&nbsp;&nbsp;</button>
						</div>
					</div>
         		</div>
          		<!-- /.box-body -->
       		</div>
       		
       		<form id="targetForm" name="targetForm" method="post" enctype="multipart/form-data" action="/email/export-target">
       		<input type="hidden" id="ssn" name="ssn" value="${ssn }">
       		<div class="box box-warning">
        		<div class="box-header with-border text-left">
        		<h3 class="box-title"> 조건별 이메일 추출</h3></div>
        		
        		<div class="box-body">
        		
        			<div class="callout callout-success bg-gray">
						<p>
							  <span class="glyphicon glyphicon-check"></span>&nbsp;&nbsp;
							   파일은 **.xlsx, **.csv 만 가능합니다.<br/>
							   <span class="glyphicon glyphicon-check"></span>&nbsp;&nbsp;
							   업로드 파일은 게임 Account ID 입니다.  <br/>
							   <span class="glyphicon glyphicon-check"></span>&nbsp;&nbsp;
							   파일 한개당 최대 100만건까지 입력 가능합니다. <br/>
							   <span class="glyphicon glyphicon-check"></span>&nbsp;&nbsp;
							   <a href="/static/file/upload_sample.xlsx">샘플 다운로드</a>
						</p>
					</div>
        			<div class="row">
        				
						
						<table class="table">
							<tbody>
								<tr>
								<th>
									<div class="col-xs-12">파일 선택</div>
								</th>
								<td class="col-xs-10">
									<div class="col-xs-3">
                    				<input type="file" id="attach" name="attach">
                    				</div>
								</td>
								</tr>
							</tbody>
						</table>
        			</div>
		         	<div class="col-xs-12 text-center">
						<div class="btn_area">
							<input type="button" class="btn btn-primary" id="btnTargetExport" value="&nbsp;&nbsp;추출&nbsp;&nbsp;">
						</div>
					</div>
         		</div>
          		<!-- /.box-body -->
       		</div>
       		</form>
        	<!-- /.box -->
      	</div>
    </section>
    <!-- /.content -->

</ftt:page>
<script type="text/javascript">

var _export = (function(){
	
	var initCountry = function(){
		$("#countrySelector").multiselect({
			numberDisplayed: 0,
			maxHeight: 300,
			//allSelectedText: "",
			includeSelectAllOption: true,
			enableHTML: true,
			selectAllText: "전체 선택",
			selectAllValue: "99",
			nonSelectedText: "-- 국가 선택 --",
			selectAllJustVisible: true,
			enableCaseInsensitiveFiltering: true,
			buttonTitle: function(options, select) {
				var labels = [];
				options.each(function () {
					labels.push($(this).attr("title"));
				});
				return labels.join(', ');
			},
			onChange: function() {
				$("span.country-label").hide();
				$("#countryAll").val("");
				var selectedCountryList = $("#countrySelector").val();
				if (selectedCountryList) {
					for (var i = 0; i < selectedCountryList.length; i++) {
						$("span.country-label[countryCode=" + selectedCountryList[i] + "]").show();
					}
				}
			},
			onSelectAll: function(selected) {
				$("span.country-label").hide();
				$("#countryAll").val("");
				if(selected){
					$("#countryAll").val("99");
				}
				var selectedCountryList = $("#countrySelector").val();
				if (selectedCountryList) {
					for (var i = 0; i < selectedCountryList.length; i++) {
						$("span.country-label[countryCode=" + selectedCountryList[i] + "]").show();
					}
				}
			}
		});
	};
	var initLang = function(){
		$("#langSelector").multiselect({
			numberDisplayed: 0,
			maxHeight: 300,
			//allSelectedText: "",
			includeSelectAllOption: true,
			enableHTML: true,
			selectAllText: "전체 선택",
			selectAllValue: "99",
			nonSelectedText: "-- 언어 선택 --",
			selectAllJustVisible: true,
			enableCaseInsensitiveFiltering: true,
			buttonTitle: function(options, select) {
				var labels = [];
				options.each(function () {
					labels.push($(this).attr("title"));
				});
				return labels.join(', ');
			},
			onChange: function() {
				$("span.lang-label").hide();
				$("#langAll").val("");
				var selectedLangList = $("#langSelector").val();
				if (selectedLangList) {
					for (var i = 0; i < selectedLangList.length; i++) {
						$("span.lang-label[langCode=" + selectedLangList[i] + "]").show();
					}
				}
			},
			onSelectAll: function(selected) {
				$("span.lang-label").hide();
				$("#langAll").val("");
				if(selected){
					$("#langAll").val("99");
				}
				var selectedLangList = $("#langSelector").val();
				if (selectedLangList) {
					for (var i = 0; i < selectedLangList.length; i++) {
						$("span.lang-label[langCode=" + selectedLangList[i] + "]").show();
					}
				}
			}
		});
	};
	
	
	var initUI = function(){
		if ($("#countrySelector option").length == 1) {
			$("#countrySelector").multiselect("selectAll", false);
			$("#countrySelector").multiselect('refresh');			
		}
		if ($("#langSelector option").length == 1) {
			$("#langSelector").multiselect("selectAll", false);
			$("#langSelector").multiselect('refresh');			
		}
	};	
	
	var getForm = function(){
		var data = {};
		data.ssn = $("#ssn").val();
		data.country = ($("#countryAll").val()=="") ? $("#countrySelector").val() : [$("#countryAll").val()];
		data.lang = ($("#langAll").val()=="") ? $("#langSelector").val() : [$("#langAll").val()];
		return data;
	};
	
	var validateData = function(data){
		/* if(data.ssn == 0){
			alert("추출할 게임을 선택해주세요");
			return false;
		} */
		if(data.country == null && data.lang == null){
			alert("추출할 국가 또는 언어를 선택해 주세요");
			return false;
		}
		
		return true;
	};
	
	var validTargetData = function(){
		var ssn = $("#ssn").val();
		/*
		if(ssn == "" || ssn == "0"){
			alert("게임을 선택하세요");
			return false;
		}*/
		
		var fileName = $("#attach").val();
		if(fileName == ""){
			alert("파일을 선택해주세요");
			return false;
		}
		var fileExt = fileName.slice(fileName.indexOf(".") + 1).toLowerCase();

		if(fileExt != "xlsx" && fileExt != "xls" && fileExt != "csv"){
			alert("*.xlsx, *.csv 파일만 업로드 가능합니다.");
			return false;
		}
		
		return true;
	};
	
	return {
		init : function(){
			initCountry();
			initLang();
			initUI();
		},
		
		send : function(){
			var data = getForm();
			if(!validateData(data)) return false;
			if(!confirm("전체 이메일을 추출하시겠습니까?")) return false;
			var country = (data.country == null) ? "" : data.country.join(",");
			var lang = (data.lang == null) ? "" : data.lang.join(",");
			location.href = '/email/export-all?ssn='+data.ssn+'&country='+country+'&lang='+lang;
		},
		
		sendTarget : function(){
			if(!validTargetData()){
				return false;
			}
			if(!confirm("조건별 이메일을 추출하시겠습니까?")){
				return false;
			}
			$("#targetForm").submit();
		}
	}
})();

$("#document").ready(function(){
	_export.init();
	
	//전체 이메일 추출 버튼
	$("#btnExport").click(function(){
		_export.send();	
	});
	
	//조건별 이메일 엑셀파일 업로드 버튼
	$("#btnTargetExport").click(function(){
		_export.sendTarget();
	});
	
	
});

</script>
