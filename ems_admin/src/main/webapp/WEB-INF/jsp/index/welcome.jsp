<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ftt" tagdir="/WEB-INF/tags/"%>
<ftt:page>
    <!-- 페이지명, breadcrumb -->
    <section class="content-header">
		<h1>
			환영합니다!
		</h1>
	</section>

	<section class="content">
    	<!-- 페이지 안내 -->
      	<!-- 목록 -->
      	<div id="manual" class="clearfix">
			<div class="wrap_manual">
				<iframe src="/static/file/ems_guide.pdf" width="1024" height="724" allowfullscreen webkitallowfullscreen></iframe>
			</div>
		</div>
    </section>
    <!-- /.content -->

</ftt:page>
<script type="text/javascript">

</script>
