<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ftt" tagdir="/WEB-INF/tags/"%>
<ftt:page>
    <!-- 페이지명, breadcrumb -->
    <section class="content-header">
    	<h1>
      		[게임선택]
        	<small>게임을 선택하세요</small>
    	</h1>
      	<ol class="breadcrumb">
        	<li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        	<li class="active">게임 선택</li>
      	</ol>
    </section>
    <!-- 메인컨텐츠 영역 -->
    <section class="content">
    	<!-- 페이지 안내 -->
		<div class="callout callout-danger">
			<p>
				<span class="glyphicon glyphicon-check"></span>&nbsp;&nbsp;433 소식통은 433 게임을 이용하는 유저들의 E-mail 수집을 위한 시스템 입니다.
			</p>
			<p>
				<span class="glyphicon glyphicon-check"></span>&nbsp;&nbsp;소식통에 세팅된 국가/언어/마켓 등이 변경되었다면, “퍼블리싱 정보 관리” 메뉴에서 설정해 주세요.
			</p>
		</div>
		
		<div class="row" style="margin-top:10px;padding-left:10px;">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">게임을 선택 하세요.</h3>
						<div class="box-tools">
							<div class="input-group input-group-sm" style="width: 150px;">
								<input type="text" id="search" class="form-control pull-right" placeholder="Search">
								<div class="input-group-btn">
									<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
								</div>
							</div>
						</div>
					</div>
					<div class="box-body">
						<br />
						<div class="col-xs-12">
							<ul class="list-inline">
								<c:forEach var="pubInfoGame" items="${gameList}" varStatus="status">
									<li class="game-list-item">
										<a href="${path}?ssn=${pubInfoGame.ssn}">
											<span class="info-box" style="width:240px;box-shadow:1px 1px 1px 1px rgba(0,0,0,0.1)">
												<span class="info-box-icon bg-white" style="padding:0;">
													<img src="${pubInfoGame.appIcon}" class="title-appicon" style="width:100%;height:100%;vertical-align:top;" />
												</span>
												<span class="info-box-content text-left game-content" style="padding-top:20px;">
													<span class="info-box-text" style="padding-left:4px"><strong>${pubInfoGame.name}</strong></span>
													<span class="info-box-text" style="padding-left:4px;font-size:9pt;">SSN: ${pubInfoGame.ssn}</span>
												</span>
											</span>
										</a>
									</li>
								</c:forEach>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
    </section>
    <!-- /.content -->
	
</ftt:page>
<script src="<c:url value='/static/js/gameList.js'/>"></script>
<script type="text/javascript">

</script>
