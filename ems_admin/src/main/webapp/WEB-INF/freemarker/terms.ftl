<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>4:33 소식통 개인정보 수집 및 이용 동의 안내</title>
</head>
<body>

1. 수집하는 개인정보의 항목 <br>
- 이메일 주소 <br>
2. 개인정보의 수집 및 이용 목적 <br>
- 서비스의 변경사항 알림 및 별도 혜택 제공, 신규 서비스를 위한 마케팅, 광고 등 <br>
3. 개인정보의 보유 및 이용 기간<br>
- 회원 탈퇴 또는 서비스 종료 시까지<br>
4. 제3자 정보 제공 동의<br>
(필수) 메일 수신을 위해 제3자 정보 제공에 동의 합니다.<br>
<table border="1" cellpadding="0" cellspacing="0">
<tr>
<td>개인정보를 제공받는 자</td><td>처음소프트㈜</td>
</tr>
<tr>
<td>개인정보를 제공받는 자의 개인정보 이용목적 </td><td>이메일 발송</td>
</tr>
<tr>
<td>제공하는 개인정보의 항목	</td><td>이메일 주소</td>
</tr>
<tr>
<td>개인정보를 제공받는 자의 개인정보 보유 및 이용기간	 </td><td>발송 확인 후 파기</td>
</tr>
</table>
<br>


입력하신 메일 주소는 소식통 메일 발송 외에 다른 용도로 사용되지 않습니다.<br>
감사합니다.<br>
본 메일은 발신전용 메일로 회신이 불가능 합니다.<br>
Copyright ⓒ4:33 Creative Lab. All rights reserved.
</body>
</html>