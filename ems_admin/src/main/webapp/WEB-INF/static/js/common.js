function loadRewardMsg(ssn, rewardMsgId, callback) {
	if (ssn <= 0 || rewardMsgId <= 0) {
		callback(null);
		return;
	}

	var url  = globalConfig.rewardSvr + "/API/MessageGroup.aspx"
	var data = {
		"action": "GetList",
		"ssn":    ssn,
		"gid":    rewardMsgId
	};
	
	$.getJSON(url, data, function(resp) {
		if (resp && resp.data) {
			var rewardMsgMap = {};
			resp.data.forEach(function(msg) {
				rewardMsgMap[msg.Lang] = msg.Msg;
			});
			
			callback(rewardMsgMap);
		} else {
			callback(null);
		}
	});
}

function createRewardMsg(ssn, rewardMsgMap, callback) {
	if (ssn <= 0 || rewardMsgMap == null) {
		callback(0);
		return;
	}
	
	var url  = globalConfig.rewardSvr + "/API/MessageGroup.aspx"
	var data = {
		"action": "Create",
		"ssn": ssn,
		"messages": JSON.stringify(rewardMsgMap),
		"gid": 0
	};
	
	$.post(url, data, function(resp, status) {
		if (resp && resp.code == 0) {
			callback(resp.data);
		} else {
			callback(0);
		}
	});
}

function copyPush(ssn, pushNo, newRewardMsgId, callback) {
	$.ajax({
		"url":        "/ajax/push/copy?ssn=" + ssn + "&push_no=" + pushNo +"&reward_msg_id=" + newRewardMsgId,
		"type":       "PUT",
		"data":       null,
		"dataType":   "json",
		"cache":       false,
		"contentType": 'application/json; charset=utf-8',
		"processData": false,
		"complete": function() {
		},
		"success": function(resp) {
			if (resp && resp.code == 0) {
				console.log(resp);
				callback(resp.data.pushNo);
				return;
			}
			
			if (resp && resp.message) {
				alert(resp.message)
				return;
			}
			
			alert("재사용 실패");
		},
		"error": function() {
			alert("재사용 실패");
		}
	});	
}

function sendTestPush(ssn, pushNo, langs, uids) {
	var data = {
		"langs": langs,
		"uids": uids
	};
	
	$.ajax({
		"url":        "/ajax/test/send?ssn=" + ssn + "&push_no=" + pushNo,
		"type":       "POST",
		"data":       JSON.stringify(data),
		"dataType":   "json",
		"cache":       false,
		"contentType": 'application/json; charset=utf-8',
		"processData": false,
		"complete": function() {
		},
		"success": function(resp) {
			if (resp) {
				if (resp.code == 0) {
					alert("테스트 발송 완료");
					return;
				} else if (resp.message) {
					alert("테스트 발송 실패 (" +resp.message + ")");
					return;
				}
			}
			alert("테스트 발송 실패");
		},
		"error": function() {
			alert("테스트 발송 실패");
		}
	});
}

function getNextSendTime(repeatType, tz, date, time, days, callback) {
	if (repeatType == 0) {
		var data = {
			"tz": tz,	
			"date": date,	
			"time": time
		};
		
		$.getJSON("/ajax/schedule/calc", data, function(resp) {
			if (resp && resp.code == 0) {
				if (resp.data && resp.data.length > 0) {
					var tzOffset = Object.keys(resp.data[0])[0];
					callback(tzOffset, resp.data[0][tzOffset]);
					return;
				}
			}
			
			callback("ERR", null);
		});
	} else if (repeatType == 2) {
		var data = {
			"tz": tz,	
			"date": date,	
			"time": time,
			"days": days
		};
		
		$.getJSON("/ajax/schedule/calc/repeat_week", data, function(resp) {
			if (resp && resp.code == 0) {
				if (resp.data && resp.data.length > 0) {
					var tzOffset = Object.keys(resp.data[0])[0];
					callback(tzOffset, resp.data[0][tzOffset]);
					return;
				}
			}
			
			callback("ERR", null);
		});
	}
}

function uuid() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
		return v.toString(16);
	});
};



