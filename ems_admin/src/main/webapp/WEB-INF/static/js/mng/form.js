
var _mng = (function(){
	
	var initZone = function(){
		$("#zoneSelector").multiselect({
			numberDisplayed: 0,
			maxHeight: 300,
			includeSelectAllOption: true,
			selectAllText: "전체 선택",
			selectAllValue: "99",
			nonSelectedText: "-- 서비스지역(서버) 선택 --",
			selectAllJustVisible: true,
			enableCaseInsensitiveFiltering: false,
			onChange: function() {
				$("span.zone-label").hide();
				
				var selectedZoneList = $("#zoneSelector").val();
				if (selectedZoneList) {
					for (var i = 0; selectedZoneList && i < selectedZoneList.length; i++) {
						$("span.zone-label[zoneCode=" + selectedZoneList[i] + "]").show();
					}
				}
			},
			onSelectAll: function(selected) {
				if (selected) {
					$("span.zone-label").show();
				} else {
					$("span.zone-label").hide();
				}
			}
		});
	};
	
	var initCountry = function(){
		$("#countrySelector").multiselect({
			numberDisplayed: 0,
			maxHeight: 300,
			//allSelectedText: "",
			includeSelectAllOption: true,
			enableHTML: true,
			selectAllText: "전체 선택",
			selectAllValue: "99",
			nonSelectedText: "-- 국가 선택 --",
			selectAllJustVisible: true,
			enableCaseInsensitiveFiltering: true,
			buttonTitle: function(options, select) {
				var labels = [];
				options.each(function () {
					labels.push($(this).attr("title"));
				});
				return labels.join(', ');
			},
			onChange: function() {
				$("span.country-label").hide();
				
				var selectedCountryList = $("#countrySelector").val();
				if (selectedCountryList) {
					for (var i = 0; i < selectedCountryList.length; i++) {
						$("span.country-label[countryCode=" + selectedCountryList[i] + "]").show();
					}
				}
			},
			onSelectAll: function(selected) {
				$("span.country-label").hide();
				
				var selectedCountryList = $("#countrySelector").val();
				if (selectedCountryList) {
					for (var i = 0; i < selectedCountryList.length; i++) {
						$("span.country-label[countryCode=" + selectedCountryList[i] + "]").show();
					}
				}
			}
		});
	};
	
	var initMarket = function(){
		$("#marketSelector").multiselect({
			numberDisplayed: 0,
			maxHeight: 300,
			includeSelectAllOption: true,
			enableHTML: true,
			selectAllText: "전체 선택",
			selectAllValue: "99",
			nonSelectedText: "-- 마켓 선택 --",
			selectAllJustVisible: true,
			enableCaseInsensitiveFiltering: true,
			buttonTitle: function(options, select) {
				var labels = [];
				options.each(function () {
					labels.push($(this).attr("title"));
				});
				return labels.join(', ');
			},
			onChange: function() {
				$("span.market-label").hide();
				
				var selectedMarketList = $("#marketSelector").val();
				if (selectedMarketList) {
					for (var i = 0; i < selectedMarketList.length; i++) {
						$("span.market-label[marketCode=" + selectedMarketList[i] + "]").show();
					}
				}
			},
			onSelectAll: function(selected) {
				$("span.market-label").hide();
				
				var selectedMarketList = $("#marketSelector").val();
				if (selectedMarketList) {
					for (var i = 0; i < selectedMarketList.length; i++) {
						$("span.market-label[marketCode=" + selectedMarketList[i] + "]").show();
					}
				}
			}
		});
		
	};
	
	var initUI = function(){
		if ($("#zoneSelector option").length == 1) {
			$("#zoneSelector").multiselect("selectAll", false);
			$("#zoneSelector").multiselect('refresh');	
		}
		
		if ($("#countrySelector option").length == 1) {
			$("#countrySelector").multiselect("selectAll", false);
			$("#countrySelector").multiselect('refresh');			
		}
		
		if ($("#marketSelector option").length == 1) {
			$("#marketSelector").multiselect("selectAll", false);
			$("#marketSelector").multiselect('refresh');			
		}
		//Flat red color scheme for iCheck
	    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
	      checkboxClass: 'icheckbox_flat-green',
	      radioClass   : 'iradio_flat-green'
	    })

	};
	
	var initReward = function(){
		
		var rewardFmtUrl = contentVars.rewardSvr + "/api/itemstring.aspx"; 
		var rewardFmtUrlData = {"action": "GetInfo", "ssn": pageVars.ssn};
		$.getJSON(rewardFmtUrl, rewardFmtUrlData, function(resp) {
			if (resp == null || resp.state != 0 || resp.data == "")	return;
			renderItemStrInputForm(JSON.parse(resp.data));
		});

		var rewardMsgChkUrl = contentVars.rewardSvr + "/API/MessageGroup.aspx";
		var rewardMsgChkUrlData = {"action": "IsEnableLangGroup", "ssn": pageVars.ssn}
		$.getJSON(rewardMsgChkUrl, rewardMsgChkUrlData, function(resp) {
			if (resp == null || resp.code != 0) {
				alert("리워드 메시지 정보를 가져오는데 실패 했습니다.");
				return;
			}
			
			if (resp.data) {
				pageVars.wg = $("#rewardMsgWidgetContent").gmsg({
						ssn: pageVars.ssn,
						mgid: contentVars.messageIdx,
						apiPath: contentVars.rewardSvr+"/API/MessageGroup.aspx",
						pubapiPath: contentVars.pubInfoSvr + "/api",
						defaultMessage: contentVars.defaultMessage
					});
				$("#rewardMsgWidgetUi").show();
				$("#rewardMsgUi").hide()
			}else{
				$("#rewardMsgUi").show()
			}
		});
	};
	
	//ajax 전송 데이터 생성
	var getFormData = function(){
		var formData = {};
		formData.ssn = pageVars.ssn;
		formData.zone = $("#zoneSelector").val();
		formData.country = $("#countrySelector").val();
		formData.market = $("#marketSelector").val();
		formData.dispStatus = $('input[name=dispStatus]:radio:checked').val();
		
		//아이템 설정
		formData.rewardIdx = pageVars.rewardIdx;
		formData.rewardStr = $("#itemStr").val();
		
		if (pageVars.wg) {
			formData.messageIdx = pageVars.wg.gmsg("getData").mgid;
			formData.defaultMessage = pageVars.wg.gmsg("getData").defaultMessage;
		}else{
			formData.defaultMessage = $.trim($("#form input[name=rewardMsg]").val());
		}
		
		if(contentVars.messageIdx > 0) {
			formData.messageIdx = contentVars.messageIdx;	
		}
		
		//노출 메세지
		formData.message = new Array();
		$("input[name=message]").each(function(){
			var langCode = $(this).attr("langShortName");
			var message = $(this).val();
			formData.message.push({"langCode":langCode,"message":message});
		});
		
		return formData;
	};
	
	var validateData = function(data){
		if(data.zone == null) {
			alert("서비스지역(서버)을 선택해 주세요");
			return false;
		}else if(data.country == null){
			alert("노출 국가를 선택해 주세요");
			return false;
		}else if(data.market == null){
			alert("마켓을 선택해 주세요");
			return false;
		}else if(data.dispStatus == undefined){
			alert("노출상태를 체크해주세요.");
			return false;
		}else if(data.rewardStr == ""){
			alert("아이템 문자열을 입력해주세요.");
			return false;
		}
		
		for(var i=0;i<data.message.length;i++){
			if(data.message[i].message == ""){
				alert("언어별 내용을 입력해주세요");
				return false;
			}
			if(data.message[i].message.length > 64){
				alert("제목은 64자 이하로만 가능합니다.");
				return false;
			}
		} 
		return true;		
	}
	
	return {
		init : function(){
			initZone();
			initCountry();
			initMarket();
			initUI();
			initReward();
			
			if (pageVars.mode == "modify") {
				$("#countrySelector").multiselect("select", "00");
				$("span.country-label[countryCode=00]").show();
				$("#selectedCountryList").find("span[countryCode=00]").show();
			}
		},
		
		goList : function(){
			location.href = "list?ssn="+pageVars.ssn;
		},
		
		regist : function(){

			var formData = getFormData();
			if(!validateData(formData)) return false;
			if(!confirm("저장하시겠습니까?")) return false;
			
			var url = (pageVars.mode == "modify") ? "/ajax/mng/modify" : "/ajax/mng/add";
			
			$.ajax({
				url : url,
				type: 'POST',
				contentType: "application/json",
			    data : JSON.stringify(formData),
				dataType: 'json',
				success: function(resp) {
					alert("완료되었습니다.");
					_mng.goList();
				},
				error: function() {
					alert('시스템 에러가 발생하였습니다.');
				}
			});
			
 		}
	}	
})();


$("#document").ready(function(){
	
	//초기화
	_mng.init();
	
	//저장 버튼 
	$("#btnRegist").click(function(e){
		_mng.regist();
	});
	
	//목록 버튼
	$("#btnList").click(function(e){
		_mng.goList();
	});
	
	// 모든 언어에 동일하게 적용
	$("button.btn-entire-lang").click(function() {
		var langShortName    = $(this).attr("langShortName");
		var langName    = $(this).attr("langName");
		var message = "";
		
		if (!confirm(langName + " 언어로 모든 언어에 적용 됩니다.")) {
			return false;
		}
		
		$("input[name=message]").each(function(){
			var shortName = $(this).attr("langShortName");
			if(shortName == langShortName){
				message = $(this).val();
				return;
			}
		});
		
		$("input[name=message]").each(function(){
			$(this).val(message);
		});
		
		if (message == "") {
			$("span.content-exists").hide();
		} else {
			$("span.content-exists").show();
		}
		
		return false;	
	});

	
	// 내용 삭제
	$("button.btn-content-remove").click(function() {
		
		var langShortName    = $(this).attr("langShortName");
		var langName    = $(this).attr("langName");
		
		if (!confirm(langName + " 언어의 내용이 삭제 됩니다.")) {
			return false;
		}	
		
		$("input[name=message]").each(function(){
			var shortName = $(this).attr("langShortName");
			if(shortName == langShortName){
				$(this).val("");
				return;
			}
		});
		
		return false;
	});
	
	//아이템 문자열 전체 삭제
	$("button.btn-reset-itemstr").click(function() {
		$("#itemStrList").html("");
		$("#itemStrList").hide();
		$("#itemStrListEmpty").show();
		return false;
	});
	
	//아이템 문자열 추가 
	$("button.btn-add-itemstr").click(function() {
		var item = getItemFromItemStrInputForm();
		if (item == null) return false;
		
		var $html = $('<tr><td class="itemstr"></td><td><button class="btn btn-default btn-block btn-xs">삭제</button></td></tr>');
	
		$html.find("td.itemstr").text(JSON.stringify(item));
		$html.find("button").click(function() {
			$html.remove();
			
			if ($("#itemStrList tr").length == 0) {
				$("#itemStrList").hide();
				$("#itemStrListEmpty").show();
			}
			return false;
		});
		
		$("#itemStrList").append( $html)
		$("#itemStrInputForm input.item-input-value").val("");
		
		$("#itemStrList").show();
		$("#itemStrListEmpty").hide();	
		
		return false;
	});
	
	//아이템 문자열 생성
	$("button.btn-create-itemstr").click(function() {
		var items = [];
		
		try {
			$("#itemStrList td.itemstr").each(function() {
				var item = JSON.parse($(this).text());
				items.push(item);
			});
		} catch (e) {
		}
		
		if (items.length == 0) {
			var item = getItemFromItemStrInputForm();
			if (item != null) items.push(item);
		}
	
		if (items.length == 0) {
			alert("등록한 아이템이 없습니다.");
			return false;
		}
		var rewardGenUrl =  contentVars.rewardSvr + "/api/itemstring.aspx"; 
		 
		var rewardGenUrlData = {"action": "GetItemString", "ssn": pageVars.ssn, "items": JSON.stringify(items)};
		$.getJSON(rewardGenUrl, rewardGenUrlData, function(resp) {
			
			if (resp.state == 0 && resp.data) {
				$("input[name=itemStr]").val(resp.data);
			}
		});
		
		return false;
	});
	
});

function getItemFromItemStrInputForm() {
	var item = {};
	var keyCount = 0;
	
	$("#itemStrInputForm input.item-input-value").each(function() {
		keyCount++;
		var key = $(this).attr("key");
		var value = $(this).val();
		if (value == "") return;
		item[key] = value;
	});
	
	if (Object.keys(item).length != keyCount) {
		return null;
	}
	
	return item;
}

function renderItemStrInputForm(fmtData) {
	if (fmtData == null) return;
	
	var keys = null;
	if (fmtData.Keys) {
		keys = fmtData.Keys.split(",");
	}
	
	var keyNames = null;
	if (fmtData.keyNames) {
		keyNames = fmtData.keyNames.split(",");
	}
	
	if (keys) {
		var idx = 0;
		keys.forEach(function(key) {
			var keyName;
		
			if (keyNames) {
				keyName = keyNames[idx];
			} else {
				keyName = key;
			}
			
			var $tpl = $($("#tplItemStrInput").html());
			$tpl.find("label.item-input-title").text(keyName);
			$tpl.find("input.item-input-value").attr("key", key);
			$("#itemStrInputForm").append($tpl);
			
			idx++;
		});
	}
	
	if (fmtData.UseMulti) {
		$(".itemstr-multiple").show();
	}
	
	$("#itemStrInputUi").show();
}	