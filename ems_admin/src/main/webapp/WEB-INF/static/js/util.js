  
(function() {
	    var _oldAjax = $.ajax;
	    $.ajax = function(options) {
	        $.extend(options, {
	        	beforeSend: function () {
	          	 $('#_loading').modal('show');
	            },
	            complete: function (result) {
	          	  $('#_loading').modal('hide');
	            }
	        });
	        return _oldAjax(options);
	     };
})();



function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
    .substr(1)
        .split("&")
        .forEach(function (item) {
        tmp = item.split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    });
    return result;
}