$(function() {
	attachEvent();
});

function attachEvent() {
	$("img.title-appicon").on("error", function(evt) {
		$(this).attr("src", "http://backoffice.four33.co.kr/img/icon_999.jpg");
	});
	
	$("#search").on("keyup", function(evt) {
		$("li.game-list-item").each(function() {
			var keyword = $("#search").val().trim();
			var regexp = new RegExp(keyword, "ig");
			var container = $(this);
			
			if (keyword != "") {
				$(this).find("span.game-content").each(function() {
					if (regexp.test($(this).text())) {
						container.show();
					} else {
						container.hide();
					}
				});
			} else {
				$(this).show();
			}
		});
	});
}