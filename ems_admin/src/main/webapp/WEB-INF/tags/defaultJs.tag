<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<spring:eval expression="@environment.getProperty('ftt.domain.static')" var="staticDomain"/>

<!-- jQuery 2.2.3 -->
<script src="/static/common/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Bootstrap 3.3.6 -->
<script src="/static/common/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="/static/common/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="/static/common/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="/static/common/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/static/common/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="/static/common/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="/static/common/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="/static/common/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="/static/common/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="/static/common/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/static/common/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/static/common/dist/js/app.min.js"></script>
<!-- smart editor2 -->
<script type="text/javascript" src="/static/common/plugins/smarteditor2/js/service/HuskyEZCreator.js"></script>
<!-- dataTables -->
<script src="/static/common/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/static/common/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="/static/common/plugins/tagsinput/bootstrap-tagsinput.min.js"></script>
<script src="/static/common/plugins/bootstrap-fileinput/plugins/sortable.js"></script>
<script src="/static/common/plugins/bootstrap-fileinput/fileinput.js"></script>
<script src="/static/common/plugins/bootstrap-fileinput/locales/kr.js"></script>
<script src="/static/common/plugins/bootstrap-fileinput/themes/explorer/theme.js"></script>
<script src="/static/common/plugins/select2/select2.js"></script>
<script src="/static/common/plugins/iCheck/icheck.min.js"></script>

<!--  <script src="/static/js/util.js"></script>  -->
<script src="//${staticDomain}/service/push3admin/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>
<script src="//${staticDomain}/common/js/module/widget/ftt.gmsg2.js"></script>

