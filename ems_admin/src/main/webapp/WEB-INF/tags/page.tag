<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="ftt" tagdir="/WEB-INF/tags/"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<spring:eval expression="@environment.getProperty('ftt.domain.static')" var="staticDomain" />
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />

<title>433 소식통 관리</title>

<ftt:defaultCss />
<ftt:defaultJs />
</head>
<body class="hold-transition skin-purple sidebar-mini">
	<div class="wrapper">
		<ftt:pageLayout>
			<jsp:doBody />
		</ftt:pageLayout>
	</div>
</body>
</html>