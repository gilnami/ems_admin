<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="menuId" required="false" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<s:eval expression="@environment.getProperty('ems_admin.auth.addr')" var="authAddr" />
<s:eval expression="@environment.getProperty('ftt.domain.static')" var="staticDomain"/>

<script src="${authAddr}/user/gnb2"></script>

<!-- Main Header -->
<header class="main-header">
	<!-- 사이트 로고 -->
	<a href="/" class="logo">
		<span class="logo-mini"><b>소식통</b></span>
		<span class="logo-lg"><b>&nbsp;&nbsp;&nbsp;&nbsp;433 소식통</b></span>
	</a>
	<!-- 상단 네비 -->
	<nav class="navbar navbar-static-top">
		<!-- 메뉴 토글버튼 -->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		  <span class="sr-only">Toggle navigation</span>
		</a>
		<!-- 로그인 시 사용자 메뉴 -->
		<div class="navbar-custom-menu">
	  		<ul class="nav navbar-nav">
		  	<!-- Messages: style can be found in dropdown.less-->
	      		<li class="name-menu d-secOnline hidden">
	        		<a href="#"><u class="d-pNickname"></u>님</a>
	      		</li>
	      		<li class="logout-menu d-secOnline hidden">
	        		<a href="#" class="d-btnLogout">로그아웃</a>
	      		</li>
	      		<li class="logout-menu d-secOffline hidden">
	        		<a href="#" class="d-btnLogin">로그인</a>
	      		</li>
	    	</ul>
	  	</div>
	</nav>
</header>

<!-- 좌측 사이드 메뉴 -->
<aside class="main-sidebar">
      <c:choose>
		<c:when test="${not empty menuItem || ssn > 0}">
			<select id="gameSelector">
				<c:if test="${ssn == 0}">
					<option>게임을 선택해주세요.</option>
				</c:if>
				<option ${0 == ssn ? 'selected' : ''} value="${0}">전체</option>
				<c:forEach var="pubInfoGame" items="${gameList}" varStatus="status">
					<option ${pubInfoGame.ssn == ssn ? 'selected' : ''} value="${pubInfoGame.ssn}">${pubInfoGame.name}(${pubInfoGame.ssn})</option>
				</c:forEach>
			</select>
		</c:when>
		<c:otherwise>
			<select style="width:200px" id="gameSelector">
				<option>메뉴를 선택해주세요.</option>
			</select>
		</c:otherwise>
	</c:choose>
	<section class="sidebar">
    <ul class="sidebar-menu">
    	
      	<!-- 현재 페이지의 메뉴에 class="active" 추가 -->
      	<li <c:if test="${menuItem == '/mng' }">class="active"</c:if> >
        	<a href="/mng/list?ssn=${ssn }">
          		<i class="fa fa-star"></i>
          		<span>소식통 관리</span>
       		</a>
      	</li>
      	<li <c:if test="${menuItem == '/email' }">class="active"</c:if>>
	        <a href="/email/export-form?ssn=${ssn }">
	          	<i class="fa fa-tags"></i>
	          	<span>E-mail 추출</span>
	        </a>
      	</li>
      	
      	<li <c:if test="${menuItem == '/member' }">class="active"</c:if>>
	        <a href="/member/list?ssn=${ssn }">
	          	<i class="fa fa-tags"></i>
	          	<span>사용자 조회</span>
	        </a>
      	</li>

      	
    </ul>
  	</section>
  	<!-- /.sidebar -->
</aside>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<jsp:doBody />
</div>
<!-- /.content-wrapper -->

<!-- footer -->
<footer class="main-footer">
	<div class="pull-right hidden-xs"></div>
	<strong>Copyright &copy; 2017 4:33</strong> All rights reserved.
</footer>


<div class="modal fade" id="_loading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-body">
		<div class="box box-danger">
          <div class="box-header">
            <h3 class="box-title">Loading..</h3>
          </div>
          <div class="box-body">
            잠시만 기다려주세요
          </div>
          <!-- /.box-body -->
          <!-- Loading (remove the following to stop the loading)-->
          <div class="overlay">
            <i class="fa fa-refresh fa-spin"></i>
          </div>
          <!-- end loading -->
        </div>
        <!-- /.box -->
        </div>
	</div>
</div>          
</div>

<script>
	(function($) {
		var userInfo = {
				isLogin : false,
				usn : 0,
				nickname : ""
			};

		$(function() {
			userInfo = $.extend(userInfo, getUserInfo() || {});

			
			if (userInfo.isLogin) {
				$(".d-secOnline").removeClass("hidden");
				$(".d-btnProfile").on("click", function() {
					$.GNB.profile();
				});
				$(".d-btnLogout").on("click", function() {
					$.GNB.logout();
				});
			} else {
				$(".d-secOffline").removeClass("hidden");
				$(".d-btnLogin").on("click", function() {
					$.GNB.login();
				});
			}

			showUserInfo();
		});

		function getUserInfo() {
			var data = $.GNB.getUserInfo();
			return data;
		}

		function showUserInfo() {
			$(".d-pNickname").text(userInfo.nickname + "(" + userInfo.usn + ")");
		}
		
		$("#gameSelector").multiselect({
			"buttonWidth": "100%",
			"maxHeight": 500,
			"enableCaseInsensitiveFiltering": true,
			"onInitialized": function(select, container) {
				$(container[0]).find("input[type=radio]").hide();
				$(container[0]).find("button.multiselect").removeClass("btn-default");
				$(container[0]).find("button.multiselect").addClass("bg-olive");
				$(container[0]).find("button.multiselect").addClass("btn-flat");
			}, 
			"onChange": function(option, checked, select) {
				location.href = location.href.replace(/ssn=[0-9]*/, "ssn=" + $(option).val());
			}
		});
	})(jQuery);
</script>
