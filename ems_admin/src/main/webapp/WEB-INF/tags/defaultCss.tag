<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:eval expression="@environment.getProperty('ftt.domain.static')" var="staticDomain"/>

<link rel="stylesheet" href="/static/common/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="/static/common/dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="/static/common/dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="/static/common/plugins/iCheck/flat/blue.css">
<link rel="stylesheet" href="/static/common/plugins/iCheck/all.css">
<link rel="stylesheet" href="/static/common/plugins/morris/morris.css">
<link rel="stylesheet" href="/static/common/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="/static/common/plugins/datepicker/datepicker3.css">
<link rel="stylesheet" href="/static/common/plugins/daterangepicker/daterangepicker.css">
<link rel="stylesheet" href="/static/common/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<link rel="stylesheet" href="/static/common/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="/static/common/plugins/tagsinput/bootstrap-tagsinput.css">
<link rel="stylesheet" href="/static/common/plugins/bootstrap-fileinput/fileinput.css">
<link rel="stylesheet" href="/static/common/plugins/bootstrap-fileinput/themes/explorer/theme.css">
<link rel="stylesheet" href="/static/common/plugins/select2/select2.css">
<link rel="stylesheet" href="/static/common/dist/css/custom.css">
<link href="//${staticDomain}/common/css/module/ftt.globalmsg.css" rel="stylesheet" type="text/css" />
<link href="//${staticDomain}/service/push3admin/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
<link href="//${staticDomain}/common/css/module/ftt.globalmsg.css" rel="stylesheet" type="text/css" />
