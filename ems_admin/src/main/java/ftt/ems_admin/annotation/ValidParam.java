package ftt.ems_admin.annotation;

import java.lang.annotation.Documented;  
import java.lang.annotation.ElementType;  
import java.lang.annotation.Retention;  
import java.lang.annotation.RetentionPolicy;  
import java.lang.annotation.Target;

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
/**
 * Annotation that will capture conditions that will validate an incoming request method parameter. 
 * @author prabharangopalan
 *
 */
public @interface ValidParam {

    /**
     * The minimum length of the required parameter. Works only {@link #empty()} is false. Defaults to 1.
     * 
     * @return
     */
    int min() default 1;

    /**
     * The maximum length of the required parameter. Works only {@link #empty()} is false. Defaults to 1000.
     * 
     * @return
     */
    int max() default 1000;

    /**
     * Whether the parameter's value can be empty/null. If set, this overrides the min/max settings. IOW, empty = {gfm-js-extract-pre-1} means
     * min = 0 and max = 0 irrespective of what those are set to. <br>
     * 
     * Default is {gfm-js-extract-pre-2}
     * 
     * @return
     */
    boolean empty() default false;
}