package ftt.ems_admin.annotation;

/**
 * 
 * @author kwonje
 * Error Code, Error Message(Message Resource Bundle ID) 정의
 *
 */
public enum ErrorCode {
	
	//-1 ~ 99 system 오류 관련
	SYSTEM_ERROR_UNKNOWN(-1, "system.error.unknown"), //서버 오류
	SYSTEM_ERROR_DB(-2, "system.error.db"), //db 오류
	MISSING_PARAMETER(-3, "missing.parameter"), //필수 파라미터 누락
	
		
	//-100 ~ -199 사용자 로그인 및 권한 관련
	LOGIN_REQUIRED(-100,"login.required"), //로그인 필요
	PERMISSION_DENIED(-101,"permission.denied"), //권한 없음
	
	FILE_DOWNLOAD_ERROR(-202,"file.download.error"), //파일 다운로드 오류
	FILE_UPLOAD_ERROR(-203,"file.upload.error"); //파일 업로드오류

	
	private int errorCode;
	private String errorMessage;
	
	ErrorCode(int errorCode, String errorMessage){
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
	
	public int getErrorCode(){
		return errorCode;
	}
	
	public String getErrorMessage(){
		return errorMessage;
	}

}
