package ftt.ems_admin.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import ftt.common.auth.AuthPermission;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@AuthPermission(value="/ems/email", writeRequired=true)
public @interface AuthEmail {  
}