package ftt.ems_admin.controller.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import ftt.common.auth.AuthLoginRequiredException;
import ftt.common.auth.AuthUser;
import ftt.common.auth.IAuthService;

/**
 * Auth2 유저정보 가져오기 공통 
 * @author kwonje
 *
 */
@Service
public class AuthContext implements IAuthContext {

	@Autowired
	private ApplicationContext context;
	
	private IAuthService authSvc;
	
	public AuthUser getAuthInfo (HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		authSvc = context.getBean(IAuthService.class);
		AuthUser userInfo = authSvc.getUserInfo();
		
		if(userInfo == null){
			throw new AuthLoginRequiredException();
		}
		
		return userInfo;
	}
}