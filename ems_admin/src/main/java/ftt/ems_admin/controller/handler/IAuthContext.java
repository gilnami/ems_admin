package ftt.ems_admin.controller.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ftt.common.auth.AuthUser;


public interface IAuthContext {

	abstract AuthUser getAuthInfo(HttpServletRequest request, HttpServletResponse response) throws Exception;
	
}