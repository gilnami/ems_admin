package ftt.ems_admin.controller.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.LocaleResolver;

import ftt.common.auth.AuthLoginRequiredException;
import ftt.common.auth.AuthNotAllowedException;
import ftt.ems_admin.annotation.ErrorCode;


@Order(Ordered.LOWEST_PRECEDENCE)
@ControllerAdvice(annotations = Controller.class)
public class CommonExceptionHandler {

	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver locale;

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public static final String DEFAULT_ERROR_VIEW = "/error/commonError";

	@ExceptionHandler(AuthLoginRequiredException.class)
	public String authLoginRequiredException(HttpServletRequest req, AuthLoginRequiredException e, Model model) {
		logger.error("AuthLoginRequiredException :" + e);
		String msg = messageSource.getMessage(ErrorCode.LOGIN_REQUIRED.getErrorMessage(), null, "Default", locale.resolveLocale(req));
		model.addAttribute("message",msg);
		return DEFAULT_ERROR_VIEW;
	}

	@ExceptionHandler(AuthNotAllowedException.class)
	public String authNotAllowedException(HttpServletRequest req, AuthNotAllowedException e,  Model model) {
		logger.error("AuthNotAllowedException :" + e.getMessage());
		String msg = messageSource.getMessage(ErrorCode.PERMISSION_DENIED.getErrorMessage(), null, "Default", locale.resolveLocale(req));
		model.addAttribute("message",msg);
		return DEFAULT_ERROR_VIEW;
	}
	
	@ExceptionHandler(MissingServletRequestParameterException.class)
	public String missingParameterException(HttpServletRequest req, AuthNotAllowedException e,  Model model) {
		logger.error("missingParameterException :" + e.getMessage());
		String msg = messageSource.getMessage(ErrorCode.MISSING_PARAMETER.getErrorMessage(), null, "Default", locale.resolveLocale(req));
		model.addAttribute("message",msg);
		return DEFAULT_ERROR_VIEW;
	}
	
	@ExceptionHandler(value = Exception.class)
	public String defaultException(HttpServletRequest req, Exception e, Model model){
		logger.error("{} {}", e.getClass().getName(), e);
		String msg = messageSource.getMessage(ErrorCode.SYSTEM_ERROR_UNKNOWN.getErrorMessage(), null, "Default", locale.resolveLocale(req));
		model.addAttribute("message",msg);
		return DEFAULT_ERROR_VIEW;
	}
	
	@ExceptionHandler(value = MultipartException.class)
    public String multipartException(HttpServletRequest req, HttpServletResponse rep, MultipartException e, Model model) {
		logger.error("MultipartException : ", e);
		String msg = messageSource.getMessage(ErrorCode.FILE_UPLOAD_ERROR.getErrorMessage(), null, "Default", locale.resolveLocale(req));
		model.addAttribute("message",msg);
		return DEFAULT_ERROR_VIEW;
    }
}
