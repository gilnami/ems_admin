package ftt.ems_admin.controller.handler;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;

import ftt.common.types.rpc.JsonResult;
import ftt.ems_admin.annotation.ErrorCode;
import ftt.ems_admin.exception.CommonException;



@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice(annotations = RestController.class)
public class CommonRestExceptionHandler {

	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localeResolver;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@ResponseBody
	@ExceptionHandler(CommonException.class)
	public JsonResult SampleExceptionHandler(CommonException e, HttpServletRequest request){
		
		String errorMessage = e.getErrorMessage(messageSource, localeResolver.resolveLocale(request));
		logger.error("[CommonRestExceptionHandler] code:{}, msg:{}, addmsg:{}, debmsg:{}", e.getErrorCode(), errorMessage, e.getAddMessage(), e.getDebugMessage());
		
		return JsonResult.fail(-1, errorMessage);
	}
	
	@ResponseBody
	@ExceptionHandler(Exception.class)
	public JsonResult defaultExceptionHandler(Exception e, HttpServletRequest request){
		
		CommonException CommonException;
		if (e instanceof SQLException){
			CommonException = new CommonException(ErrorCode.SYSTEM_ERROR_DB);
		} else {
			CommonException = new CommonException(ErrorCode.SYSTEM_ERROR_UNKNOWN);
		}
		
		//String errorMessage = CommonException.getErrorMessage(messageSource, localeResolver.resolveLocale(request));
		String errorMessage = messageSource.getMessage(CommonException.getErrorMessage(), null, "Default", localeResolver.resolveLocale(request));
		
		logger.error("[CommonRestExceptionHandler] class:{}, msg:{}", e.getClass(), e.getMessage());
		
		return JsonResult.fail(-2, errorMessage);
	}
}
