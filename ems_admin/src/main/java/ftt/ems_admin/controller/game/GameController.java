package ftt.ems_admin.controller.game;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ftt.common.auth.AuthLoginRequired;

@AuthLoginRequired
@Controller
@RequestMapping("/game")
public class GameController {

	/**
	 * 전체 게임 선택 화면 
	 * @param model
	 * @param path
	 * @param ssn
	 * @return
	 */
	@RequestMapping("/list")
	public String actionList(Model model, 
			@RequestParam(value = "path", defaultValue = "push") String path,
			@RequestParam(value = "ssn", defaultValue = "0") int ssn) {

		if (ssn > 0) {
			return String.format("redirect:/%s?ssn=%d", path, ssn);
		}
		model.addAttribute("path", path);
		
		return "game/gameList";
	}

}
