package ftt.ems_admin.controller.advice;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import ftt.common.pub.pubinfo.IPubInfoGameService;
import ftt.common.pub.pubinfo.PubInfoGame;
import ftt.ems_admin.config.AppConfig;

@ControllerAdvice
public class GlobalModelAttribute {
	
	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private AppConfig config;
	
	@Autowired
	private IPubInfoGameService pubGameSvc;
	

	@ModelAttribute("commonModel")
	public void commonModel(Model model,  @RequestParam(value="ssn", defaultValue="0") int ssn) {
	
		List<PubInfoGame> gameList = pubGameSvc.getGameListNameOrder();
		Map<Integer, PubInfoGame> gameMap = pubGameSvc.getGameMap();
		
		model.addAttribute("gameList", gameList);
		model.addAttribute("gameMap", gameMap);
		model.addAttribute("ssn", ssn);
		
		/*
		if (ssn > 0) {
			model.addAttribute("currPubInfoGame", gameMap.get(ssn));
			
			Map<Integer, AdminNotice> noticeMap = settingsSvc.getNotice(ssn);
			model.addAttribute("noticeMap", noticeMap);
		}
		
		IAuthService authSvc = context.getBean(IAuthService.class);
		try {
			AuthAllowType allowType = authSvc.getAllowType("/ems/admin");
			if (allowType.canWrite()) {
				model.addAttribute("pushUser", true);
			} else if (allowType.canRead()) {
				model.addAttribute("pushReader", true);
			}
			
		} catch (Exception e) {
		}
		*/
		// 시간 표시 데이터
		Date now = new Date();
	
		TimeZone utcTimeZone = TimeZone.getTimeZone("UTC");
		TimeZone sysTimeZone = TimeZone.getDefault();
		
		DateFormat sysDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		DateFormat utcDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		utcDateFormat.setTimeZone(utcTimeZone);
		int tzOffset = sysTimeZone.getOffset(now.getTime()) / 3600000;
		
		model.addAttribute("tzUtcNow", String.format("[UTC+0] %s", utcDateFormat.format(now)));
		model.addAttribute("tzSystemNow", String.format("[UTC%+d] %s", tzOffset, sysDateFormat.format(now)));
		model.addAttribute("timeZoneDispName", String.format("UTC%+d", tzOffset));
		
		// Config Data
		model.addAttribute("rewardSvr", config.getReward("server"));
		
	}
}
