package ftt.ems_admin.controller.member;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ftt.common.pub.code.CodeObject;
import ftt.common.pub.code.IGameCodeService;
import ftt.ems_admin.annotation.AuthMember;

@AuthMember
@Controller
@RequestMapping("/member")
public class MemberController {

	@Autowired
	private IGameCodeService gameCodeSvc;
	
	@ModelAttribute
	public void commonModel(Model model) {
		model.addAttribute("menuItem", "/member");
	}
	
	@RequestMapping("/list")
	public String actionList(Model model, 
			@RequestParam(value = "ssn", defaultValue = "0") int ssn) {

		Map<Integer, CodeObject> gameCodeTable = gameCodeSvc.getMap();
		
		model.addAttribute("path", "/member/list");
		model.addAttribute("ssn", ssn);
		if(ssn != 0)
			model.addAttribute("gameName", gameCodeTable.get(ssn).getName());
		
		return "/member/list";
	}
}
