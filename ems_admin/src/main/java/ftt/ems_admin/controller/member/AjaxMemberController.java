package ftt.ems_admin.controller.member;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ftt.common.pub.code.CodeObject;
import ftt.common.pub.code.ICountryCodeService;
import ftt.common.pub.code.IGameCodeService;
import ftt.common.pub.code.ILangCodeService;
import ftt.common.types.rpc.JsonResult;
import ftt.ems_admin.annotation.AuthMember;
import ftt.ems_admin.annotation.ErrorCode;
import ftt.ems_admin.entity.member.AccountInfo;
import ftt.ems_admin.entity.type.DEF;
import ftt.ems_admin.exception.CommonException;
import ftt.ems_admin.service.member.IMemberService;

@RestController
@AuthMember
@RequestMapping("/ajax/member")
public class AjaxMemberController {

	@Autowired
	private IMemberService memberService;
	
	@Autowired
	private IGameCodeService gameCodeSvc;
	
	@Autowired
	private ILangCodeService langCodeSvc;
	
	@Autowired
	private ICountryCodeService countryCodeSvc;
	
	
	@RequestMapping("/list")
	public JsonResult list(@RequestParam(value = "ssn", defaultValue = "0") int ssn,
			@RequestParam(value = "pimIdx", defaultValue = "0") long pimIdx,
			@RequestParam(value = "accountIdx", defaultValue = "0") long accountIdx
			) throws Exception {

		Map<Integer, CodeObject> gameCodeTable = gameCodeSvc.getMap();
		Map<String, CodeObject> langCodeTable = langCodeSvc.getAvailableMapShortNameKey();
		Map<String, CodeObject> countryCodeTable = countryCodeSvc.getAvailableMapShortNameKey();
		List<AccountInfo> list = memberService.getSearchUser(ssn, pimIdx, accountIdx);
		
		
		List<Map<String, Object>> resultList = new ArrayList<Map<String,Object>>();
		for(AccountInfo b : list){
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("ssn", b.getSsn());
			map.put("gameName", gameCodeTable.get(b.getSsn()).getName());
			map.put("accountIdx", b.getAccountIdx());
			map.put("pimIdx", b.getPimIdx());
			map.put("accountStatus", b.getAccountStatus());
			map.put("accountStatusName", DEF.MEMBER_STATE.getDefine(b.getAccountStatus()));
			map.put("recvStatus", b.getRecvStatus());
			map.put("recvStatusName", DEF.RECV_STATE.getDefine(b.getRecvStatus()));
			map.put("countryCode", b.getCountryCode());
			map.put("countryName", countryCodeTable.get(b.getCountryCode().toUpperCase()).getName());
			map.put("langCode", b.getLangCode());
			map.put("langName", langCodeTable.get(b.getLangCode()).getName().toLowerCase());
			map.put("regDt", b.getRegDt());
			map.put("updDt", b.getUpdDt());
			resultList.add(map);
		}
		
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("list", resultList);
		result.put("accountStatusList", DEF.MEMBER_STATE.getList());
		result.put("recvStatusList", DEF.RECV_STATE.getList());
		return JsonResult.success(result);
	}
	
	@RequestMapping("/change/account-status")
	public JsonResult changeStatus(@RequestParam(value = "ssn", defaultValue = "0") int ssn,
			@RequestParam(value = "status", defaultValue = "") String accountStatus,
			@RequestParam(value = "accountIdx", defaultValue = "0") long accountIdx
			) throws Exception {
		
		if(accountIdx == 0 || ssn == 0 || accountStatus.equals("")) 
			throw new CommonException(ErrorCode.MISSING_PARAMETER);
		
		memberService.uptAccountStatus(ssn, accountStatus, accountIdx);
		
		return JsonResult.success();
	}
	
	@RequestMapping("/change/recv-status")
	public JsonResult changeRecv(
			@RequestParam(value = "status", defaultValue = "") String recvStatus,
			@RequestParam(value = "pimIdx", defaultValue = "0") long pimIdx
			) throws Exception {
		
		if(pimIdx == 0 || recvStatus.equals("")) 
			throw new CommonException(ErrorCode.MISSING_PARAMETER);
		
		memberService.uptRecvStatus(recvStatus, pimIdx);
		
		return JsonResult.success();
	}
}
