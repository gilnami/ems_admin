package ftt.ems_admin.controller.email;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import ftt.common.auth.AuthUser;
import ftt.common.pub.code.CodeObject;
import ftt.common.pub.code.ICountryCodeService;
import ftt.common.pub.code.IGameCodeService;
import ftt.common.pub.code.ILangCodeService;
import ftt.ems_admin.annotation.Auth;
import ftt.ems_admin.annotation.AuthEmail;
import ftt.ems_admin.entity.email.EmailExport;
import ftt.ems_admin.entity.email.ExportHistory;
import ftt.ems_admin.entity.pubinfo.GameCountry;
import ftt.ems_admin.entity.pubinfo.GameLanguage;
import ftt.ems_admin.helper.CSVUtil;
import ftt.ems_admin.helper.ExcelReadUtil;
import ftt.ems_admin.service.email.IEmailService;
import ftt.ems_admin.service.pubinfo.impl.PubInfoService;

@AuthEmail
@Controller
@RequestMapping("/email")
public class EmailController {

	@Autowired
	private IGameCodeService gameCodeSvc;
	
	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private ILangCodeService langCodeSvc;
	
	@Autowired
	private ICountryCodeService countryCodeSvc;
	
	@Autowired
	private IEmailService emailService;
	
	@Autowired
	private ExcelReadUtil excelReadUtil;
	
	
	@Autowired
	private CSVUtil csvUtil;

	@Value("${spring.http.multipart.location}")
	private String tempDir;
	
	@ModelAttribute
	public void commonModel(Model model) {
		model.addAttribute("menuItem", "/email");
	}
	
	/**
	 * 이메일 추출 기본 화면 
	 * @param model
	 * @param ssn
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/export-form")
	public String actionForm(Model model, @RequestParam(value = "ssn", defaultValue = "0") int ssn) throws Exception {
		
		model.addAttribute("path", "/email/export");
		
		
		Map<Integer, CodeObject> gameCodeTable = gameCodeSvc.getMap();
		PubInfoService gameSvc = context.getBean(PubInfoService.class, ssn, 0);
		
		// 등록 가능 국가 목록
		List<GameCountry> countryList = new ArrayList<GameCountry>();
		if(ssn == 0){
			List<CodeObject> countryCodeTable = countryCodeSvc.getAvailableList();
			for(CodeObject obj : countryCodeTable){
				GameCountry bean = new GameCountry();
				bean.setCode(obj.getShortName());
				bean.setName(obj.getName());
				countryList.add(bean);
			}
		}else{
			countryList = gameSvc.getAvailableCountryList();
		}
		
		// 등록 가능한 언어 목록
		List<GameLanguage> langList = new ArrayList<GameLanguage>();
		if(ssn == 0){
			List<CodeObject> langCodeTable = langCodeSvc.getAvailableList();
			for(CodeObject obj : langCodeTable){
				GameLanguage bean = new GameLanguage();
				bean.setCode(obj.getCode());
				bean.setName(obj.getName());
				bean.setShortName(obj.getShortName());
				langList.add(bean);
			}
		}else{
			langList = gameSvc.getAvailableLangList();
		}
		
		// 현재 접속 게임 명
		String gameName = "";
		CodeObject currGameCodeObj = gameCodeTable.get(ssn);
		if (currGameCodeObj != null) {
			gameName = currGameCodeObj.getName();
		}
		if(ssn == 0) gameName = "전체게임";
		
		model.addAttribute("countryList", countryList);
		model.addAttribute("langList", langList);
		model.addAttribute("ssn", ssn);
		model.addAttribute("gameName", gameName);
		return "/email/export";
	}
	
	/**
	 * 이메일 추출 전체 
	 * @param ssn
	 * @param country
	 * @param lang
	 * @param user
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/export-all")
	public void exportAll(
			@RequestParam(value="ssn", defaultValue="0") int ssn, 
			@RequestParam(value="country", defaultValue="") String country, 
			@RequestParam(value="lang", defaultValue="") String lang, 
			@Auth AuthUser user, HttpServletRequest request, HttpServletResponse response) throws Exception {

		//유저리스트
		List<EmailExport> list = emailService.getFilteringSubscriveUserList(ssn, country, lang);
		
		//export 파일 만들기 
		String zipName = emailService.makeExportFileList(list);

		//압축파일 다운로드
		File zipFile = new File(zipName);
		response.setContentType("application/x-zip-compressed;");
		response.setHeader("Content-Disposition", "attachment; filename=\"" + zipFile.getName() + "\";");
		OutputStream out = null;
		InputStream in = null;
		try {
			out = response.getOutputStream();
			in = new FileInputStream(zipFile);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (Exception e) {
				}
				out = null;
			}
			if (in != null) {
				try {
					in.close();
				} catch (Exception e) {
				}
				in = null;
			}
			zipFile.delete();
		}
		
		//히스토리 저장
		ExportHistory history = new ExportHistory(ssn, user.getUsn(), 1, country, lang);
		emailService.setLoginLog(history);
	}
	
	/**
	 * 이메일 조건별 추출 
	 * @param ssn
	 * @param file
	 * @param user
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/export-target")
	public void exportTarget(
			@RequestParam(value = "ssn", defaultValue = "0") int ssn,
			@RequestParam(value = "attach", defaultValue = "") MultipartFile file,
			@Auth AuthUser user, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		final String uploadFolder = "_export";
        String localRoot =  tempDir + "/";
        String uploadPath = uploadFolder + "/" + getDailyDir();
        String fileExt = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.')+1);
        File localTempDir = new File(localRoot +  "/" + uploadPath);
        if(! localTempDir.exists()) {
        	localTempDir.mkdirs();
        }
        
        //임시디렉토리 이동 
        String fileName = file.getOriginalFilename();
        File destFile = new File(localRoot + "/" + uploadPath + "/" + fileName);        
        file.transferTo(destFile);
        
        List<Integer> accountList = new ArrayList<Integer>();
        
        //엑셀 데이터 읽기
        if(fileExt.equals("csv")){
	        csvUtil.setFilePath(destFile.getAbsolutePath());
	        accountList = csvUtil.getCellConvertIntegerList(0);
        }else{
        	excelReadUtil.setFilePath(destFile.getAbsolutePath());
        	excelReadUtil.setStartRow(2);
			accountList = excelReadUtil.getCellConvertIntegerList(0);
        }
        
		//리스트 필터링
		List<EmailExport> list = emailService.getTargetFilteringSubscriveUserList(ssn, accountList);
		
		//export 파일 만들기 
		String zipName = emailService.makeExportFileList(list);

		//압축파일 다운로드
		File zipFile = new File(zipName);
		response.setContentType("application/x-zip-compressed;");
		response.setHeader("Content-Disposition", "attachment; filename=\"" + zipFile.getName() + "\";");
		OutputStream out = null;
		InputStream in = null;
		try {
			out = response.getOutputStream();
			in = new FileInputStream(zipFile);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (Exception e) {
				}
				out = null;
			}
			if (in != null) {
				try {
					in.close();
				} catch (Exception e) {
				}
				in = null;
			}
			zipFile.delete();
		}
	    
		//히스토리 저장 
		ExportHistory history = new ExportHistory(ssn, user.getUsn(), 2, null, null);
		emailService.setLoginLog(history);
	}
	
	//디렉토리 경로
	private String getDailyDir() throws Exception {
		Calendar oCalendar = Calendar.getInstance();
		int y = oCalendar.get(Calendar.YEAR);
		int m = oCalendar.get(Calendar.MONTH)+1;
		int d = oCalendar.get(Calendar.DAY_OF_MONTH);
		String dir = Integer.toString(y)+"/"+Integer.toString(m)+"/"+Integer.toString(d); 
		return dir;
	}
}
