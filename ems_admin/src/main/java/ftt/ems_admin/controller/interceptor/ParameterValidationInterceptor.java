package ftt.ems_admin.controller.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.core.MethodParameter;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import ftt.ems_admin.annotation.ErrorCode;
import ftt.ems_admin.annotation.ValidParam;
import ftt.ems_admin.exception.CommonException;


@Component
public class ParameterValidationInterceptor extends HandlerInterceptorAdapter{

	private final ParameterNameDiscoverer parameterNameDiscoverer = new LocalVariableTableParameterNameDiscoverer();

	private Logger logger = LoggerFactory.getLogger(getClass());
	
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod && areParametersValid((HandlerMethod) handler, request)) {
            return true;
        } else {
        	throw new CommonException(ErrorCode.MISSING_PARAMETER);
        }
    }

    /**
     * Validates each parameter that are annotated with {@link ValidParam} annotation
     * 
     * @param handlerMethod
     * @param request
     * @return
     */
    private boolean areParametersValid(HandlerMethod handlerMethod, HttpServletRequest request) {
        MethodParameter[] methodParameters = handlerMethod.getMethodParameters();
        for (MethodParameter param : methodParameters) {
            param.initParameterNameDiscovery(this.parameterNameDiscoverer);
            RequestParam requestParam = param.getParameterAnnotation(RequestParam.class);
            ValidParam validationConstraint = param.getParameterAnnotation(ValidParam.class);
            if (validationConstraint != null) {
                // apply validations for only those params who have the 'ValidParam' annotation.
                String paramName = requestParam.value();
                logger.debug("validating the parameter " + paramName);
                // cannot get the argument via reflection. so have to get it from the request itself.
                String paramValueInRequest = request.getParameter(paramName);
                int min = validationConstraint.min();
                int max = validationConstraint.max();
                boolean empty = validationConstraint.empty();
                int valueLength = StringUtils.isEmpty(paramValueInRequest) ? 0 : paramValueInRequest.length();
                // if the parameter can be empty, its length should 0
                // otherwise, it should be betweeen min & max lengths
                if ((empty && valueLength == 0) || (!empty && valueLength >= min && valueLength <= max)) {
                    logger.debug("parameter " + paramName + " is valid");
                    continue;
                } else {
                    logger.error("parameter " + paramName + " is invalid " + " with value " + paramValueInRequest);
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws java.lang.Exception{
    }

}
