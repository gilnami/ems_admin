package ftt.ems_admin.controller.mng;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ftt.common.pub.code.CodeObject;
import ftt.common.pub.code.ICountryCodeService;
import ftt.common.pub.code.IGameCodeService;
import ftt.common.pub.code.ILangCodeService;
import ftt.common.pub.code.IMarketCodeService;
import ftt.common.pub.code.IZoneCodeService;
import ftt.common.pub.pubinfo.IPubInfoLangService;
import ftt.ems_admin.annotation.AuthManager;
import ftt.ems_admin.config.AppConfig;
import ftt.ems_admin.entity.ems.EmsCondition;
import ftt.ems_admin.entity.ems.EmsLang;
import ftt.ems_admin.entity.ems.EmsReward;
import ftt.ems_admin.entity.pubinfo.GameCountry;
import ftt.ems_admin.entity.pubinfo.GameLanguage;
import ftt.ems_admin.entity.pubinfo.GameMarket;
import ftt.ems_admin.entity.pubinfo.GameZone;
import ftt.ems_admin.service.mng.IMngService;
import ftt.ems_admin.service.pubinfo.impl.PubInfoService;

@AuthManager
@Controller
@RequestMapping("/mng")
public class MngCongroller {

	@Autowired
	private AppConfig config;
	
	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private IGameCodeService gameCodeSvc;
	
	@Autowired
	private IMarketCodeService marketCodeSvc;
	
	@Autowired
	private ILangCodeService langCodeSvc;
	
	@Autowired
	private IZoneCodeService zoneCodeSvc;
	
	@Autowired
	private ICountryCodeService countryCodeSvc;
	
	
	@Autowired
	private IMngService mngService;
	
	@ModelAttribute
	public void commonModel(Model model) {
		model.addAttribute("menuItem", "/mng");
	}
	
	/**
	 * 소식통 관리 화면
	 * @param model
	 * @param ssn
	 * @return
	 */
	@RequestMapping("/list")
	public String actionList(Model model, @RequestParam(value = "ssn", defaultValue = "0") int ssn) throws Exception {
		
		model.addAttribute("path", "/mng/list");
		
		if (ssn <= 0) {
			return "redirect:/game/list?path=/mng/list";
		}
		
		
		Map<Integer, CodeObject> gameCodeTable = gameCodeSvc.getMap();
		Map<Integer, CodeObject> zoneCodeTable = zoneCodeSvc.getMap();
		Map<String, CodeObject> langCodeTable = langCodeSvc.getAvailableMapShortNameKey();
		Map<String, CodeObject> countryCodeTable = countryCodeSvc.getAvailableMapShortNameKey();
		Map<Integer, CodeObject> marketCodeTable = marketCodeSvc.getMap();
		
		// 현재 접속 게임 명
		String gameName = "";
		CodeObject currGameCodeObj = gameCodeTable.get(ssn);
		if (currGameCodeObj != null) {
			gameName = currGameCodeObj.getName();
		}
		
		EmsCondition condition = mngService.getEmsCondition(ssn);
		if(condition != null && condition.getSsn() > 0){
			List<String> emsCountry = mngService.getEmsCountry(ssn);
			List<Integer> emsMarket = mngService.getEmsMarket(ssn);
			List<Integer> emsZone = mngService.getEmsZone(ssn);
			List<EmsLang> emsLang = mngService.getEmsLang(ssn);
			
			model.addAttribute("emsCondition", condition);
			model.addAttribute("emsCountry", emsCountry);
			model.addAttribute("emsMarket", emsMarket);
			model.addAttribute("emsZone", emsZone);
			model.addAttribute("emsLang", emsLang);
			
		}
		model.addAttribute("ssn", ssn);
		model.addAttribute("gameName", gameName);
		model.addAttribute("zoneCodeTable", zoneCodeTable);
		model.addAttribute("langCodeTable", langCodeTable);
		model.addAttribute("marketCodeTable", marketCodeTable);
		model.addAttribute("countryCodeTable", countryCodeTable);
		
		return "/mng/list";
	}

	/**
	 * 소식통 등록 화면 
	 * @param model
	 * @param ssn
	 * @return
	 */
	@RequestMapping("/form")
	public String actionForm(Model model, @RequestParam("ssn") int ssn) throws Exception{
		
		model.addAttribute("path", "/mng/view");
		
		if (ssn <= 0) {
			return "redirect:/game/list?path=/mng/view";
		}
		
		PubInfoService gameSvc = context.getBean(PubInfoService.class, ssn, 0);
		
		Map<Integer, CodeObject> gameCodeTable = gameCodeSvc.getMap();
		Map<Integer, CodeObject> zoneCodeTable = zoneCodeSvc.getMap();
		
		// 등록 가능 서비스 존 목록
		List<GameZone> zoneList = gameSvc.getAvailableZoneList();
		
		// 등록 가능 국가 목록
		List<GameCountry> countryList = gameSvc.getAvailableCountryList();
		Map<Integer, List<String>> zoneCountryCodeListMap = gameSvc.getAvailableZoneCountryListMap();
		
		// 등록 가능한 마켓 목록
		List<GameMarket> marketList = gameSvc.getAvailableMarketList();
		Map<Integer, List<Integer>> zoneMarketCodeListMap = gameSvc.getAvailableZoneMarketListMap();
		
		// 등록 가능한 언어 목록
		List<GameLanguage> langList = gameSvc.getAvailableLangList();
		Map<Integer, List<Integer>> zoneLangCodeListMap = gameSvc.getAvailableZoneLangListMap();
		
		
		// 현재 접속 게임 명
		String gameName = "";
		CodeObject currGameCodeObj = gameCodeTable.get(ssn);
		if (currGameCodeObj != null) {
			gameName = currGameCodeObj.getName();
		}
		
		EmsCondition condition = mngService.getEmsCondition(ssn);
		if(condition != null && condition.getSsn() > 0){
			List<String> emsCountry = mngService.getEmsCountry(ssn);
			List<Integer> emsMarket = mngService.getEmsMarket(ssn);
			List<Integer> emsZone = mngService.getEmsZone(ssn);
			List<EmsLang> emsLang = mngService.getEmsLang(ssn);
			EmsReward emsReward = mngService.getEmsReward(condition.getRewardIdx());
			
			model.addAttribute("emsCondition", condition);
			model.addAttribute("emsCountry", emsCountry);
			model.addAttribute("emsMarket", emsMarket);
			model.addAttribute("emsZone", emsZone);
			model.addAttribute("emsLang", emsLang);
			model.addAttribute("emsReward", emsReward);
			model.addAttribute("mode","modify");
			
		}

		model.addAttribute("ssn", ssn);
		model.addAttribute("gameName", gameName);
		model.addAttribute("zoneCodeTable", zoneCodeTable);
		model.addAttribute("zoneList", zoneList);
		model.addAttribute("countryList", countryList);
		model.addAttribute("marketList", marketList);
		model.addAttribute("langList", langList);
		model.addAttribute("zoneCountryCodeListMap", zoneCountryCodeListMap);
		model.addAttribute("zoneMarketCodeListMap", zoneMarketCodeListMap);
		model.addAttribute("zoneLangCodeListMap", zoneLangCodeListMap);
		model.addAttribute("rewardSvr", config.getReward("server"));
		model.addAttribute("pubInfoSvr", config.getPubInfo().getApiServer());
	
		return "/mng/form";
	}
	
	
}
