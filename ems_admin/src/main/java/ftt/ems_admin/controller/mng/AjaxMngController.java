package ftt.ems_admin.controller.mng;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ftt.common.auth.AuthUser;
import ftt.common.types.rpc.JsonResult;
import ftt.ems_admin.annotation.Auth;
import ftt.ems_admin.annotation.AuthManager;
import ftt.ems_admin.entity.ems.EmsForm;
import ftt.ems_admin.service.mng.IMngService;

@AuthManager
@RestController
@RequestMapping("/ajax/mng")
public class AjaxMngController {

	@Autowired
	private IMngService mngService;

	/**
	 * Ems 신규 저장
	 * 
	 * @param form
	 * @param auth
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/add")
	public JsonResult addEms(@Valid @RequestBody EmsForm data, @Auth AuthUser user) throws Exception {

		mngService.addEms(data, user);
		return JsonResult.success();
	}

	/**
	 * Ems 수정
	 * @param data
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/modify")
	public JsonResult modifyEms(@Valid @RequestBody EmsForm data, @Auth AuthUser user) throws Exception {

		mngService.modifyEms(data, user);
		return JsonResult.success();
	}

}
