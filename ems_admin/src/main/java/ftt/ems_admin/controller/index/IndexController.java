package ftt.ems_admin.controller.index;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import ftt.common.auth.AuthLoginRequired;

@AuthLoginRequired
@Controller
@RequestMapping("/")
public class IndexController {
	
	
	@RequestMapping("/")
	public String actioneIndex(Model model, HttpServletRequest req) throws Exception {
		
		return "/index/welcome";
	}
	
}
