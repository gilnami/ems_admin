package ftt.ems_admin.dao.game;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import ftt.ems_admin.annotation.GameDB;

@GameDB
public interface IGameDao {

	/**
	 * Emdb -> 게임db 동기화
	 * @param map
	 */
	public void setEmailPimIdx(HashMap<String, Object> map);
	
	
	/**
	 * 탈퇴후 7일 경과 유저 목록 
	 * @return
	 */
	public List<Long> getUnregUserList();
	
	/**
	 * 휴면 유저 목록
	 * @return
	 */
	public List<Long> getSleepUserList();
	
	/**
	 * 복귀 유저 목록 
	 * @return
	 */
	public List<Long> getReturnUserList();
	
	/**
	 * 제재유저 목록 
	 * @return
	 */
	public List<Long> getBlockUserListByRange();
	
	public List<Long> getBlockUserListByAccount(@Param("accountIdxList") String accountIdxList);
	
}
