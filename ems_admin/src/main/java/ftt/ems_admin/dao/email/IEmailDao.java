package ftt.ems_admin.dao.email;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import ftt.ems_admin.annotation.EMDB;
import ftt.ems_admin.entity.email.EmailExport;
import ftt.ems_admin.entity.email.ExportHistory;

@EMDB
public interface IEmailDao {

	/**
	 * 정상 유저 이메일(pim) 추출 
	 * @return
	 * @throws Exception
	 */
	public List<EmailExport> getSubscriveUserList(@Param("ssn") int ssn) throws Exception;
	
	/**
	 * 이메일 추출 히스토리 로깅
	 * @param exportHistory
	 * @throws Exception
	 */
	public void addExportHistory(ExportHistory exportHistory) throws Exception;
}
