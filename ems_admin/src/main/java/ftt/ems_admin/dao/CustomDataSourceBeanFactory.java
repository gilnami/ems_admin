package ftt.ems_admin.dao;

import java.io.IOException;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import ftt.ems_admin.annotation.EMDB;
import ftt.ems_admin.annotation.GameDB;

public abstract class CustomDataSourceBeanFactory {

	public static final String BASE_PACKAGE = "ftt.ems_admin.dao";
	public static final String MAPPER_LOCATIONS_PATH = "classpath*:sql/*.xml";

	protected void configureSqlSessionFactory(SqlSessionFactoryBean sessionFactoryBean, DataSource dataSource)
			throws IOException { 
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
	    
		sessionFactoryBean.setDataSource(dataSource);
		sessionFactoryBean.setMapperLocations(resolver.getResources(CustomDataSourceBeanFactory.MAPPER_LOCATIONS_PATH));
	}
}

@Configuration
@MapperScan(basePackages = {
		CustomDataSourceBeanFactory.BASE_PACKAGE }, annotationClass = EMDB.class, sqlSessionFactoryRef = "emdbSessionFactory")
class EmdbDataSourceBeanFactory extends CustomDataSourceBeanFactory {

	@Bean(name = "emdbSessionFactory")
	public SqlSessionFactory emdbSessionFactory(@Qualifier("emdbDataSource") DataSource ds) throws Exception {
		SqlSessionFactoryBean sessionFactoryBean = new SqlSessionFactoryBean();
		configureSqlSessionFactory(sessionFactoryBean, ds);
		return sessionFactoryBean.getObject();
	}

	@Bean(name = "emdbSessionTemplate", destroyMethod = "clearCache")
	public SqlSessionTemplate emdbSessionTemplate(
			@Qualifier("emdbSessionFactory") SqlSessionFactory pubdbSessionFactory) {
		return new SqlSessionTemplate(pubdbSessionFactory);
	}

}

@Configuration
@MapperScan(basePackages = {
		CustomDataSourceBeanFactory.BASE_PACKAGE }, annotationClass = GameDB.class, sqlSessionFactoryRef = "gameSqlSessionFactory")
class DynamicGameDataSourceConfig extends CustomDataSourceBeanFactory {

	@Bean(name = "gameSqlSessionFactory")
	public SqlSessionFactory gameSqlSessionFactory(@Qualifier("gameDataSource") DataSource dataSource)
			throws Exception {
		SqlSessionFactoryBean sessionFactoryBean = new SqlSessionFactoryBean();
		configureSqlSessionFactory(sessionFactoryBean, dataSource);
		return sessionFactoryBean.getObject();
	}

	@Bean(name = "gameSqlSessionTemplate")
	public SqlSessionTemplate gameSqlSessionTemplate(SqlSessionFactory gameSqlSessionFactory) throws Exception {
		return new SqlSessionTemplate(gameSqlSessionFactory);
	}
}


