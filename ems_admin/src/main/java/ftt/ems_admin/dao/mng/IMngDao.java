package ftt.ems_admin.dao.mng;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import ftt.ems_admin.annotation.EMDB;
import ftt.ems_admin.entity.TesterEntity;
import ftt.ems_admin.entity.ems.EmsCondition;
import ftt.ems_admin.entity.ems.EmsHistory;
import ftt.ems_admin.entity.ems.EmsLang;
import ftt.ems_admin.entity.ems.EmsReward;

@EMDB
public interface IMngDao {

	/**
	 * ems 등록
	 * @param condition
	 * @throws Exception
	 */
	public void addCondition(EmsCondition condition) throws Exception;
	public int addRewardBox(EmsReward reward) throws Exception;
	public void addCountry(@Param("ssn") int ssn, @Param("list") List<String> list) throws Exception;
	public void addLang(@Param("ssn") int ssn, @Param("list") List<EmsLang> list) throws Exception;
	public void addMarket(@Param("ssn") int ssn, @Param("list") List<Integer> list) throws Exception;
	public void addZone(@Param("ssn") int ssn, @Param("list") List<Integer> list) throws Exception;
	
	
	/**
	 * ems 조회 
	 * @param ssn
	 * @return
	 * @throws Exception
	 */
	public EmsCondition getCondition(@Param("ssn") int ssn) throws Exception;
	public EmsReward getReward(@Param("rewardIdx") long rewardIdx) throws Exception;
	public List<String> getCountry(@Param("ssn") int ssn) throws Exception;
	public List<EmsLang> getLang(@Param("ssn") int ssn) throws Exception;
	public List<Integer> getMarket(@Param("ssn") int ssn) throws Exception;
	public List<Integer> getZone(@Param("ssn") int ssn) throws Exception;
	
	/**
	 * ems 삭제
	 * @param ssn
	 * @throws Exception
	 */
	public void delCountry(@Param("ssn") int ssn) throws Exception;
	public void delMarket(@Param("ssn") int ssn) throws Exception;
	public void delLang(@Param("ssn") int ssn) throws Exception;
	public void delZone(@Param("ssn") int ssn) throws Exception;
	
	/**
	 * 소식통 관리 히스토리 저장 
	 * @param history
	 * @throws Exception
	 */
	public void addEmAdminHistory(EmsHistory history) throws Exception;
	
	public void addEmMainTest(List<TesterEntity> list) throws Exception;
	public void addAccountInfoTest(@Param("list") List<TesterEntity> list) throws Exception;
}
