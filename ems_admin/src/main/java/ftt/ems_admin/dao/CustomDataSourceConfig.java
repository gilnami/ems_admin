package ftt.ems_admin.dao;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import ftt.common.types.config.DataSourceConfig;
import ftt.ems_admin.config.CustomRoutingDataSource;
import ftt.ems_admin.config.GameDataSourceConfig;

@Configuration
public class CustomDataSourceConfig {
	
	@Autowired
	private GameDataSourceConfig gameDataSourceConfig;
	
	@Bean
	@Primary
	@ConfigurationProperties(prefix="datasource.emdb")
	public DataSource emdbDataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "gameDataSource")
	public DataSource gameDataSource() {
		Iterator<String> set = gameDataSourceConfig.getDatasource().keySet().iterator();
		Map<Object, Object> resolvedDataSources = new HashMap<>();

		while (set.hasNext()) {
			String database = set.next();
			DataSourceConfig dsConfig = gameDataSourceConfig.getDatasource().get(database);
			DataSourceBuilder dataSourceBuilder = new DataSourceBuilder(this.getClass().getClassLoader());
			dataSourceBuilder.driverClassName(dsConfig.getDriverClassName()).url(dsConfig.getUrl())
					.username(dsConfig.getUsername()).password(dsConfig.getPassword());
			resolvedDataSources.put(database, dataSourceBuilder.build());
		}

		CustomRoutingDataSource dataSource = new CustomRoutingDataSource();
		dataSource.setTargetDataSources(resolvedDataSources);
		dataSource.afterPropertiesSet();
		return dataSource;
	}
	
}
