package ftt.ems_admin.dao.member	;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import ftt.ems_admin.annotation.EMDB;
import ftt.ems_admin.entity.ems.EmsReward;
import ftt.ems_admin.entity.member.AccountInfo;

@EMDB
public interface IMemberDao {

	/**
	 * EMDB -> 게임DB동기화 실패 목록
	 * @param intervalDays
	 * @return
	 * @throws Exception
	 */
	public List<AccountInfo> getGameAccountSyncFailList(@Param("intervalDays") int intervalDays) throws Exception;
	
	/**
	 * EMDB 게임DB 동기화 실패 건수 삭제 
	 * @param ssn
	 * @param accountIdx
	 * @throws Exception
	 */
	public void delGameAccountSyncFail(@Param("ssn") int ssn, @Param("accountIdx") long accountIdx) throws Exception;
	
	/**
	 * 리워드 지급 실패 유저 목록
	 * @param intervalDays
	 * @return
	 * @throws Exception
	 */
	public List<AccountInfo> getRewardFailList(@Param("intervalDays") int intervalDays) throws Exception;
	
	/**
	 * 리워드 세팅 정보 조회
	 * @param ssn
	 * @return
	 * @throws Exception
	 */
	public List<EmsReward> getRewardBoxAllList() throws Exception;
	
	/**
	 * 리워드 재지급 실패건 삭제 
	 * @param idx
	 * @throws Exception
	 */
	public void delRewardFail(@Param("idx") int idx) throws Exception;
	
	/**
	 * 탈퇴 유저 삭제 
	 * @param ssn
	 * @param accountIdx
	 * @throws Exception
	 */
	public void delUnregAccount(@Param("ssn") int ssn, @Param("accountIdx") long accountIdx) throws Exception;
	
	/**
	 * 유저 상태 갱신
	 * @param ssn
	 * @param accountStatus
	 * @param accountIdx
	 * @throws Exception
	 */
	public void uptAccountStatusBatch(@Param("ssn") int ssn, @Param("accountStatus") String accountStatus, @Param("accountIdx") String accountIdx) throws Exception;
	public void uptAccountStatus(@Param("ssn") int ssn, @Param("accountStatus") String accountStatus, @Param("accountIdx") long accountIdx) throws Exception;
	
	/**
	 * 제재유저 목록 
	 * @return
	 * @throws Exception
	 */
	public List<AccountInfo> getBlockAccountList() throws Exception;
	
	/**
	 * 이메일 수신 재동의 대상 유저 (2년)
	 * @return
	 * @throws Exception
	 */
	public List<Integer> getEmailTermsUserlist() throws Exception;
	
	/**
	 * 이메일 수신 동의 상태 갱신 
	 * @param recvStatus
	 * @param pimIdx
	 */
	public void uptEmMainStatus(@Param("recvStatus") String recvStatus, @Param("pimIdx") long pimIdx);
	
	/**
	 * 유저 조회 
	 * @param ssn
	 * @param pimIdx
	 * @param accountIdx
	 * @return
	 * @throws Exception
	 */
	public List<AccountInfo> getSearchUser(@Param("ssn") int ssn, @Param("pimIdx") long pimIdx, @Param("accountIdx") long accountIdx) throws Exception;
}
