package ftt.ems_admin.helper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ftt.common.lib.AESWrap;
import ftt.ems_admin.config.AppConfig;
import ftt.ems_admin.entity.email.EmailExport;

@Component
public class ExcelWriteUtil {

	@Autowired
	private AppConfig appConfig;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private String saveFilePath;
	private Workbook xlsxWb;
	private List<EmailExport> rowData;
	
	
	public void setSaveFilePath(String path) {
		this.saveFilePath = path;
	}

	public void setRowData(List<EmailExport> list) {
		this.rowData = list;
	}
	
	public String generateExcelData() throws Exception {

		Row row = null;
		Cell cell = null;
		Sheet sheet1 = null;
		CellStyle cellStyle = null;

		int rowNum = 1; 
		int innerNum = 1;
		int total = rowData.size();
		int fileCnt = 1; 
		final int maxRow = 1000000;
		for(EmailExport email : rowData){
			
			if(innerNum == 1){
				xlsxWb = new XSSFWorkbook();

				// *** Sheet-------------------------------------------------
				// Sheet 생성
				sheet1 = xlsxWb.createSheet("Sheet");

				// 컬럼 너비 설정
				sheet1.setColumnWidth(0, 5000);
				sheet1.setColumnWidth(1, 5000);
				sheet1.setColumnWidth(2, 40000);
				// ----------------------------------------------------------
				cellStyle = xlsxWb.createCellStyle();
				
				// 첫 번째 줄
				row = sheet1.createRow(0);

				// 첫 번째 줄에 Cell 설정하기-------------
				cell = row.createCell(0);
				cell.setCellValue("PIM");
				cell.setCellStyle(cellStyle); // 셀 스타일 적용

				cell = row.createCell(1);
				cell.setCellValue("언어코드");

				cell = row.createCell(2);
				cell.setCellValue("수신거부링크");
				cell.setCellStyle(cellStyle); // 셀 스타일 적용
				// ---------------------------------
			}
			
			row = sheet1.createRow(innerNum);
			
			cell = row.createCell(0);
			cell.setCellValue(email.getPimIdx());
			
			cell = row.createCell(1);
			cell.setCellValue(email.getLangCode());
			
			cell = row.createCell(2);
			cell.setCellValue(getUnsubscribeLink(email.getPimIdx(), email.getRegDt(), email.getLangCode()));
			cell.setCellStyle(cellStyle); // 셀 스타일 적용
			
			
			if(innerNum >= maxRow || rowNum == total){
				innerNum = 1;
				// excel 파일 저장
				try {
					logger.debug("make excel file");
					File xlsFile = new File(saveFilePath + "/" + fileCnt + ".xlsx");
					logger.debug("xlsFile :" + xlsFile.getAbsolutePath());
					FileOutputStream fileOut = new FileOutputStream(xlsFile);
					xlsxWb.write(fileOut);
					fileCnt++;
					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			rowNum++;
			innerNum++;
			
		}
		
		try {
			//파일 압축 
			Zip.compress(saveFilePath, saveFilePath+".zip");
		} catch (Throwable e) {
			e.printStackTrace();
		} 
		
		return saveFilePath+".zip";
	}
	
	public String getUnsubscribeLink(long pimIdx, String regDt, String langCode) throws Exception{

		String aesKey = appConfig.getAesKey();
		String aesIv = appConfig.getAesIv();
		String url = appConfig.getEms().getUnsubscribe()+"?";
		
		long regTimeMillis = DateUtil.convertDateToTimeStamp(regDt);
		
		try {
			AESWrap aes = new AESWrap(aesKey.getBytes(), aesIv.getBytes());
			StringBuffer buffer = new StringBuffer();
			StringBuffer param = new StringBuffer();
			
			param.append("idx=").append(pimIdx).append("&rdt=").append(regTimeMillis);
			String encodedData = aes.encrypt(param.toString());
			
			buffer.append(url).append("lang=").append(langCode);
			buffer.append("&d=").append(URLEncoder.encode(encodedData, "UTF-8"));
			
			return buffer.toString();
		} catch (Exception e) {
			
		}
		return "";
	}
}
