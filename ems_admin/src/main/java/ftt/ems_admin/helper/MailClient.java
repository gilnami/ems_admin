package ftt.ems_admin.helper;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import ftt.ems_admin.config.AppConfig;

@Component
public class MailClient {
	
	@Autowired
	JavaMailSender mailSender;
	
	@Autowired
	private AppConfig appConfig;
	
	public boolean sendMail(String from, String to, String subject, String content) {

		try {
			return sendMail(from, to, subject, content);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@Async("mailingThreadPoolTaskExecutor")
	public void sendAsyncMail(String to, String subject, String content) {
		
		Logger logger = LoggerFactory.getLogger(this.getClass());
		
		if (!sendMail(to, subject, content))
			logger.error("sendAsyncMail failed : {}, {}, {}, {}", to, subject, content);
	}
	
	public boolean sendMail(String to, String subject, String content) {

		Properties prop = System.getProperties();
		prop.put("mail.smtp.host", appConfig.getSmtpSource().get("host"));
		prop.put("mail.smtp.socketFactory.port", appConfig.getSmtpSource().get("port"));
		prop.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.smtp.port", appConfig.getSmtpSource().get("port"));
		
    	Authenticator auth = new MyAuthentication(appConfig.getSmtpSource().get("username"), 
    			appConfig.getSmtpSource().get("password"));
        
        //session 생성 및  MimeMessage생성
        Session session = Session.getDefaultInstance(prop, auth);
        MimeMessage message = new MimeMessage(session);

        try {
        	InternetAddress iFrom = new InternetAddress(appConfig.getSmtpSource().get("fromEmail"), 
        			appConfig.getSmtpSource().get("fromName"), "UTF-8");
        	InternetAddress iTo = new InternetAddress(to);
        	
            message.setFrom(iFrom);
            message.addRecipient(RecipientType.BCC, iTo);
            message.setSubject(subject, "UTF-8");
            message.setText(content, "UTF-8", "html");

            mailSender.send(message);
            return true;

        } catch (UnsupportedEncodingException e) {
        	// TODO Auto-generated catch block
        	e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (MailException e) {
            e.printStackTrace();
        }
        
		return false;
	}
}

class MyAuthentication extends Authenticator {
    
    PasswordAuthentication pa;
    
    public MyAuthentication(String id, String pw){
        pa = new PasswordAuthentication(id, pw);
    }
 
    public PasswordAuthentication getPasswordAuthentication() {
        return pa;
    }
}