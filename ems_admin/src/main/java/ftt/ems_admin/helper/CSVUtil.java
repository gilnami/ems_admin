package ftt.ems_admin.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class CSVUtil {

	private String filePath;
	
	public List<Integer> getCellConvertIntegerList(int cellNo) throws Exception{
        
		List<Integer> list = new ArrayList<>();
        try {
            // csv 데이타 파일
            File csv = new File(filePath);
            BufferedReader br = new BufferedReader(new FileReader(csv));
            String line = "";
 
            while ((line = br.readLine()) != null) {
                // -1 옵션은 마지막 "," 이후 빈 공백도 읽기 위한 옵션
                String[] token = line.split(",", -1);
                int cellCount = line.split(",").length;
                for(int i=0;i<cellCount;i++) {
                	if(i == cellNo){
                		try{
                			list.add(Integer.parseInt(token[i]));
                		}catch(NumberFormatException ne){
                			continue;
                		}
                	}
                }
            }
            br.close();
        } 
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } 
        catch (IOException e) {
            e.printStackTrace();
        }
        return list;

	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
}
