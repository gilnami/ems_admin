package ftt.ems_admin.helper;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class JsonSerializer {
	private static ObjectMapper prettySerializer;
	private static ObjectMapper serializer;
	private static ObjectMapper deserializer;
	
	static {
		prettySerializer = new ObjectMapper()
				.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
				.configure(SerializationFeature.INDENT_OUTPUT, true);
		
		serializer = new ObjectMapper()
				.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		
		deserializer = new ObjectMapper()
				.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}
	
	public static String toJson(Object obj) throws JsonProcessingException {
		return prettySerializer.writeValueAsString(obj);
	}
	
	public static String toJsonWithoutIndent(Object obj) throws JsonProcessingException {
		return serializer.writeValueAsString(obj);
	}
	
	public static <T> T fromJson(String jsonDoc, Class<T> clazz) throws JsonParseException, JsonMappingException, IOException {
		return deserializer.readValue(jsonDoc, clazz);
	}
	
	public static <T> T fromJson(String jsonDoc, TypeReference<T> typeRef) throws JsonParseException, JsonMappingException, IOException {
		return deserializer.readValue(jsonDoc, typeRef);
	}
}
