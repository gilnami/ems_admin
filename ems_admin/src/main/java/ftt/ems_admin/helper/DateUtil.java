package ftt.ems_admin.helper;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {

	/**
	 * yyyyMMddHHmmss
	 */
	public static final String FORMAT_FULL_DEFAULT = "yyyyMMddHHmmss";
	/**
	 * yyyy-MM-dd HH:mm:ss
	 */
	public static final String FORMAT_FULL_STR = "yyyy-MM-dd HH:mm:ss";
	
	/**
	 * yyyy-MM-dd HH:mm:ss
	 */
	public static final String FORMAT_DAY_STR = "yyyy-MM-dd";
	
	/**
	 * yyyy년 MM월 dd일 HH시 mm분 ss초
	 */
	public static final String FORMAT_FULL_KOREA = "yyyy년 MM월 dd일 HH시 mm분 ss초";
	
	public static final int YEAR  = Calendar.YEAR;
    public static final int MONTH = Calendar.MONTH;
    public static final int DAY   = Calendar.DAY_OF_MONTH;
    public static final int HOUR = Calendar.HOUR_OF_DAY;
    public static final int MINUTE = Calendar.MINUTE;
    public static final int SECOND = Calendar.SECOND;
    

	/**
	 * 현재시간 timestamp second까지 반환
	 * @return
	 */
	public static long getCurrentTimestamp() {
		
		return System.currentTimeMillis()/1000;
	}

	
	/**
     * 타임스탬프 값 Date 반환
     * @param timestamp
     * @return
     */
    public static Date getDate(long timestamp) {
    	if((timestamp+"").length() == 10)
    	{
    		timestamp *= 1000;
    	}
    	return new Date(timestamp);
    }
    /**
     * 타임스탬프 값으로 Date 반환
     * @param timestamp
     * @return
     */
    public static Date getDate(int timestamp) {
    	return getDate((long)timestamp);
    }
    
    /**
	 * 현재 시간을 문자열 반환한다. =>yyyy-MM-dd HH:mm:ss
	 * @param date	:	Date 객체
	 * @return	String
	 */
	public static String getDateFullString() {
		return getDateString(new Date());
	}
	/**
	 * 현재 시간을 문자열 8자리로 반환한다. => yyyyMMdd
	 * @return	String
	 */
	public static String getDateString() {		
		return getDateFullString().substring(0,  8);
	}
	/**
	 * 입력된 Date 객체에 해당하는 시간을 문자열  반환한다. => yyyy-MM-dd HH:mm:ss
	 * @param date	:	Date 객체
	 * @return	String
	 */
	public static String getDateString(Date date) {
		return getDateString(date, FORMAT_FULL_STR);
	}
	/**
	 * 입력된 Date 객체에 해당하는 시간을 입력된 날짜 포멧에 맞춰 문자열로 반환한다.
	 * @param date		:	Date 객체
	 * @param format	:	날짜 포멧
	 * @return	String
	 */
	public static String getDateString(Date date, String format) {
		return new SimpleDateFormat(format).format(date);
	}
	
	
	
	public static long convertDateToTimeStamp(String date) throws Exception {
		return convertDateToTimeStamp(date, FORMAT_FULL_STR);
	}
	
	/**
	 * 문자열 시간포맷을 Long Timestamp로 변환
	 */
	public static long convertDateToTimeStamp(String date, String format) throws Exception {
		SimpleDateFormat _sfor = new SimpleDateFormat(format);
		Date dd = _sfor.parse(date);
		String convertDate = _sfor.format(dd);
		long _nowReg = Timestamp.valueOf(convertDate).getTime();

		return (long) _nowReg;
	}

	
	/**
	 * 현재 날짜에서 가감한 날짜를 가져온다
	 * 반환 날짜 포멧 : yyyy-MM-dd
	 * @param value	:	현재 날짜에서 가감할 일 수
	 * @return	String
	 */
	public static String getAddDayString(int value) 
	{
		Calendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DATE, value);
		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DAY_STR);
		return sdf.format(calendar.getTime());
	}
	
	/**
	 * Make an int Month from a date
	 * @return int
	 */
	public static int getMonthInt() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM");
		return Integer.parseInt(dateFormat.format(new Date()));
	}

	/**
	 * Make an int Year from a date
	 * @return int
	 */
	public static int getYearInt() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
		return Integer.parseInt(dateFormat.format(new Date()));
	}

}
