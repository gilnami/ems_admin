package ftt.ems_admin.helper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

@Component
public class ExcelReadUtil {

	/**
	 * 엑셀파일의 경로
	 */
	private String filePath;

	/**
	 * 추출할 컬럼 명
	 */
	private List<String> outputColumns;

	/**
	 * 추출을 시작할 행 번호
	 */
	private static int startRow;

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public List<String> getOutputColumns() {

		List<String> temp = new ArrayList<String>();
		temp.addAll(outputColumns);

		return temp;
	}

	public void setOutputColumns(List<String> outputColumns) {

		List<String> temp = new ArrayList<String>();
		temp.addAll(outputColumns);

		this.outputColumns = temp;
	}

	public void setOutputColumns(String... outputColumns) {

		if (this.outputColumns == null) {
			this.outputColumns = new ArrayList<String>();
		}

		for (String ouputColumn : outputColumns) {
			this.outputColumns.add(ouputColumn);
		}
	}

	public static int getStartRow() {
		return startRow;
	}

	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	/**
	 * 
	 * 엑셀파일을 읽어서 Workbook 객체에 리턴한다. XLS와 XLSX 확장자를 비교한다.
	 * 
	 * @param filePath
	 * @return
	 * 
	 */
	public Workbook getWorkbook(String filePath) {

		/**
		 * FileInputStream은 파일의 경로에 있는 파일을 읽어서 Byte로 가져온다.
		 * 
		 * 파일이 존재하지 않는다면은 RuntimeException이 발생된다.
		 */
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(filePath);
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e.getMessage(), e);
		}

		Workbook wb = null;

		/**
		 * 파일의 확장자를 체크해서 .XLS 라면 HSSFWorkbook에 .XLSX라면 XSSFWorkbook에 각각 초기화 한다.
		 */
		if (filePath.toUpperCase().endsWith(".XLS")) {
			try {
				wb = new HSSFWorkbook(fis);
			} catch (IOException e) {
				throw new RuntimeException(e.getMessage(), e);
			}
		} else if (filePath.toUpperCase().endsWith(".XLSX")) {
			try {
				wb = new XSSFWorkbook(fis);
			} catch (IOException e) {
				throw new RuntimeException(e.getMessage(), e);
			}
		}

		return wb;
	}

	/**
	 * Cell에 해당하는 Column Name을 가젼온다(A,B,C..) 만약 Cell이 Null이라면 int cellIndex의 값으로
	 * Column Name을 가져온다.
	 * 
	 * @param cell
	 * @param cellIndex
	 * @return
	 */
	public String getName(Cell cell, int cellIndex) {
		int cellNum = 0;
		if (cell != null) {
			cellNum = cell.getColumnIndex();
		} else {
			cellNum = cellIndex;
		}

		return CellReference.convertNumToColString(cellNum);
	}

	public static String getValue(Cell cell) {

		DataFormatter fmt = new DataFormatter();

		return fmt.formatCellValue(cell);
	}

	public List<Map<String, String>> getRowsConvertMap() {
		// 엑셀 파일 자체
		// 엑셀파일을 읽어 들인다.
		// FileType.getWorkbook() <-- 파일의 확장자에 따라서 적절하게 가져온다.
		Workbook wb = getWorkbook(getFilePath());

		Sheet sheet = wb.getSheetAt(0);
		/**
		 * sheet에서 유효한(데이터가 있는) 행의 개수를 가져온다.
		 */
		int numOfRows = sheet.getPhysicalNumberOfRows();
		int numOfCells = 0;

		Row row = null;
		Cell cell = null;

		String cellName = "";
		Map<String, String> map = null;
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();

		/**
		 * 각 Row만큼 반복을 한다.
		 */
		for (int rowIndex = getStartRow() - 1; rowIndex < numOfRows; rowIndex++) {
			row = sheet.getRow(rowIndex);

			if (row != null) {
				/**
				 * 가져온 Row의 Cell의 개수를 구한다.
				 */
				numOfCells = row.getLastCellNum();
				map = new HashMap<String, String>();
				for (int cellIndex = 0; cellIndex < numOfCells; cellIndex++) {
					cell = row.getCell(cellIndex);
					cellName = getName(cell, cellIndex);
					/**
					 * map객체의 Cell의 이름을 키(Key)로 데이터를 담는다.
					 */
					map.put(cellName, getValue(cell));
				}
				result.add(map);
			}
		}
		return result;
	}
	
	/**
	 * 특정 cell의 List 반환 
	 * @param cellIndex
	 * @return
	 */
	public List<Integer> getCellConvertIntegerList(int cellIndex) {
		Workbook wb = getWorkbook(getFilePath());

		Sheet sheet = wb.getSheetAt(0);
		/**
		 * sheet에서 유효한(데이터가 있는) 행의 개수를 가져온다.
		 */
		int numOfRows = sheet.getPhysicalNumberOfRows();

		Row row = null;
		Cell cell = null;

		List<Integer> list = new ArrayList<>();

		for (int rowIndex = getStartRow() - 1; rowIndex < numOfRows; rowIndex++) {
			row = sheet.getRow(rowIndex);

			if (row != null) {
				cell = row.getCell(cellIndex);
				int value = Integer.parseInt(getValue(cell));
				list.add(value);
			}
		}
		return list;
	}
}