package ftt.ems_admin.entity.member;

public class AccountInfo {

	private int idx;
	private int ssn;
	private long accountIdx;
	private long pimIdx;
	private String accountStatus;
	private String countryCode;
	private String langCode;
	private String recvStatus;
	private int zoneCode;
	private int worldCode;
	private String regDt;
	private String updDt;
	public int getSsn() {
		return ssn;
	}
	public void setSsn(int ssn) {
		this.ssn = ssn;
	}
	public long getAccountIdx() {
		return accountIdx;
	}
	public void setAccountIdx(long accountIdx) {
		this.accountIdx = accountIdx;
	}
	public long getPimIdx() {
		return pimIdx;
	}
	public void setPimIdx(long pimIdx) {
		this.pimIdx = pimIdx;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getLangCode() {
		return langCode;
	}
	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}
	public int getZoneCode() {
		return zoneCode;
	}
	public void setZoneCode(int zoneCode) {
		this.zoneCode = zoneCode;
	}
	public int getWorldCode() {
		return worldCode;
	}
	public void setWorldCode(int worldCode) {
		this.worldCode = worldCode;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getUpdDt() {
		return updDt;
	}
	public void setUpdDt(String updDt) {
		this.updDt = updDt;
	}
	public String getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	@Override
	public String toString() {
		return "AccountInfo [idx=" + idx + ", ssn=" + ssn + ", accountIdx=" + accountIdx + ", pimIdx=" + pimIdx
				+ ", accountStatus=" + accountStatus + ", countryCode=" + countryCode + ", langCode=" + langCode
				+ ", recvStatus=" + recvStatus + ", zoneCode=" + zoneCode + ", worldCode=" + worldCode + ", regDt="
				+ regDt + ", updDt=" + updDt + "]";
	}
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public String getRecvStatus() {
		return recvStatus;
	}
	public void setRecvStatus(String recvStatus) {
		this.recvStatus = recvStatus;
	}
	
	
}
