package ftt.ems_admin.entity.type;

import java.util.HashMap;
import java.util.LinkedHashMap;

public final class DEF {
	public enum MEMBER_STATE { 
		NORMAL("N"), BLOCK("B"), REST("R");
		
		private String value;
		
		private MEMBER_STATE(String value){
			this.value = value;
		}
		
		public String getValue(){
			return value;
		}
		
		public static HashMap<String, DEF.MEMBER_STATE> getList(){
			HashMap<String, DEF.MEMBER_STATE> map = new LinkedHashMap<String, DEF.MEMBER_STATE>();
			for(DEF.MEMBER_STATE o : DEF.MEMBER_STATE.values()){
				map.put(o.getValue(), o);
			}
			return map;
		}
		
		public static DEF.MEMBER_STATE getDefine(String value){
			
			for(DEF.MEMBER_STATE o : DEF.MEMBER_STATE.values()){
				if ( value.equals(o.getValue()) )
					return o;
			}
						
			return null;
		}
	};
	
	public enum RECV_STATE { 
		AVAIL("A"), DENY("D");
		
		private String value;
		
		private RECV_STATE(String value){
			this.value = value;
		}
		
		public String getValue(){
			return value;
		}
		
		public static HashMap<String, DEF.RECV_STATE> getList(){
			HashMap<String, DEF.RECV_STATE> map = new LinkedHashMap<String, DEF.RECV_STATE>();
			for(DEF.RECV_STATE o : DEF.RECV_STATE.values()){
				map.put(o.getValue(), o);
			}
			return map;
		}
		
		public static DEF.RECV_STATE getDefine(String value){
			
			for(DEF.RECV_STATE o : DEF.RECV_STATE.values()){
				if ( value.equals(o.getValue()) )
					return o;
			}
						
			return null;
		}
	};
	
	public enum LANG_CODE { 
		// 한국어, 영어, 일본어, 중국어 간체, 중국어 번체, 태국어, 인도네시아어, 프랑스어, 독일어
		KO("KO"), EN("EN"), JA("JA"), ZH_CN("ZH_CN"), ZH_TW("ZH_TW"), TH("TH"), ID("ID"), FR("FR"), DE("DE");
		
		private String value;
		
		private LANG_CODE(String value){
			this.value = value;
		}
		
		public String getValue(){
			return value;
		}
		
		public static DEF.LANG_CODE getDefine(String value){
			
			for(DEF.LANG_CODE o : DEF.LANG_CODE.values()){
				if ( value.equals(o.getValue()) )
					return o;
			}
						
			return null;
		}
	};
	
	public enum COUNTRY_CODE { 
		ETC("00");
		
		private String value;
		
		private COUNTRY_CODE(String value){
			this.value = value;
		}
		
		public String getValue(){
			return value;
		}
	};

	public enum PIM_TYPE { 
		EMAIL("E"), PHONE("P");
		
		private String value;
		
		private PIM_TYPE(String value){
			this.value = value;
		}
		
		public String getValue(){
			return value;
		}
		
		public static DEF.PIM_TYPE getDefine(String value){
			
			for(DEF.PIM_TYPE o : DEF.PIM_TYPE.values()){
				if ( value.equals(o.getValue()) )
					return o;
			}
						
			return null;
		}
	};
	
	public enum DISP_STATUS { 
		NONE(0), INNER(1), PUBLIC(3);
		
		private int value;
		
		private DISP_STATUS(int value){
			this.value = value;
		}
		
		public int getValue(){
			return value;
		}
		
		public static DEF.DISP_STATUS getDefine(int value){
			
			for(DEF.DISP_STATUS o : DEF.DISP_STATUS.values()){
				if ( value == o.getValue() )
					return o;
			}
						
			return null;
		}
	};
}