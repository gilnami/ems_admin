package ftt.ems_admin.entity;

public class TesterEntity {

	private int ssn;
	private int pimIdx;
	private int accountIdx;
	private String recvStatus;
	private String accountStatus;
	private String countryCode;
	private String langCode;
	public int getSsn() {
		return ssn;
	}
	public void setSsn(int ssn) {
		this.ssn = ssn;
	}
	public int getPimIdx() {
		return pimIdx;
	}
	public void setPimIdx(int pimIdx) {
		this.pimIdx = pimIdx;
	}
	public int getAccountIdx() {
		return accountIdx;
	}
	public void setAccountIdx(int accountIdx) {
		this.accountIdx = accountIdx;
	}
	public String getAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getLangCode() {
		return langCode;
	}
	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}
	@Override
	public String toString() {
		return "TesterEntity [ssn=" + ssn + ", pimIdx=" + pimIdx + ", accountIdx=" + accountIdx + ", accountStatus="
				+ accountStatus + ", countryCode=" + countryCode + ", langCode=" + langCode + "]";
	}
	public String getRecvStatus() {
		return recvStatus;
	}
	public void setRecvStatus(String recvStatus) {
		this.recvStatus = recvStatus;
	}
	
}
