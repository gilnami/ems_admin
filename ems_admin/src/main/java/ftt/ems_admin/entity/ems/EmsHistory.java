package ftt.ems_admin.entity.ems;

public class EmsHistory {

	private long idx;
	private long actUsn;
	private int ssn;
	private String actContent;
	private String regDt;
	
	public long getIdx() {
		return idx;
	}
	public void setIdx(long idx) {
		this.idx = idx;
	}
	public long getActUsn() {
		return actUsn;
	}
	public void setActUsn(long actUsn) {
		this.actUsn = actUsn;
	}
	public int getSsn() {
		return ssn;
	}
	public void setSsn(int ssn) {
		this.ssn = ssn;
	}
	public String getActContent() {
		return actContent;
	}
	public void setActContent(String actContent) {
		this.actContent = actContent;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	@Override
	public String toString() {
		return "EmsHistory [idx=" + idx + ", actUsn=" + actUsn + ", ssn=" + ssn + ", actContent=" + actContent
				+ ", regDt=" + regDt + "]";
	}
}
