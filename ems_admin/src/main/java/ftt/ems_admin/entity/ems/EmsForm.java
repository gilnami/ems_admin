package ftt.ems_admin.entity.ems;

import java.util.List;

import javax.validation.constraints.NotNull;

public class EmsForm {

	private int ssn;
	
	@NotNull
	private List<Integer> zone;
	
	@NotNull
	private List<String> country;
	
	@NotNull
	private List<Integer> market;
	
	private int dispStatus;
	
	@NotNull
	private List<EmsLang> message;
	
	@NotNull
	private String rewardStr;
	
	private int rewardIdx; 
	
	private int messageIdx;
	
	@NotNull
	private String defaultMessage;
	
	public List<Integer> getZone() {
		return zone;
	}
	public void setZone(List<Integer> zone) {
		this.zone = zone;
	}
	public List<String> getCountry() {
		return country;
	}
	public void setCountry(List<String> country) {
		this.country = country;
	}
	public List<Integer> getMarket() {
		return market;
	}
	public void setMarket(List<Integer> market) {
		this.market = market;
	}
	public int getDispStatus() {
		return dispStatus;
	}
	public void setDispStatus(int dispStatus) {
		this.dispStatus = dispStatus;
	}
	public String getRewardStr() {
		return rewardStr;
	}
	public void setRewardStr(String rewardStr) {
		this.rewardStr = rewardStr;
	}
	public String getDefaultMessage() {
		return defaultMessage;
	}
	public void setDefaultMessage(String defaultMessage) {
		this.defaultMessage = defaultMessage;
	}
	@Override
	public String toString() {
		return "EmsForm [ssn=" + ssn + ", zone=" + zone + ", country=" + country + ", market=" + market
				+ ", dispStatus=" + dispStatus + ", message=" + message + ", rewardStr=" + rewardStr + ", rewardIdx="
				+ rewardIdx + ", messageIdx=" + messageIdx + ", defaultMessage=" + defaultMessage + "]";
	}
	public int getSsn() {
		return ssn;
	}
	public void setSsn(int ssn) {
		this.ssn = ssn;
	}
	public int getMessageIdx() {
		return messageIdx;
	}
	public void setMessageIdx(int messageIdx) {
		this.messageIdx = messageIdx;
	}
	public List<EmsLang> getMessage() {
		return message;
	}
	public void setMessage(List<EmsLang> message) {
		this.message = message;
	}
	public int getRewardIdx() {
		return rewardIdx;
	}
	public void setRewardIdx(int rewardIdx) {
		this.rewardIdx = rewardIdx;
	}
	

	
}
