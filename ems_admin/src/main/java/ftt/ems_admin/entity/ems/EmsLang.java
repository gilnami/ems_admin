package ftt.ems_admin.entity.ems;

public class EmsLang {

	private int ssn;
	private String langCode;
	private String message;

	public int getSsn() {
		return ssn;
	}

	public void setSsn(int ssn) {
		this.ssn = ssn;
	}

	public String getLangCode() {
		return langCode;
	}

	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "EmsLang [ssn=" + ssn + ", langCode=" + langCode + ", message=" + message + "]";
	}

}
