package ftt.ems_admin.entity.ems;

public class EmsCondition {

	private int ssn;
	private long rewardIdx;
	private int dispStatus;
	private String regDt;
	private String modDt;
	private long actUsn;
	public int getSsn() {
		return ssn;
	}
	public void setSsn(int ssn) {
		this.ssn = ssn;
	}
	public long getRewardIdx() {
		return rewardIdx;
	}
	public void setRewardIdx(long rewardIdx) {
		this.rewardIdx = rewardIdx;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getModDt() {
		return modDt;
	}
	public void setModDt(String modDt) {
		this.modDt = modDt;
	}
	public long getActUsn() {
		return actUsn;
	}
	public void setActUsn(long actUsn) {
		this.actUsn = actUsn;
	}
	public int getDispStatus() {
		return dispStatus;
	}
	public void setDispStatus(int dispStatus) {
		this.dispStatus = dispStatus;
	}
}
