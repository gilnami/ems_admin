package ftt.ems_admin.entity.ems;

public class EmsReward {

	private int ssn;
	private String rewardStr;
	private int rewardIdx;
	private String defaultMessage;
	private int messageIdx;
	public int getSsn() {
		return ssn;
	}
	public void setSsn(int ssn) {
		this.ssn = ssn;
	}
	public String getRewardStr() {
		return rewardStr;
	}
	public void setRewardStr(String rewardStr) {
		this.rewardStr = rewardStr;
	}
	public int getRewardIdx() {
		return rewardIdx;
	}
	public void setRewardIdx(int rewardIdx) {
		this.rewardIdx = rewardIdx;
	}
	public String getDefaultMessage() {
		return defaultMessage;
	}
	public void setDefaultMessage(String defaultMessage) {
		this.defaultMessage = defaultMessage;
	}
	public int getMessageIdx() {
		return messageIdx;
	}
	public void setMessageIdx(int messageIdx) {
		this.messageIdx = messageIdx;
	}
	
}
