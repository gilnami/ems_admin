package ftt.ems_admin.entity.email;

public class EmailExport {

	private long pimIdx;
	private String countryCode;
	private String langCode;
	private String regDt;
	private long accountIdx;
	private String unsubscribeLink;
	
	public long getPimIdx() {
		return pimIdx;
	}
	public void setPimIdx(long pimIdx) {
		this.pimIdx = pimIdx;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getLangCode() {
		return langCode;
	}
	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	@Override
	public String toString() {
		return "EmailExport [pimIdx=" + pimIdx + ", countryCode=" + countryCode + ", langCode=" + langCode + ", regDt="
				+ regDt + ", accountIdx=" + accountIdx + ", unsubscribeLink=" + unsubscribeLink + "]";
	}
	public long getAccountIdx() {
		return accountIdx;
	}
	public void setAccountIdx(long accountIdx) {
		this.accountIdx = accountIdx;
	}
	public String getUnsubscribeLink() {
		return unsubscribeLink;
	}
	public void setUnsubscribeLink(String unsubscribeLink) {
		this.unsubscribeLink = unsubscribeLink;
	}
}
