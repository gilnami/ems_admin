package ftt.ems_admin.entity.email;

import java.util.List;

import javax.validation.constraints.NotNull;

public class ExportForm {

	private int ssn;
	
	private List<String> country;

	private List<String> lang;

	public int getSsn() {
		return ssn;
	}

	public void setSsn(int ssn) {
		this.ssn = ssn;
	}

	public List<String> getCountry() {
		return country;
	}

	public void setCountry(List<String> country) {
		this.country = country;
	}

	public List<String> getLang() {
		return lang;
	}

	public void setLang(List<String> lang) {
		this.lang = lang;
	}

	@Override
	public String toString() {
		return "EmsForm [ssn=" + ssn + ", country=" + country + ", lang=" + lang + "]";
	}
	
	
}
