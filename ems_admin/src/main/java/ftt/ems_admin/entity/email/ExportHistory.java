package ftt.ems_admin.entity.email;

import ftt.ems_admin.helper.DateUtil;

/**
 * 이메일 추출 히스토리  
 *
 */
public class ExportHistory {

	private int ssn;
	private long actUsn;
	private int historyType;
	private String countryCode;
	private String langCode;
	private String regDt;
	
	public ExportHistory(int ssn, long actUsn, int historyType, String countryCode, String langCode){
		this.ssn = ssn;
		this.actUsn = actUsn;
		this.historyType = historyType;
		this.countryCode = countryCode;
		this.langCode = langCode;
		this.regDt = DateUtil.getDateFullString();
	}

	public int getSsn() {
		return ssn;
	}
	public void setSsn(int ssn) {
		this.ssn = ssn;
	}
	public long getActUsn() {
		return actUsn;
	}
	public void setActUsn(long actUsn) {
		this.actUsn = actUsn;
	}
	public int getHistoryType() {
		return historyType;
	}
	public void setHistoryType(int historyType) {
		this.historyType = historyType;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getLangCode() {
		return langCode;
	}
	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	
}
