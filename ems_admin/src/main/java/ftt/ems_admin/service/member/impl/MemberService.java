package ftt.ems_admin.service.member.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import freemarker.template.Configuration;
import ftt.common.pim.impl.PimClient;
import ftt.common.reward.impl.RewardClient;
import ftt.ems_admin.config.AppConfig;
import ftt.ems_admin.config.GameDataSourceConfig;
import ftt.ems_admin.dao.member.IMemberDao;
import ftt.ems_admin.entity.ems.EmsReward;
import ftt.ems_admin.entity.member.AccountInfo;
import ftt.ems_admin.entity.type.DEF;
import ftt.ems_admin.helper.MailClient;
import ftt.ems_admin.service.game.IGameService;
import ftt.ems_admin.service.member.IMemberService;

@Service
public class MemberService implements IMemberService {

	@Autowired
	private IMemberDao memberDao;

	@Autowired
	private IGameService gameService;

	@Autowired
	@Lazy
	private RewardClient rewardClient;
	
	@Autowired
	@Lazy
	private PimClient pimClient;
	
	@Autowired
	private AppConfig appConfig;

	@Autowired
	private GameDataSourceConfig gameDataSourceConfig;
	
	@Autowired
    Configuration freemarkerConfiguration;
	
	@Autowired
	private MailClient mailClient;
	
	@Autowired
	private MessageSource messageSource;
	

	private Logger logger = LoggerFactory.getLogger(getClass());

	private static int rewardServiceIdx = 1;
	private static int rewardExpireMinutes = 525600; // minutes, 1 year

	/**
	 * 게임db 동기화 실패건 재시도 
	 * - 1. 하루전까지의 동기화 실패 리스트 조회 
	 * - 2. 게임db별 동기화 시도 
	 * - 3. 동기화 성공시
	 * 실패table delete
	 */
	@Override
	public void retryAccountSyncToGamedb(int intervalDays) throws Exception {

		List<AccountInfo> list = memberDao.getGameAccountSyncFailList(intervalDays);
		if (list == null || list.size() == 0)
			return;

		for (AccountInfo info : list) {
			
			if(!gameDataSourceConfig.isValidGame(info.getSsn())) continue;
			
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("accountIdx", info.getAccountIdx());
			map.put("pimIdx", info.getPimIdx());

			// 게임DB 동기화
			gameService.setEmailPimIdx(info.getSsn(), map);

			if ((int) map.get("poResult") == 0) {
				memberDao.delGameAccountSyncFail(info.getSsn(), info.getAccountIdx());
				logger.info("retry account sync success !! {} : {}", info.getSsn(), info.getAccountIdx());
			}
		}
	}

	/**
	 * 리워드 실패 재지급 시도 
	 * - 1. 지급실패 목록 조회 
	 * - 2. 게임별 리워드 세팅 조회 
	 * - 3. 재지급 시도 
	 * - 4. 재지급 완료시 실패table
	 * delete
	 */
	public void retrySendReward(int intervalDays) throws Exception {

		List<AccountInfo> list = memberDao.getRewardFailList(intervalDays);
		if (list == null || list.size() == 0)
			return;

		List<EmsReward> rewardList = memberDao.getRewardBoxAllList();
		Map<Integer, EmsReward> rewardMap = new HashMap<Integer, EmsReward>();
		for (EmsReward reward : rewardList) {
			rewardMap.put(reward.getSsn(), reward);
		}

		for (AccountInfo info : list) {
			
			if(!gameDataSourceConfig.isValidGame(info.getSsn())) continue;
			
			EmsReward rewardBox = rewardMap.get(info.getSsn());

			// 리워드 API 호출
			boolean rewardResult = rewardClient.giveReward(info.getSsn(), rewardServiceIdx, rewardBox.getRewardIdx(),
					info.getWorldCode(), Long.toString(info.getAccountIdx()), rewardBox.getRewardStr(),
					rewardBox.getMessageIdx(), rewardBox.getDefaultMessage(), rewardExpireMinutes);

			if (rewardResult) {
				memberDao.delRewardFail(info.getIdx());
			}
		}
	}

	/**
	 * 탈퇴 7일 경과 유저 삭제 
	 * - 1. ems에서 관리중인 game ssn list 
	 * - 2. 각 게임의 탈퇴7일 경과 유저 리스트 조회 
	 * - 3. emdb내 삭제
	 * 
	 * @throws Exception
	 */
	@Override
	public void unregMemberSyncToEmdb() throws Exception {

		Iterator<String> set = gameDataSourceConfig.getDatasource().keySet().iterator();
		while (set.hasNext()) {
			String database = set.next();
			if (database.indexOf("gamedb_") < 0) continue;
			String ssnStr = database.substring(database.indexOf("_") + 1);
			int ssn = Integer.parseInt(ssnStr);
			List<Long> list = gameService.getUnregUserList(ssn);
			
			for (Long accountIdx : list) {
				// 유저 삭제
				memberDao.delUnregAccount(ssn, accountIdx);
			}
		}
	}

	/**
	 * Game의 휴면유저 -> Emdb 상태 갱신 
	 * 1. ems에서 관리중인 game ssn list 
	 * 2. 각 게임의 휴면 유저 리스트 조회 
	 * 3. emdb내 상태 갱신
	 */
	@Override
	public void sleepUserSyncToEmdb() throws Exception {

		Iterator<String> set = gameDataSourceConfig.getDatasource().keySet().iterator();
		while (set.hasNext()) {
			String database = set.next();
			if (database.indexOf("gamedb_") < 0) continue;
			String ssnStr = database.substring(database.indexOf("_") + 1);
			int ssn = Integer.parseInt(ssnStr);
			List<Long> list = gameService.getSleepUserList(ssn);
			
			if(list == null || list.size() == 0) continue;
			
			String accountIdxList = list.stream().map(o -> o.toString()).collect(Collectors.joining(", "));
			// 유저 상태 갱신
			memberDao.uptAccountStatusBatch(ssn, DEF.MEMBER_STATE.REST.getValue(), accountIdxList);
		}

	}
	
	/**
	 * Game의 휴면 복귀 유저 -> Emdb 상태 갱신 
	 * 1. ems에서 관리중인 game ssn list 
	 * 2. 각 게임의 휴면 복귀 유저 리스트 조회 
	 * 3. emdb내 상태 갱신
	 */
	@Override
	public void returnUserSyncToEmdb() throws Exception {

		Iterator<String> set = gameDataSourceConfig.getDatasource().keySet().iterator();
		while (set.hasNext()) {
			String database = set.next();
			if (database.indexOf("gamedb_") < 0) continue;
			String ssnStr = database.substring(database.indexOf("_") + 1);
			int ssn = Integer.parseInt(ssnStr);
			List<Long> list = gameService.getReturnUserList(ssn);
			
			if(list == null || list.size() == 0) continue;
			
			String accountIdxList = list.stream().map(o -> o.toString()).collect(Collectors.joining(", "));
			// 유저 상태 갱신
			memberDao.uptAccountStatusBatch(ssn, DEF.MEMBER_STATE.NORMAL.getValue(), accountIdxList);
		}

	}

	
	/**
	 * 각 게임 제재 해지 유저 -> Emdb 상태 갱신
	 * 1. ems의 전체 제재유저 조회 
	 * 2. ems에서 관리중인 game ssn list 
	 * 3. 각 게임별 ems의 제재유저의 게임의 제재상태 조회
	 * 4. ems 제재유저와 게임의 제재상태 비교하여 제재해제된 유저 필터링
	 * 5. 제재해제된 유저의 ems 상태 갱신
	 * 
	 * @throws Exception
	 */
	@Override
	public void unBlockUserSyncToEmdb() throws Exception {
		
		//emdb내의 제재유저 전체 조회 
		List<AccountInfo> currentEmBlockList = memberDao.getBlockAccountList();
		if(currentEmBlockList == null || currentEmBlockList.size() == 0) return;

		//게임별 동기화 시작
		Iterator<String> set = gameDataSourceConfig.getDatasource().keySet().iterator();
		while (set.hasNext()) {
			String database = set.next();
			if (database.indexOf("gamedb_") < 0) continue;
			
			String ssnStr = database.substring(database.indexOf("_") + 1);
			int ssn = Integer.parseInt(ssnStr);

			//emdb 제재유저 ssn별 콤마로 구분하여 문자열 생성
			String blockAccountIdxList = currentEmBlockList.stream().filter(user -> (user.getSsn() == ssn))
					.map(o -> Long.toString(o.getAccountIdx())).collect(Collectors.joining(", "));
			
			logger.debug("blockAccountIdxList : " + blockAccountIdxList.toString());
			
			//game db에서도 해당 유저가 제재상태인지 조회 (제재상태의 유저가 리턴됨) 
			List<Long> currentGameBlockList = gameService.getBlockUserListByAccount(ssn, blockAccountIdxList);
			
			logger.debug("currentGameBlockList : " + currentGameBlockList.toString());
			
			//(currentEmBlockList)emdb 제재유저 - (currentGameBlockList) 게임제재유저  = unBlockAccountIdxList(제재 해지 유저)
		    List<AccountInfo> unBlockUserList = currentEmBlockList.stream().filter(emUser -> {
		    	for(Long gameUser : currentGameBlockList){
		    		if(emUser.getAccountIdx() == gameUser) {
		    			currentGameBlockList.remove(gameUser);
		    			return false;
		    		}
		    	}
		    	return true;
		    }).collect(Collectors.toList());
		    
		    if(unBlockUserList == null || unBlockUserList.size() == 0) continue;
		    
		    String unBlockAccountIdxList = unBlockUserList.stream().filter(user -> (user.getSsn() == ssn))
					.map(o -> Long.toString(o.getAccountIdx())).collect(Collectors.joining(", "));
			
		    //유저 상태 갱신
			memberDao.uptAccountStatusBatch(ssn, DEF.MEMBER_STATE.NORMAL.getValue(), unBlockAccountIdxList);
		}
	}

	
	/**
	 * 게임 제재 유저 -> Emdb 상태 갱신 
	 * 1. ems에서 관리중인 game ssn list 
	 * 2. 각 게임의 재재 유저 리스트 조회 
	 * 3. emdb내 상태 갱신
	 * @throws Exception
	 */
	@Override
	public void blockUserSyncToEmdb() throws Exception {

		Iterator<String> set = gameDataSourceConfig.getDatasource().keySet().iterator();
		while (set.hasNext()) {
			String database = set.next();
			if (database.indexOf("gamedb_") < 0) continue;
			String ssnStr = database.substring(database.indexOf("_") + 1);
			int ssn = Integer.parseInt(ssnStr);
			List<Long> list = gameService.getBlockUserListByRange(ssn);
			if(list == null || list.size() == 0) continue;
			String accountIdxList = list.stream().map(o -> o.toString()).collect(Collectors.joining(", "));
			
			// 유저 상태 갱신
			memberDao.uptAccountStatusBatch(ssn, DEF.MEMBER_STATE.BLOCK.getValue(), accountIdxList);
		}
	}
	
	/**
	 * 이메일 수신 재동의 메일 발송
	 */
	@Override
	public void sendEmailTerms() throws Exception {
		List<Integer> list = memberDao.getEmailTermsUserlist();
		String title = messageSource.getMessage("email.terms.title", null, Locale.KOREAN);
		HashMap<String, Object> contentMap = new HashMap<String, Object>();
		contentMap.put("content", messageSource.getMessage("email.terms.content", null, Locale.KOREAN));
		String content = FreeMarkerTemplateUtils.processTemplateIntoString(freemarkerConfiguration.getTemplate("/terms.ftl"), contentMap);
		
		String email = "";
		for(Integer pimIdx : list){
			email = pimClient.decrypt(ftt.common.pim.type.DEF.PIM_TYPE.EMAIL.getValue(), pimIdx);
			
			mailClient.sendAsyncMail(email, title, content);
			
			//수신 재동의 상태 갱신
			memberDao.uptEmMainStatus(DEF.RECV_STATE.AVAIL.getValue(), pimIdx);
		}
	}
	
	/**
	 * 유저 조회 
	 */
	@Override
	public List<AccountInfo> getSearchUser(int ssn, long pimIdx, long accountIdx) throws Exception {
		return memberDao.getSearchUser(ssn, pimIdx, accountIdx);
	}
	
	@Override
	public void uptAccountStatus(int ssn, String accountStatus, long accountIdx) throws Exception {
		memberDao.uptAccountStatus(ssn, accountStatus, accountIdx);
	}
	
	@Override
	public void uptRecvStatus(String recvStatus, long pimIdx) throws Exception {
		memberDao.uptEmMainStatus(recvStatus, pimIdx);
	}
}
