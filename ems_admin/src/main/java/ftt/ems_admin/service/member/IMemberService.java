package ftt.ems_admin.service.member;

import java.util.List;

import ftt.ems_admin.entity.member.AccountInfo;

public interface IMemberService {
	
	public void retryAccountSyncToGamedb(int intervalDays) throws Exception;
	
	public void retrySendReward(int intervalDays) throws Exception;
	
	public void unregMemberSyncToEmdb() throws Exception;
	
	public void sleepUserSyncToEmdb() throws Exception;

	public void blockUserSyncToEmdb() throws Exception;

	public void unBlockUserSyncToEmdb() throws Exception;

	public void sendEmailTerms() throws Exception;

	public void returnUserSyncToEmdb() throws Exception;

	public List<AccountInfo> getSearchUser(int ssn, long pimIdx, long accountIdx) throws Exception;

	public void uptAccountStatus(int ssn, String accountStatus, long accountIdx) throws Exception;

	public void uptRecvStatus(String recvStatus, long pimIdx) throws Exception;
	
}
