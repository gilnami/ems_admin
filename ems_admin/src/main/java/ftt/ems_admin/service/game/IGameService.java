package ftt.ems_admin.service.game;

import java.util.HashMap;
import java.util.List;

public interface IGameService {
	
	public void setEmailPimIdx(int ssn, HashMap<String, Object> map);
	
	public List<Long> getUnregUserList(int ssn);
	
	public List<Long> getSleepUserList(int ssn);
	
	public List<Long> getReturnUserList(int ssn);
	
	public List<Long> getBlockUserListByRange(int ssn);
	
	public List<Long> getBlockUserListByAccount(int ssn, String accountIdxList);
}
