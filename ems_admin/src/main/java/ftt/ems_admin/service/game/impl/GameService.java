package ftt.ems_admin.service.game.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftt.ems_admin.dao.game.IGameDao;
import ftt.ems_admin.service.game.IGameService;

@Service
public class GameService implements IGameService {

	@Autowired
	private IGameDao gameDao;

	/*
	 * ############## GameDataAspect를 통해서 args[0]의 인자에 ssn을 반드시 보내야
	 * datasource.lookup ##############
	 */

	/**
	 * 게임DB 유저정보 동기화 emdb -> gamedb
	 */
	@Override
	public void setEmailPimIdx(int ssn, HashMap<String, Object> map) {
		gameDao.setEmailPimIdx(map);
	}

	/**
	 * 탈퇴 후 7일 경과 유저 목록
	 * 
	 * @param ssn
	 * @return
	 */
	@Override
	public List<Long> getUnregUserList(int ssn) {
		return gameDao.getUnregUserList();
	}

	/**
	 *휴면유저(365일 미접속) 목록
	 * 
	 * @param ssn
	 * @return
	 */
	@Override
	public List<Long> getSleepUserList(int ssn) {
		return gameDao.getSleepUserList();
	}
	
	/**
	 * 제재 유저 목록 조회 
	 * @param ssn
	 * @return
	 */
	@Override
	public List<Long> getBlockUserListByRange(int ssn) {
		return gameDao.getBlockUserListByRange();
	}
	
	/**
	 * 게임의 유저별 제재 상태 조회 
	 */
	@Override
	public List<Long> getBlockUserListByAccount(int ssn, String accountIdxList) {
		return gameDao.getBlockUserListByAccount(accountIdxList);
	}

	/**
	 * 휴면 복귀유저 조회 
	 */
	@Override
	public List<Long> getReturnUserList(int ssn) {
		return gameDao.getReturnUserList();
	}
}
