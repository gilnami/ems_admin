package ftt.ems_admin.service.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import ftt.ems_admin.service.member.IMemberService;

@Service
public class BatchScheduleService {

	@Autowired
	private IMemberService memberService;

	/**
	 * Emdb -> GameDB 유저 동기화 실패 목록 재시도
	 * 
	 * @throws Exception
	 */
	@Scheduled(cron = "1/10 * * * * ?") // 1,11,21,31,41,51초에 실행
	public void retryAccountSyncToGamedb() throws Exception {
		int intervalDays = 1;
		memberService.retryAccountSyncToGamedb(intervalDays);
	}

	/**
	 * 보상 지급 실패 재시도
	 * 
	 * @throws Exception
	 */
	@Scheduled(cron = "5/10 * * * * ?") // 5,15,25,35,45,55초에 실행
	public void retrySendReward() throws Exception {
		int intervalDays = 1;
		memberService.retrySendReward(intervalDays);
	}

	/**
	 * 탈퇴 유저 동기화
	 * 
	 * @throws Exception
	 */
	@Scheduled(cron = "10 0 3,4 * * ?") // 매일 새벽3,4시 10초에
	public void unregMemberSyncToEmdb() throws Exception {
		memberService.unregMemberSyncToEmdb();
	}

	/**
	 * 휴면유저 동기화
	 * 
	 * @throws Exception
	 */
	@Scheduled(cron = "10 10 3,4 * * ?") // 매일 새벽3,4시 10분 10초에
	public void sleepUserSyncToEmdb() throws Exception {
		memberService.sleepUserSyncToEmdb();
	}
	
	/**
	 * (휴면)복귀 유저 동기화 
	 * @throws Exception
	 */
	@Scheduled(cron = "10 20 3,4 * * ?") // 매일 새벽3,4시 20분 10초에
	public void returnUserSyncToEmdb() throws Exception {
		memberService.returnUserSyncToEmdb();
	}
	
	
	/**
	 * 제재 해제 유저 동기화 
	 * @throws Exception
	 */
	@Scheduled(cron = "10 30 3,4 * * ?") // 매일 새벽3,4시 30분 10초에
	public void unBlockUserSyncToEmdb() throws Exception {
		memberService.unBlockUserSyncToEmdb();
	}
	
	/**
	 * 제재유저 동기화 
	 * @throws Exception
	 */
	@Scheduled(cron = "10 40 3,4 * * ?") // 매일 새벽3,4시 40분 10초에
	public void blockUserSyncToEmdb() throws Exception { 
		memberService.blockUserSyncToEmdb();
	}

	/**
	 * 이메일 수신 재동의 메일 발송
	 * 
	 * @throws Exception
	 */
	@Scheduled(cron = "10 10 6 * * ?") // 매일 새벽 6시 10분 10초에
	public void sendEmailTerms() throws Exception {
		memberService.sendEmailTerms();
	}
}
