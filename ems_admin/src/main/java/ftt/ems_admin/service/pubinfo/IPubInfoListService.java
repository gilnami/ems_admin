package ftt.ems_admin.service.pubinfo;

import java.util.List;

import ftt.ems_admin.entity.pubinfo.EmsGame;


public interface IPubInfoListService {
	public List<EmsGame> getAvailableGameList();
	public List<Integer> getAvailableZoneCodeList(int ssn);
}
