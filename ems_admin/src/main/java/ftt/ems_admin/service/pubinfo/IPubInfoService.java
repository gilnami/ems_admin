package ftt.ems_admin.service.pubinfo;

import java.util.List;
import java.util.Map;

import ftt.ems_admin.entity.pubinfo.GameCountry;
import ftt.ems_admin.entity.pubinfo.GameLanguage;
import ftt.ems_admin.entity.pubinfo.GameMarket;
import ftt.ems_admin.entity.pubinfo.GameZone;

public interface IPubInfoService {
	public List<GameZone> getAvailableZoneList();
	public List<GameCountry> getAvailableCountryList();
	public Map<String, GameCountry> getAvailableCountryMap();
	public Map<Integer, List<String>> getAvailableZoneCountryListMap();
	public List<GameMarket> getAvailableMarketList();
	public List<GameCountry> getMarketCountryList(int market);
	public Map<Integer, List<Integer>> getAvailableZoneMarketListMap();
	public List<GameLanguage> getAvailableLangList();
	public Map<Integer, List<Integer>> getAvailableZoneLangListMap();
}
