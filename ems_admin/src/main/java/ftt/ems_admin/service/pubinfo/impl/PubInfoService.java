package ftt.ems_admin.service.pubinfo.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import ftt.common.pub.code.CodeObject;
import ftt.common.pub.code.ICountryCodeService;
import ftt.common.pub.code.ILangCodeService;
import ftt.common.pub.code.IMarketCodeService;
import ftt.common.pub.code.IZoneCodeService;
import ftt.common.pub.pubinfo.IPubInfoCountryService;
import ftt.common.pub.pubinfo.IPubInfoZoneService;
import ftt.common.pub.pubinfo.PubInfoZone;
import ftt.common.pub.pubinfo.PubInfoZoneData;
import ftt.common.pub.pubinfo.PubInfoZoneData.MarketData;
import ftt.common.pub.pubinfo.PubInfoZoneDataField;
import ftt.ems_admin.entity.pubinfo.GameCountry;
import ftt.ems_admin.entity.pubinfo.GameLanguage;
import ftt.ems_admin.entity.pubinfo.GameMarket;
import ftt.ems_admin.entity.pubinfo.GameZone;
import ftt.ems_admin.service.pubinfo.IPubInfoService;

@Scope("prototype")
@Service
public class PubInfoService implements IPubInfoService {
	
	
	@Autowired
	private IMarketCodeService marketCodeSvc;
	
	@Autowired
	private ILangCodeService langCodeSvc;
	
	@Autowired
	private ICountryCodeService countryCodeSvc;
	
	@Autowired
	private IPubInfoZoneService pubInfoZoneSvc;
	
	@Autowired
	private IPubInfoCountryService pubInfoCountrySvc;
	
	@Autowired
	private IZoneCodeService zoneCodeSvc;
	
	private int ssn = 0;
	private int zone = 0;
	private boolean loaded = false;
	private Map<Integer, PubInfoZoneData> pubInfoZoneDataMap = null;

	private List<GameCountry>  countryList = null;						// 국가 목록(존 입력시 현재 존, 존 미입력시 전체 존)
	private List<GameMarket>   marketList = null;							// 마켓 목록(존 입력시 현재 존, 존 미입력시 전체 존)
	private List<GameLanguage> langList = null;							// 언어 목록
	
	private Map<Integer, List<String>> zoneCountryCodeListMap = null;		// 서비스 존별 국가 코드 목록
	private Map<Integer, List<Integer>> zoneMarketCodeListMap = null;		// 서비스 존별 마켓 코드 목록
	private Map<Integer, List<Integer>> zoneLangCodeListMap   = null;		// 서비스 존별 언어 코드 목록
	
	
	// 등록/수정 할 서비스 존의 데이터
	private final static String[] zoneDataQueryFields = new String[] { 
			PubInfoZoneDataField.country,
			PubInfoZoneDataField.market,
			PubInfoZoneDataField.lang,
	};
	
	public PubInfoService(int ssn, int zone) {
		this.ssn = ssn;
		this.zone = zone;
	}
	
	// 입력 서비스 존의 데이터 추출
	private void loadZoneDataMap() {
		if (loaded) return;
		
		if (zone > 0) {
			pubInfoZoneDataMap = pubInfoZoneSvc.getZoneData(ssn, zone, zoneDataQueryFields);
		} else {
			pubInfoZoneDataMap = pubInfoZoneSvc.getZoneData(ssn, zoneDataQueryFields);
		}
		
		loaded = true;
	}

	// 국가 정보
	private void loadCountryData() {
		if (countryList != null) return;
		if (zoneCountryCodeListMap != null) return;
		
		loadZoneDataMap();
		
		countryList = new ArrayList<GameCountry>();
		zoneCountryCodeListMap = new HashMap<Integer, List<String>>();
		
		Map<String, CodeObject> countryCodeTable = countryCodeSvc.getMapShortNameKey();
		Set<String> tmpCountryCodeSet = new HashSet<String>();
		
		Set<Integer> zoneCodeSet = pubInfoZoneDataMap.keySet();
		for (int zoneCode : zoneCodeSet) {
			PubInfoZoneData pubInfoZoneData = pubInfoZoneDataMap.get(zoneCode);
			if (pubInfoZoneData == null) continue;
			
			List<String> countryCodeList = pubInfoZoneData.getCountryList();
			if (countryCodeList == null || countryCodeList.size() == 0) continue;
		
			// 전체 게임인 경우
			if (countryCodeList.contains("99")) {
				List<String> newCountryCodeList = new ArrayList<String>();
				List<CodeObject> countryCodeObjList = countryCodeSvc.getAvailableList();
				if (countryCodeObjList != null) {
					for (CodeObject codeObj : countryCodeObjList) {
						newCountryCodeList.add(codeObj.getShortName());
					}
					countryCodeList = newCountryCodeList; 
				}
			} 
			
			// 전체 서비스 존의 국가 목록
			tmpCountryCodeSet.addAll(countryCodeList);
				
			// 서비스 존 별 국가 정보
			zoneCountryCodeListMap.put(zoneCode, countryCodeList);
		}
	
		for (String countryCode : tmpCountryCodeSet) {
			CodeObject countryCodeObj = countryCodeTable.get(countryCode);
			if (countryCodeObj == null) continue;
		
			GameCountry country = new GameCountry();
			country.setCode(countryCode);
			country.setName(countryCodeObj.getName());
			
			countryList.add(country);
		}
		
		
		Collections.sort(countryList, new Comparator<GameCountry>() {
			@Override
			public int compare(GameCountry country1, GameCountry country2) {
				return country1.getName().compareToIgnoreCase(country2.getName());
			}
		});
	}

	// 마켓 정보
	private void loadMarketData() {
		if (marketList != null) return;
		if (zoneMarketCodeListMap != null) return;
		
		loadZoneDataMap();
		
		marketList = new ArrayList<GameMarket>();
		zoneMarketCodeListMap = new HashMap<Integer, List<Integer>>();
	
		Map<Integer, CodeObject> marketCodeTable = marketCodeSvc.getAvailableMap();
		Set<Integer> tmpMarketCodeSet = new HashSet<Integer>();
		
		for (Entry<Integer, PubInfoZoneData> entry : pubInfoZoneDataMap.entrySet()) {
			PubInfoZoneData pubInfoZoneData = entry.getValue();
			if (pubInfoZoneData == null) continue;
			
			Map<Integer, MarketData> marketDataMap = pubInfoZoneData.getMarketDataMap();
			if (marketDataMap == null) continue;
			
			Set<Integer> marketCodeSet = marketDataMap.keySet();
			if (marketCodeSet == null) continue;
		
			tmpMarketCodeSet.addAll(marketCodeSet);
			zoneMarketCodeListMap.put(entry.getKey(), new ArrayList<Integer>(marketCodeSet));
		}
		
		for (int marketCode : tmpMarketCodeSet) {
			CodeObject marketCodeObj = marketCodeTable.get(marketCode);
			if (marketCodeObj == null) continue;
			
			GameMarket market = new GameMarket();
			
			market.setCode(marketCode);
			market.setName(marketCodeObj.getName());
			
			marketList.add(market);
		}
	}	
	
	// 언어 정보
	private void loadLangData() {
		if (langList != null) return;
		if (zoneLangCodeListMap != null) return;
		
		loadZoneDataMap();
		
		langList = new ArrayList<GameLanguage>();
		zoneLangCodeListMap = new HashMap<Integer, List<Integer>>();
	
		Map<Integer, CodeObject> langCodeTable = langCodeSvc.getAvailableMap();
		Set<Integer> tmpLangCodeSet = new HashSet<Integer>();
		
		for (Entry<Integer, PubInfoZoneData> entry : pubInfoZoneDataMap.entrySet()) {
			PubInfoZoneData pubInfoZoneData = entry.getValue();
			if (pubInfoZoneData == null) continue;
			
			List<Integer> langCodeList = pubInfoZoneData.getLangList();
			if (langCodeList == null) continue;
			
			tmpLangCodeSet.addAll(langCodeList);
			zoneLangCodeListMap.put(entry.getKey(), new ArrayList<Integer>(langCodeList));
		}
		
		for (int langCode : tmpLangCodeSet) {
			CodeObject langCodeObj = langCodeTable.get(langCode);
			if (langCodeObj == null) continue;
			
			GameLanguage lang = new GameLanguage();
			
			lang.setCode(langCode);
			lang.setName(langCodeObj.getName());
			lang.setShortName(langCodeObj.getShortName());
			
			langList.add(lang);
		}
	}
	@Override
	public List<GameZone> getAvailableZoneList() {
		List<PubInfoZone> pubInfoZoneList = pubInfoZoneSvc.getZoneList(ssn);
		List<GameZone> noticeZoneList = new ArrayList<GameZone>();
		
		Map<Integer, CodeObject> zoneCodeObjMap = zoneCodeSvc.getAvailableMap();
		
		if (pubInfoZoneList != null) {
			for (PubInfoZone pubInfoZone : pubInfoZoneList) {
				int zoneCode = pubInfoZone.getZone();
				
				CodeObject zoneCodeObj = zoneCodeObjMap.get(zoneCode);
				if (zoneCodeObj == null) continue;
				
				GameZone noticeZone = new GameZone();
				noticeZone.setCode(zoneCode);
				noticeZone.setName(zoneCodeObj.getName());
				
				noticeZoneList.add(noticeZone);
			}
		}
		
		return noticeZoneList;
	}

	@Override
	public List<GameCountry> getAvailableCountryList() {
		loadCountryData();
		return countryList;
	}
	
	@Override
	public Map<String, GameCountry> getAvailableCountryMap() {
		loadCountryData();
		Map<String, GameCountry> result = new HashMap<String, GameCountry>();
		
		if (countryList != null) {
			for (GameCountry country : countryList) {
				result.put(country.getCode(), country);
			}
		}
		
		return result;
	}

	@Override
	public List<GameCountry> getMarketCountryList(int market) {
		List<GameCountry> result = new ArrayList<GameCountry>();
		
		List<CodeObject> codeObjList = pubInfoCountrySvc.getCountryCodeListOfMarket(ssn, zone, market);
		if (codeObjList != null) {
			for (CodeObject codeObj : codeObjList) {
				GameCountry country = new GameCountry();
				country.setCode(codeObj.getShortName());
				country.setName(codeObj.getName());
				
				result.add(country);
			}
			
		}
		
		return result;
	}

	@Override
	public Map<Integer, List<String>> getAvailableZoneCountryListMap() {
		loadCountryData();
		return zoneCountryCodeListMap;
	}
	
	@Override
	public List<GameMarket> getAvailableMarketList() {
		loadMarketData();
		return marketList;
	}

	@Override
	public Map<Integer, List<Integer>> getAvailableZoneMarketListMap() {
		loadMarketData();
		return zoneMarketCodeListMap;
	}

	@Override
	public List<GameLanguage> getAvailableLangList() {
		loadLangData();
		return langList;
	}
	
	@Override
	public Map<Integer, List<Integer>> getAvailableZoneLangListMap() {
		loadMarketData();
		return zoneLangCodeListMap;
	}
	
	public String getLangShortName(int langCode){
		String langShortName = "";
		
		
		return langShortName;
	}
}
