package ftt.ems_admin.service.pubinfo.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ftt.common.pub.pubinfo.IPubInfoGameService;
import ftt.common.pub.pubinfo.IPubInfoZoneService;
import ftt.common.pub.pubinfo.PubInfoGame;
import ftt.common.pub.pubinfo.PubInfoZone;
import ftt.ems_admin.entity.pubinfo.EmsGame;
import ftt.ems_admin.service.pubinfo.IPubInfoListService;

@Service
public class PubInfoListService implements IPubInfoListService {
	
	@Autowired
	private IPubInfoGameService pubInfoGameSvc;
	
	@Autowired
	private IPubInfoZoneService pubInfoZoneSvc;
	
	@Override
	public List<EmsGame> getAvailableGameList() {
		List<PubInfoGame> pubInfoGameList = pubInfoGameSvc.getGameListNameOrder();
		List<EmsGame> noticeGameList = new ArrayList<EmsGame>();
		
		if (pubInfoGameList != null) {
			for (PubInfoGame pubInfoGame : pubInfoGameList) {
				EmsGame noticeGame = new EmsGame();
				
				noticeGame.setSsn(pubInfoGame.getSsn());
				noticeGame.setName(pubInfoGame.getName());
				noticeGame.setAppIcon(pubInfoGame.getAppIcon());
				
				noticeGameList.add(noticeGame);
			}
		}
		
		return noticeGameList;
	}

	@Override
	public List<Integer> getAvailableZoneCodeList(int ssn) {
		List<PubInfoZone> pubInfoZoneList = pubInfoZoneSvc.getZoneList(ssn);
		List<Integer> zoneCodeList = new ArrayList<Integer>();
	
		if (pubInfoZoneList != null) {
			for (PubInfoZone pubInfoZone : pubInfoZoneList) {
				zoneCodeList.add(pubInfoZone.getZone());
			}
		}
		
		return zoneCodeList;
	}
}
