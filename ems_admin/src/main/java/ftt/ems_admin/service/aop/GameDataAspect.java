package ftt.ems_admin.service.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import ftt.ems_admin.config.DataSourceContextHolder;

/**
 * 
 * @description 메소드 수행시 실행.
 * 
 *              게임 DB 선택
 * 
 */
@Aspect
@Component
public class GameDataAspect {

	
	@Around("execution(* ftt.ems_admin.service.game.*GameService.*(..)) ")
	public Object aroundExecute(ProceedingJoinPoint pjp) throws Throwable {

		Object[] args = pjp.getArgs();
		try {
			if (args != null && args[0] instanceof Integer) {
				DataSourceContextHolder.setDataSource(Integer.parseInt(args[0].toString()));
			}
			Object returnValue = pjp.proceed();
			DataSourceContextHolder.clearDataSource();
			return returnValue;
		} finally {
			DataSourceContextHolder.clearDataSource();
		}
		
	}

}
