package ftt.ems_admin.service.mng.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ftt.common.auth.AuthUser;
import ftt.ems_admin.dao.mng.IMngDao;
import ftt.ems_admin.entity.TesterEntity;
import ftt.ems_admin.entity.ems.EmsCondition;
import ftt.ems_admin.entity.ems.EmsForm;
import ftt.ems_admin.entity.ems.EmsHistory;
import ftt.ems_admin.entity.ems.EmsLang;
import ftt.ems_admin.entity.ems.EmsReward;
import ftt.ems_admin.helper.DateUtil;
import ftt.ems_admin.service.mng.IMngService;

@Service
public class MngService implements IMngService {

	@Autowired
	private IMngDao mngDao;

	/**
	 * Ems 신규 등록
	 */
	@Override
	@Transactional
	public void addEms(EmsForm form, AuthUser user) throws Exception {

		// reward save
		EmsReward reward = new EmsReward();
		reward.setSsn(form.getSsn());
		reward.setDefaultMessage(form.getDefaultMessage());
		reward.setMessageIdx(form.getMessageIdx());
		reward.setRewardStr(form.getRewardStr());
		mngDao.addRewardBox(reward);

		if (reward.getRewardIdx() < 1)
			return;

		// country
		mngDao.addCountry(form.getSsn(), form.getCountry());

		// market
		mngDao.addMarket(form.getSsn(), form.getMarket());

		// zone
		mngDao.addZone(form.getSsn(), form.getZone());

		// language
		mngDao.addLang(form.getSsn(), form.getMessage());

		// condition
		EmsCondition condition = new EmsCondition();
		condition.setSsn(form.getSsn());
		condition.setActUsn(user.getUsn());
		condition.setDispStatus(form.getDispStatus());
		condition.setRegDt(DateUtil.getDateFullString());
		condition.setModDt(DateUtil.getDateFullString());
		condition.setRewardIdx(reward.getRewardIdx());
		mngDao.addCondition(condition);
		
		//히스토리 저장
		addEmAdminHistory(form, form.getSsn(), user.getUsn());

	}

	/**
	 * Ems 수정 
	 */
	@Override
	@Transactional
	public void modifyEms(EmsForm form, AuthUser user) throws Exception {

		// reward
		EmsReward reward = new EmsReward();
		reward.setRewardIdx(form.getRewardIdx());
		reward.setSsn(form.getSsn());
		reward.setDefaultMessage(form.getDefaultMessage());
		reward.setMessageIdx(form.getMessageIdx());
		reward.setRewardStr(form.getRewardStr());
		mngDao.addRewardBox(reward);

		// condition
		EmsCondition condition = new EmsCondition();
		condition.setSsn(form.getSsn());
		condition.setActUsn(user.getUsn());
		condition.setDispStatus(form.getDispStatus());
		condition.setRegDt(DateUtil.getDateFullString());
		condition.setModDt(DateUtil.getDateFullString());
		condition.setRewardIdx(reward.getRewardIdx());
		mngDao.addCondition(condition);

		// country
		mngDao.delCountry(form.getSsn());
		mngDao.addCountry(form.getSsn(), form.getCountry());

		// market
		mngDao.delMarket(form.getSsn());
		mngDao.addMarket(form.getSsn(), form.getMarket());

		// zone
		mngDao.delZone(form.getSsn());
		mngDao.addZone(form.getSsn(), form.getZone());

		// lang
		mngDao.delLang(form.getSsn());
		mngDao.addLang(form.getSsn(), form.getMessage());
		
		//히스토리 저장
		addEmAdminHistory(form, form.getSsn(), user.getUsn());

	}

	/**
	 * Ems 조회 - Country
	 */
	@Override
	public List<String> getEmsCountry(int ssn) throws Exception {
		return mngDao.getCountry(ssn);
	}

	/**
	 * Ems 조회 - Market
	 */
	@Override
	public List<Integer> getEmsMarket(int ssn) throws Exception {
		return mngDao.getMarket(ssn);
	}

	/**
	 * Ems 조회 - Zone
	 */
	@Override
	public List<Integer> getEmsZone(int ssn) throws Exception {
		return mngDao.getZone(ssn);
	}

	/**
	 * Ems 조회 - Reward
	 */
	@Override
	public EmsReward getEmsReward(long rewardIdx) throws Exception {
		return mngDao.getReward(rewardIdx);
	}

	/**
	 * Ems 조회 - Lang
	 */
	@Override
	public List<EmsLang> getEmsLang(int ssn) throws Exception {
		return mngDao.getLang(ssn);
	}

	/**
	 * Ems 조회 - Condition
	 */
	@Override
	public EmsCondition getEmsCondition(int ssn) throws Exception {
		return mngDao.getCondition(ssn);
	}
	
	/**
	 * Ems 관리 히스토리 저장 
	 * @param form
	 * @param ssn
	 * @param usn
	 * @throws Exception
	 */
	@Override
	public void addEmAdminHistory(EmsForm form, int ssn, long usn) throws Exception {
		EmsHistory history = new EmsHistory();
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		String actContent = gson.toJson(form).toString();
		
		history.setActContent(actContent);
		history.setSsn(ssn);
		history.setActUsn(usn);
		history.setRegDt(DateUtil.getDateFullString());
		
		mngDao.addEmAdminHistory(history);
		
	}
	
	@Override
	public void addEmMain(List<TesterEntity> list) throws Exception{
		mngDao.addEmMainTest(list);
	}
	@Override
	public void addAccountInfo(List<TesterEntity> list) throws Exception{
		mngDao.addAccountInfoTest(list);
	}
	
}
