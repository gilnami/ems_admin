package ftt.ems_admin.service.mng;

import java.util.List;

import ftt.common.auth.AuthUser;
import ftt.ems_admin.entity.TesterEntity;
import ftt.ems_admin.entity.ems.EmsCondition;
import ftt.ems_admin.entity.ems.EmsForm;
import ftt.ems_admin.entity.ems.EmsLang;
import ftt.ems_admin.entity.ems.EmsReward;

public interface IMngService {

	public void addEms(EmsForm form, AuthUser user) throws Exception;

	public void modifyEms(EmsForm form, AuthUser user) throws Exception;

	public List<String> getEmsCountry(int ssn) throws Exception;

	public List<Integer> getEmsMarket(int ssn) throws Exception;

	public List<Integer> getEmsZone(int ssn) throws Exception;

	public EmsReward getEmsReward(long rewardIdx) throws Exception;

	public List<EmsLang> getEmsLang(int ssn) throws Exception;

	public EmsCondition getEmsCondition(int ssn) throws Exception;

	public void addEmAdminHistory(EmsForm form, int ssn, long usn) throws Exception;

	public void addEmMain(List<TesterEntity> list) throws Exception;

	public void addAccountInfo(List<TesterEntity> list) throws Exception;

}
