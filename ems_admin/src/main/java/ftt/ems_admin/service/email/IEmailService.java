package ftt.ems_admin.service.email;

import java.util.List;

import ftt.ems_admin.entity.email.EmailExport;
import ftt.ems_admin.entity.email.ExportHistory;

public interface IEmailService {

	public List<EmailExport> getSubscriveUserList(int ssn) throws Exception;
	
	public List<EmailExport> getFilteringSubscriveUserList(int ssn, String countryList, String langList) throws Exception;
	
	public List<EmailExport> getTargetFilteringSubscriveUserList(int ssn, List<Integer> accountList) throws Exception;
		
	public String getUnsubscribeLink(long pimIdx, String regDt, String langCode) throws Exception;
	
	public void setLoginLog(ExportHistory exportHistory) throws Exception;
	
	public String makeExportFileList(List<EmailExport> list) throws Exception;
}
