package ftt.ems_admin.service.email.impl;

import java.io.File;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import ftt.common.lib.AESWrap;
import ftt.ems_admin.config.AppConfig;
import ftt.ems_admin.dao.email.IEmailDao;
import ftt.ems_admin.entity.email.EmailExport;
import ftt.ems_admin.entity.email.ExportHistory;
import ftt.ems_admin.helper.DateUtil;
import ftt.ems_admin.helper.ExcelWriteUtil;
import ftt.ems_admin.service.email.IEmailService;

@Service
public class EmailService implements IEmailService {

	@Autowired
	private IEmailDao emailDao;
	
	@Autowired
	private AppConfig appConfig;
	
	@Autowired
	private ExcelWriteUtil excelWriteUtil;
	
	@Value("${spring.http.multipart.location}")
	private String tempDir;
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 정기구독 유저 목록 조회
	 */
	@Override
	public List<EmailExport> getSubscriveUserList(int ssn) throws Exception {
		List<EmailExport> list =  emailDao.getSubscriveUserList(ssn);
		logger.debug(list.toString());
		return list;
	}

	/**
	 * 정기구독 유저목록 필터링 
	 */
	@Override
	public List<EmailExport> getFilteringSubscriveUserList(int ssn, String country, String lang) throws Exception {

		final String _ALL = "99";
		List<EmailExport> originList = getSubscriveUserList(ssn);
		logger.debug("origin list : " + originList);
		List<EmailExport> countryFilteringList = new ArrayList<EmailExport>();
		List<EmailExport> langFilteringList = new ArrayList<EmailExport>();


		// filtering country
		if (country.isEmpty() || country.equals(_ALL)) {
			countryFilteringList.addAll(originList);
		} else {
			countryFilteringList = originList.stream().filter(o -> {
				if (country.indexOf(o.getCountryCode().toUpperCase()) > -1)
					return true;
				return false;
			}).collect(Collectors.toList());
		}

		// filtering language
		if (lang.isEmpty() || lang.equals(_ALL)) {
			logger.debug("language all");
			langFilteringList.addAll(countryFilteringList);
		} else {
			logger.debug("language filtering...dogin");
			langFilteringList = countryFilteringList.stream().filter(o -> {
				if (lang.indexOf(o.getLangCode().toLowerCase()) > -1)
					return true;
				return false;
			}).collect(Collectors.toList());
		}

		logger.debug("after language filtering" + langFilteringList);

		return langFilteringList;
	}
	
	/**
	 * 조건별 정기구독 유저 목록 필터링 
	 */
	@Override
	public List<EmailExport> getTargetFilteringSubscriveUserList(int ssn, List<Integer> accountList) throws Exception {

		List<EmailExport> originList = getSubscriveUserList(ssn);
		List<EmailExport> filteringList = new ArrayList<EmailExport>();
		filteringList = originList.stream().filter(o -> {
			for(Integer accountIdx : accountList){
				if(accountIdx == o.getAccountIdx()){
					accountList.remove(accountIdx);
					return true;
				}
			}
			return false;
		}).collect(Collectors.toList());

		return filteringList;
	}
	
	/**
	 * 수신거부 링크 생성 
	 */
	@Override
	public String getUnsubscribeLink(long pimIdx, String regDt, String langCode) throws Exception{

		String aesKey = appConfig.getAesKey();
		String aesIv = appConfig.getAesIv();
		
		long regTimeMillis = DateUtil.convertDateToTimeStamp(regDt);
		
		try {
			AESWrap aes = new AESWrap(aesKey.getBytes(), aesIv.getBytes());
			StringBuffer buffer = new StringBuffer();
			StringBuffer param = new StringBuffer();
			
			param.append("idx=").append(pimIdx).append("&rdt=").append(regTimeMillis);
			String encodedData = aes.encrypt(param.toString());
			
			buffer.append("lang=").append(langCode);
			buffer.append("&d=").append(URLEncoder.encode(encodedData, "UTF-8"));
			
			return buffer.toString();
		} catch (Exception e) {
			logger.error("getUnsubscribeLink Error pimIdx={}, errorMsg={}", pimIdx, e.getMessage());
		}

		return "";
	}
	
	@Override
	public String makeExportFileList(List<EmailExport> list) throws Exception {
		final String uploadFolder = "_export";
        String localRoot =  tempDir + "/";
        String uploadPath = uploadFolder + "/" + getDailyDir() + "/" + System.currentTimeMillis()/1000;
        
        File localTempDir = new File(localRoot +  "/" + uploadPath);
        if(! localTempDir.exists()) {
        	localTempDir.mkdirs();
        }
        excelWriteUtil.setSaveFilePath(localTempDir.getAbsolutePath());
        excelWriteUtil.setRowData(list);
        
        logger.debug("localTempDir : " + localTempDir.getAbsolutePath());
        String zipFile = excelWriteUtil.generateExcelData();
        
        logger.debug("zipFile :" + zipFile);
        
        /*try{
        	localTempDir.delete();
        	localTempDir.deleteOnExit();
        }catch(Exception e){};
*/        
        return zipFile;
	}
	
	/**
	 * 이메일 추출 로그 기록 
	 */
	@Override
	@Async("loggingThreadPoolTaskExecutor")
	public void setLoginLog(ExportHistory exportHistory) throws Exception{
		emailDao.addExportHistory(exportHistory);
	}

	//디렉토리 경로
	private String getDailyDir() throws Exception {
		Calendar oCalendar = Calendar.getInstance();
		int y = oCalendar.get(Calendar.YEAR);
		int m = oCalendar.get(Calendar.MONTH)+1;
		int d = oCalendar.get(Calendar.DAY_OF_MONTH);
		String dir = Integer.toString(y)+"/"+Integer.toString(m)+"/"+Integer.toString(d); 
		return dir;
	}
}
