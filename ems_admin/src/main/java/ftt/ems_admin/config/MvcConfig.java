package ftt.ems_admin.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import ftt.common.auth.auth2.web_support.Auth2Interceptor;
import ftt.ems_admin.controller.handler.AuthArgumentResolver;
import ftt.ems_admin.controller.interceptor.LogInterceptor;

@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

	@Autowired
	private AppConfig appConfig;

	@Autowired
	private Auth2Interceptor auth2Interceptor;
	
	@Autowired
	private AuthArgumentResolver authArgumentResolver;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {

		// for DEBUG
		if (appConfig.getEnvironment().equals("Dev")) {
			HandlerInterceptor logIntercepter = new LogInterceptor();
			registry.addInterceptor(logIntercepter);
		}
		registry.addInterceptor(auth2Interceptor);
	}

	@Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(authArgumentResolver);
    }
	
	@Bean
	public MessageSource messageSource() {

		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setCacheSeconds(600);
		messageSource.setBasenames(appConfig.getMessageResource());
		messageSource.setDefaultEncoding("UTF-8");

		return messageSource;
	}

	@Bean
	public LocaleResolver localeResolver() {

		SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();
		sessionLocaleResolver.setDefaultLocale(StringUtils.parseLocaleString("ko"));

		return sessionLocaleResolver;
	}

	@Bean
	public EmbeddedServletContainerCustomizer containerCustomizer() {

		return new EmbeddedServletContainerCustomizer() {
			@Override
			public void customize(ConfigurableEmbeddedServletContainer container) {

				ErrorPage error404Page = new ErrorPage(HttpStatus.NOT_FOUND, "/WEB-INF/jsp/error/404.jsp");
				container.addErrorPages(error404Page);
			}
		};
	}
}