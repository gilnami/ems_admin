package ftt.ems_admin.config;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

import ftt.common.auth.auth2client.Auth2Config;
import ftt.common.pim.impl.PimConfig;
import ftt.common.pub.PubConfig;
import ftt.common.reward.impl.RewardConfig;
import ftt.common.types.config.DataSourceConfig;
import ftt.ems_admin.config.property.EmsProperties;

@Configuration
@ConfigurationProperties(prefix = "application")
public class AppConfig {
	private Map<String, DataSourceConfig> datasource = new HashMap<String, DataSourceConfig>();
	private String environment;
	private String messageResource;
	private String staticDomain;
	private String aesKey;
	private String aesIv;
	private EmsProperties ems;
	private Map<String, String> SmtpSource = new HashMap<String, String>();
	private Map<String, String> reward = new HashMap<String, String>();
	private Map<String, String> pim = new HashMap<String, String>();
	
	@Autowired
	private PubConfig pubInfo;

	@Autowired
	private Auth2Config auth2;

	@Autowired
	private ApplicationContext context;
	
	@PostConstruct
	private void init() {
		System.setProperty("ems_admin.auth.addr", auth2.getServer());
		
		RewardConfig rewardConfig = context.getBean(RewardConfig.class);
		rewardConfig.setServiceUrl(getReward("api"));
		
		PimConfig pimConfig = context.getBean(PimConfig.class);
		pimConfig.setServiceUrl(getPim("server"));
	}
	

	public Map<String, DataSourceConfig> getDatasource() {
		return datasource;
	}

	public void setDatasource(Map<String, DataSourceConfig> datasource) {
		this.datasource = datasource;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public String getMessageResource() {
		return messageResource;
	}

	public void setMessageResource(String messageResource) {
		this.messageResource = messageResource;
	}

	public String getStaticDomain() {
		return staticDomain;
	}

	public void setStaticDomain(String staticDomain) {
		this.staticDomain = staticDomain;
	}

	public Auth2Config getAuth2() {
		return auth2;
	}

	public PubConfig getPubInfo() {
		return pubInfo;
	}

	public String getAesKey() {
		return aesKey;
	}

	public void setAesKey(String aesKey) {
		this.aesKey = aesKey;
	}

	public String getAesIv() {
		return aesIv;
	}

	public void setAesIv(String aesIv) {
		this.aesIv = aesIv;
	}

	public EmsProperties getEms() {
		return ems;
	}

	public void setEms(EmsProperties ems) {
		this.ems = ems;
	}

	public Map<String, String> getSmtpSource() {
		return SmtpSource;
	}

	public void setSmtpSource(Map<String, String> smtpSource) {
		SmtpSource = smtpSource;
	}


	public Map<String, String> getReward() {
		return reward;
	}
	
	public String getReward(String key) {
		return reward.get(key);
	}


	public void setReward(Map<String, String> reward) {
		this.reward = reward;
	}


	public Map<String, String> getPim() {
		return pim;
	}
	
	public String getPim(String key) {
		return pim.get(key);
	}

	public void setPim(Map<String, String> pim) {
		this.pim = pim;
	}


}
