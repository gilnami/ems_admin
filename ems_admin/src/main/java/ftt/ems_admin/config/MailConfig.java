package ftt.ems_admin.config;

import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;

@Configuration
public class MailConfig {
	
	@Autowired
	AppConfig appConfig;
	
	@Autowired
    private ServletContext servletContext;
	
	@Bean
	public JavaMailSender getMailSender(){
		
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		Map<String, String> smtpSource = appConfig.getSmtpSource();
		
		mailSender.setUsername(smtpSource.get("username"));
		mailSender.setPassword(smtpSource.get("password"));
		
		Properties properties = new Properties();
		properties.setProperty("mail.transport.protocol", "smtp");
		properties.setProperty("mail.smtp.host", smtpSource.get("host"));
		properties.setProperty("mail.smtp.port", smtpSource.get("port"));
		properties.setProperty("mail.smtp.auth", "true");
		properties.setProperty("mail.smtp.starttls.enable", "true");
		properties.setProperty("mail.smtp.ssl.trust", smtpSource.get("host"));

		//properties.setProperty("mail.smtp.socketFactory.port", smtpSource.get("port"));
		//properties.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		
		mailSender.setJavaMailProperties(properties);

		/*
		Logger logger = LoggerFactory.getLogger(this.getClass());
		logger.info("smtpSource@getMailSender: {}", smtpSource);
		*/

		return mailSender;
	}
	
	@Bean
	@Primary
    public FreeMarkerConfigurationFactoryBean getFreeMarkerConfiguration() {
		FreeMarkerConfigurationFactoryBean bean = new FreeMarkerConfigurationFactoryBean();
		bean.setDefaultEncoding("UTF-8");
		bean.setTemplateLoaderPath("/WEB-INF/freemarker");
		return bean;
    }
}