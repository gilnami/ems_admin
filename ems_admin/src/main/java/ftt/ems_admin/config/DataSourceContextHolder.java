package ftt.ems_admin.config;

public class DataSourceContextHolder {
	

	private static final ThreadLocal<Object> contextHolder = new ThreadLocal<>();

	public static void setDataSource(int ssn) {
		setDataSource("gamedb_"+ssn);
	}

	public static void setDataSource(Object obj) {
		contextHolder.set(obj);
	}

	public static Object getCurrentDataSource() {
		return  contextHolder.get();
	}

	public static void clearDataSource() {
		contextHolder.remove();
	}
	
}