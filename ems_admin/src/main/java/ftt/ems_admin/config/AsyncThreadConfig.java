package ftt.ems_admin.config;

import java.util.concurrent.Executor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class AsyncThreadConfig{

	/**
	 * 로그용 thread
	 * @return
	 */
	@Bean(name="loggingThreadPoolTaskExecutor")
	public Executor loggingThreadPoolTaskExecutor(){
		ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
		taskExecutor.setThreadGroupName("LOGGING");
		taskExecutor.setThreadNamePrefix("-");
		taskExecutor.setCorePoolSize(10); 
		taskExecutor.setMaxPoolSize(10); 
		taskExecutor.setQueueCapacity(10); 
		return taskExecutor;
	}
	
	/**
	 * 메일 발송용
	 * @return
	 */
	@Bean(name="mailingThreadPoolTaskExecutor")
	public Executor mailingThreadPoolTaskExecutor(){
		ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
		taskExecutor.setThreadGroupName("MAILING");
		taskExecutor.setThreadNamePrefix("-");
		taskExecutor.setCorePoolSize(10); 
		taskExecutor.setMaxPoolSize(10); 
		taskExecutor.setQueueCapacity(10); 
		return taskExecutor;
	}
	
	
}
