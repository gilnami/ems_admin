package ftt.ems_admin.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import ftt.common.types.config.DataSourceConfig;

@Configuration
@ConfigurationProperties(prefix = "")
public class GameDataSourceConfig {
		
	private Map<String, DataSourceConfig> datasource = new HashMap<String, DataSourceConfig>();
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@PostConstruct
	private void init() {
		List<String> removeKeys = new ArrayList<String>();
		for( String key : datasource.keySet() ){
			if(!key.startsWith("gamedb_")){
				removeKeys.add(key);
			} else {
				logger.info("found gamedb(" + key + ")");
			}
		}
		
		Iterator<String> iterator = removeKeys.iterator();
		while (iterator.hasNext()) {
			datasource.remove(iterator.next());
		}
	}

	public Map<String, DataSourceConfig> getDatasource() {
		return datasource;
	}
	
	public Map<String, DataSourceConfig> getDatasourceConfigMap() {
		return datasource;
	}
	
	public boolean isValidGame(int ssn){
		String datasourceKey = "gamedb_"+ssn;
		return datasource.containsKey(datasourceKey);
	}
}