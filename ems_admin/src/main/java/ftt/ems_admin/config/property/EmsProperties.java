package ftt.ems_admin.config.property;

public class EmsProperties {

	private String unsubscribe;

	public String getUnsubscribe() {
		return unsubscribe;
	}

	public void setUnsubscribe(String unsubscribe) {
		this.unsubscribe = unsubscribe;
	}
}
