package ftt.ems_admin.exception;

import java.util.Locale;

import org.springframework.context.MessageSource;

import ftt.ems_admin.annotation.ErrorCode;

@SuppressWarnings("serial")
public class CommonException extends Exception {
	private String errorCode;
	private String errorMessage;
	private String addMessage = "";
	private String debugMessage = "";
	private Object[] args;
	
	public CommonException(){
		super();
	}
	
	public CommonException(String errorCode){
		this.setErrorCode(errorCode);
	}
	
	public CommonException(ErrorCode code){
		this.setErrorCode(Integer.toString(code.getErrorCode()));
		this.setErrorMessage(code.getErrorMessage());
	}
	
	public CommonException setErrorCode(String errorCode){
		this.errorCode = errorCode;
		return this;
	}

	public String getErrorCode(){
		return errorCode;
	}

	public CommonException setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
		return this;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public String getErrorMessage(MessageSource messageSource, Locale locale) {
		if (errorMessage == null || errorMessage.equals(""))
			this.errorMessage = messageSource.getMessage(this.errorCode, this.args, locale);
		
		return errorMessage;
	}
	
	public CommonException setAddMessage(String addMessage){
		this.addMessage = addMessage;
		return this;
	}
	
	public String getAddMessage(){
		return addMessage;
	}

	public CommonException setDebugMessage(String debugMessage) {
		this.debugMessage = debugMessage;
		return this;
	}

	public String getDebugMessage() {
		return debugMessage;
	}

	public Object[] getArgs() {
		return args;
	}

	public void setArgs(Object[] args) {
		this.args = args;
	}
}