package ftt.ems_admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"ftt.common", "ftt.ems_admin"})
@EnableAutoConfiguration // (exclude={DataSourceAutoConfiguration.class})
public class FttEmsAdminApplication {
	public void run(String[] args) throws Exception {
		SpringApplication.run(FttEmsAdminApplication.class, args);
	}

	public static void main(String[] args) throws Exception {
		new FttEmsAdminApplication().run(args);
	}
}
